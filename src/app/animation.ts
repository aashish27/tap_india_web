import { animate, query, style, transition, trigger } from '@angular/animations';


export const topToBottom = trigger('routerAnimations', [
    transition('* => *', [
        query(
            ':enter',
            [style({ position: 'absolute', transform: 'translateY(-100px)', opacity: 0, width: '100%' })],
            { optional: true }
        ),
        query(
            ':leave',
            [style({ opacity: 1, transform: 'translateY(0)', position: 'absolute', width: '100%' })],
            { optional: true }
        ),
        query(
            ':leave',
            [animate(1000, style({ opacity: 0, transform: 'translateY(100px)' }))],
            { optional: true }
        ),
        query(
            ':enter',
            [animate(1000, style({ opacity: 1, transform: 'translateY(0)' }))],
            { optional: true }
        )
    ])
]);
