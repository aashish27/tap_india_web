import { Component, OnInit, Input, Output } from '@angular/core';
import { CommonFunctionService } from '../../services/common-service/common-service.service';
import { EventEmitter } from '@angular/core';
import { AuthGuard } from '../../pages/guard/auth.guard';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  @Input() endpoint:any;
  @Output() status:any = new EventEmitter();
  // @Input() guardParams: any;
  

  constructor(private commonService:CommonFunctionService, private authGuard: AuthGuard) { }

  ngOnInit() {
  }

  delete(){
    this.commonService.delete(this.endpoint).subscribe((res)=>{
      if(res['success']){
        this.status.emit({success : true});
      }
    }, (error)=>{
      console.log(error);
    });
  }
}
