import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'chart.js';
declare const $:any;
@Component({
  selector: 'app-common-chart',
  templateUrl: './common-chart.component.html',
  styleUrls: ['./common-chart.component.css']
})
export class CommonChartComponent implements OnInit {

  @Input() data:any;
  @Input() id:any;

  common_chart:any;
  common_c:any;
  ctxCanvas:any;

  constructor() { 
  }

  ngOnInit() {
    setTimeout(()=>{
      this.commonChart(this.id, this.data.chartType);
    },20);
  }

  commonChart(id, chartType){
    this.common_chart = document.getElementById(id);

    // console.log(document.getElementById(id).getContext('2d'));
    

    if(this.common_chart){
      var ctx = document.getElementById(id).getContext('2d');

      
      if(id == "mainstream-chart"){
        /*** Gradient ***/
        var gradient = ctx.createLinearGradient(0, 0, 0, 400);
        gradient.addColorStop(0, 'rgba(255,119,138,1)');   
        gradient.addColorStop(0.8, 'rgba(255,119,138,0)');

        this.data.datasets[0]['backgroundColor'] = gradient;
      }

      if(this.data.yId){
    
        if(id == "dropout-chart"){
          /*** Gradient ***/
          var gradient = ctx.createLinearGradient(0, 0, 0, 400);
              gradient.addColorStop(0, 'rgba(0,167,210,1)');   
              gradient.addColorStop(0.5, 'rgba(0,167,210,0)');
        }else{
          /*** Gradient ***/
          var gradient = ctx.createLinearGradient(0, 0, 0, 400);
              gradient.addColorStop(0, 'rgba(116,108,255,1)');   
              gradient.addColorStop(0.5, 'rgba(116,108,255,0)');
        }

        this.data.datasets[0]['backgroundColor'] = gradient;
      }

      let speedData = {
        labels: this.data.labels,
        datasets: this.data.datasets,
      };

      let chartOptions;
      if(chartType == "pie"){
        chartOptions = {
          title:{
            display: true,
            text: this.data.chartTitle,
            fontSize: 15
          },
          // tooltips: {
          //   mode: 'index',
          //   intersect: false,
          //   yPadding: 10,
          //   xPadding: 20,
          //   custom: function (tooltip) {
          //       if (!tooltip) return;
          //       tooltip.displayColors = true;
          //   },
          //   title: false,
          //   borderColor: '#47aaea',
          //   borderWidth: 1,
          //   backgroundColor: '#FFF',
          //   titleFontSize: 16,
          //   titleFontColor: '#0066ff',
          //   bodyFontColor: '#000',
          //   bodyFontSize: 12,
          //   displayColors: false
          // }
        };
      }else{
        if(this.data.yId){
          chartOptions = {
            maintainAspectRatio: false,
            responsive:true,
            scaleBeginAtZero: false,
            barBeginAtOrigin: true,
            title:{
              display: true,
              text: this.data.chartTitle,
              fontSize: 15,
              position: "bottom"
            },
            legend: {
                position: 'top',
                labels: {
                    fontColor: 'black',
                    usePointStyle: true,
                }
            },
            tooltips: {
              mode: 'index',
              intersect: false,
              yPadding: 10,
              xPadding: 20,
              custom: function (tooltip) {
                  if (!tooltip) return;
                  tooltip.displayColors = true;
              },
              callbacks: {
                  label: function (tooltipItem, data) {
                      return data.datasets[tooltipItem.datasetIndex].label + " :" + tooltipItem.yLabel;
                  },
                  title: function (tooltipItem, data) {
                      return tooltipItem[0].xLabel;
                  }
              },
              borderColor: '#47aaea',
              borderWidth: 1,
              backgroundColor: '#FFF',
              titleFontSize: 14,
              titleFontColor: '#0066ff',
              bodyFontColor: '#000',
              bodyFontSize: 12,
              displayColors: false
            },
            scales:{
              xAxes:[{
                barPercentage: 0.8,
                categoryPercentage: 0.3,
                ticks : {
                  autoSkip: false,
                  // minRotation : 30
                },
                gridLines: {
                  display: true
                }
              }],
              yAxes:[{
                barThickness: 7,
                borderSkipped:'right',
                gridLines: {
                    display: true
                },
                ticks : {
                 display: true,
                 beginAtZero: true
                },
                scaleLabel: {
                  display: true,
                  labelString: this.data.labelString[0]
                },
                position: 'left'
              },{
                barThickness: 7,
                borderSkipped:'right',
                gridLines: {
                  drawOnChartArea: false
                },
                ticks : {
                 display: true,
                 beginAtZero: true
                },
                scaleLabel: {
                  display: true,
                  labelString: this.data.labelString[1]
                },
                id: this.data.yId,
                position: 'right'
              }]
            }
          };
        }else{
          chartOptions = {
            maintainAspectRatio: false,
            responsive:true,
            scaleBeginAtZero: false,
            barBeginAtOrigin: true,
            title:{
              display: true,
              text: this.data.chartTitle,
              fontSize: 15,
              position: "bottom"
            },
            legend: {
                position: 'top',
                labels: {
                    fontColor: 'black',
                    usePointStyle: true,
                }
            },
            tooltips: {
              mode: 'index',
              intersect: false,
              yPadding: 10,
              xPadding: 20,
              custom: function (tooltip) {
                  if (!tooltip) return;
                  tooltip.displayColors = true;
              },
              callbacks: {
                  label: function (tooltipItem, data) {
                      return data.datasets[tooltipItem.datasetIndex].label + " :" + tooltipItem.yLabel;
                  },
                  title: function (tooltipItem, data) {
                      return tooltipItem[0].xLabel;
                  }
              },
              borderColor: '#47aaea',
              borderWidth: 1,
              backgroundColor: '#FFF',
              titleFontSize: 14,
              titleFontColor: '#0066ff',
              bodyFontColor: '#000',
              bodyFontSize: 12,
              displayColors: false
            },
            scales:{
              xAxes:[{
                barPercentage: 0.7,
                categoryPercentage: 0.4,
                ticks : {
                  autoSkip: false,
                  // minRotation : 30
                },
                gridLines: {
                  display: true
                }
              }],
              yAxes:[{
                barThickness: 7,
                borderSkipped:'right',
                gridLines: {
                    display: true
                },
                ticks : {
                 display: true,
                 beginAtZero: true
                },
                scaleLabel: {
                  display: true,
                  labelString: this.data.labelString
                } 
              }]
            }
          };
        }
      }
  
      this.common_c = new Chart(id, {
        type: chartType,
        data: speedData,
        options: chartOptions
      });
    }
    }
}
