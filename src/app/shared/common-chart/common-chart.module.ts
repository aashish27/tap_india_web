import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonChartComponent } from './common-chart.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[CommonChartComponent],
  declarations: [CommonChartComponent],
})
export class CommonChartModule { }
