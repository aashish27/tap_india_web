import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AccordionAnchorDirective} from './accordion';
import {AccordionLinkDirective} from './accordion';
import {AccordionDirective} from './accordion';

// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {MenuItems} from './menu-items/menu-items';
import {SpinnerComponent} from './spinner/spinner.component';
import {CardComponent} from './card/card.component';
import {CardRefreshDirective} from './card/card-refresh.directive';
import {CardToggleDirective} from './card/card-toggle.directive';
import {DataFilterPipe} from './element/data-filter.pipe';
import {ScrollTopComponent} from './scroll-top/scroll-top.component';
import { BlockCopyPasteDirective } from './directives/block-copy-paste.directive';

@NgModule({
    imports: [
        CommonModule,
        
        //NgbModule.forRoot()
    ],
    declarations: [
        AccordionAnchorDirective,
        AccordionLinkDirective,
        AccordionDirective,
        CardRefreshDirective,
        CardToggleDirective,
        SpinnerComponent,
        CardComponent,
        ScrollTopComponent,
        DataFilterPipe,
        BlockCopyPasteDirective
    ],
    exports: [
        AccordionAnchorDirective,
        AccordionLinkDirective,
        AccordionDirective,
        CardRefreshDirective,
        CardToggleDirective,
        // NgbModule,
        SpinnerComponent,
        CardComponent,
        DataFilterPipe,
        ScrollTopComponent,
        BlockCopyPasteDirective
    ],
    providers: [
        MenuItems
    ]
})
export class SharedModule {
}
