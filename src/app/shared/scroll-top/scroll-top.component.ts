import {Component, OnInit} from '@angular/core';

declare const $: any;

@Component({
    selector: 'app-scroll-top',
    templateUrl: './scroll-top.component.html',
    styleUrls: ['./scroll-top.component.css']
})
export class ScrollTopComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
        window.onscroll = function () {
            scrollFunction();
        };

        function scrollFunction() {
            if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {

                if (document.getElementById('myBtn')) document.getElementById('myBtn').style.display = 'block';
            } else {
                if (document.getElementById('myBtn'))
                    document.getElementById('myBtn').style.display = 'none';
            }
        }
    }

    topFunction() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    }
}
