import {Injectable} from '@angular/core';
import { element } from '@angular/core/src/render3';
import { EncrDecrService } from '../../services/encr-decr/encr-decr.service';
// let MENUITEMS: any = [];
declare const $: any;

// let MenuAndSubmenuList: any = [{
let MENUITEMS: any = [{
    state: 'home',
    short_label: 'B',
    name: 'Home',
    type: 'link',
    icon: 'fas fa-home',
},
{
    state: 'beneficiary',
    short_label: 'B',
    name: 'Beneficiary',
    type: 'link',
    icon: 'fas fa-user',
},{
    state: 'dashboard',
    short_label: 'B',
    name: 'Dashboard',
    type: 'link',
    icon: 'fas fa-chart-line',
},
{
    state: 'activity-data-entry',
    short_label: 'B',
    name: 'Activity Data Entry',
    type: 'sub',
    icon: 'fas fa-toolbox',
    subModule: [
        {
            state: 'solar-spray-machines',
            name: 'Solar Spray Machines',
            icon: 'fas fa-file-alt',
        },{
            state: 'farm-mechanization',
            name: 'Farm Mechanization',
            icon: 'fas fa-file-alt',
            type : 'sub',
            subModule : [
                {
                    state: 'potato',
                    name: 'Potato Planter',
                    icon: 'fas fa-file-alt',
                },{
                    state: 'zero',
                    name: 'Zero Tillage',
                    icon: 'fas fa-file-alt',
                }
            ]
        },{
            state: 'kitchen-garden',
            name: 'Kitchen Garden',
            icon: 'fas fa-file-alt',
        },{
            state: 'crop-demonstration',
            name: 'Crop Demonstration',
            icon: 'fas fa-file-alt',
        },{
            state: 'crop-diversification',
            name: 'Crop Diversification',
            icon: 'fas fa-file-alt',
        },{
            state: 'goat-promotion',
            name: 'Goat Promotion',
            icon: 'fas fa-file-alt',
        },{
            state: 'training-and-exposure-visit',
            name: 'Training And Exposure Visit',
            icon: 'fas fa-file-alt',
        },{
            state: 'school-renovation',
            name: 'School Renovation',
            icon: 'fas fa-file-alt',
        },
        {
            state: 'soak-pit-demonstrations',
            name: 'Soak Pit Demonstrations',
            icon: 'fas fa-file-alt',
        },{
            state: 'composit-unit',
            name: 'Compost Unit',
            icon: 'fas fa-file-alt',
        },{
            state: 'animal-health-camps',
            name: 'Animal Health Camps',
            icon: 'fas fa-file-alt',
        },{
            state: 'digital-literacy-center',
            name: 'Digital Literacy Center',
            icon: 'fas fa-file-alt',
        },{
            state: 'health-and-hygiene',
            name: 'Health And Sanitation Session',
            icon: 'fas fa-file-alt',
        },{
            state: 'pond-rejuvenation',
            name: 'Pond Rejuvenation',
            icon: 'fas fa-file-alt',
        },{
            state: 'hand-maize-sheller',
            name: 'Hand Maize Sheller',
            icon: 'fas fa-file-alt',
        }
        // ,{
        //     state: 'baseline',
        //     name: 'Baseline And Endline Evaluation',
        //     icon: 'fas fa-file-alt',
        // },{
        //     state: 'info-and-com',
        //     name: 'Information and communication  materials',
        //     icon: 'fas fa-file-alt',
        // },{
        //     state: 'monitor-info',
        //     name: 'Monitoring and Information System',
        //     icon: 'fas fa-file-alt',
        // }
    ]
},{
    state: 'masters',
    short_label: 'B',
    name: 'Masters',
    type: 'sub',
    icon: 'fas fa-cogs',
    subModule: [
        {
            state: 'district',
            name: 'District',
            icon: 'fas fa-file-alt',
            type: "link",
            subModule: []
        },{
            state: 'block',
            name: 'Block',
            icon: 'fas fa-file-alt',
        },{
            state: 'village',
            name: 'Village Profile',
            icon: 'fas fa-file-alt',
        },{
            state: 'activity',
            name: 'Activity List',
            icon: 'fas fa-file-alt',
        },{
            state: 'activity-target',
            name: 'Activity Target',
            icon: 'fas fa-file-alt',
        },{
            state: 'crop',
            name: 'Crop',
            icon: 'fas fa-file-alt',
        },{
            state: 'variety',
            name: 'Variety',
            icon: 'fas fa-file-alt',
        },{
            state: 'machinery',
            name: 'Machinery',
            icon: 'fas fa-file-alt',
        },{
            state: 'machinery-code-mapping',
            name: 'Machinery Code Mapping',
            icon: 'fas fa-file-alt',
        },{
            state: 'machinery-owner',
            name: 'Machinery Owner',
            icon: 'fas fa-file-alt',
        },{
            state: 'capacity-building-topic',
            name: 'CB Topic',
            icon: 'fas fa-file-alt',
        },
        
    ]
},{
    state: 'user',
    short_label: 'B',
    name: 'User',
    type: 'sub',
    icon: 'fas fa-users',
    subModule: [
        {
            state: 'users',
            name: 'Users',
            icon: 'fas fa-file-alt',
        },
        {
            state: 'users-level',
            name: 'Users-level',
            icon: 'fas fa-file-alt',
        }
        
    ]
}];

@Injectable()
export class MenuItems {
    constructor(private encrDecr: EncrDecrService) {}
    getMenuList(modules) { // Return Menu Item filtered by their active status ..
        var filteredModules = modules.filter((element) => element.isActive);
        filteredModules.forEach((el, index) => {
            let sub = el["subModule"].filter((element) => element.isActive);
            sub.forEach((elm , ind) => {
                let subChild = elm["subModule"].filter((elementChild) => elementChild.isActive);
                sub[ind]["subModule"] = subChild;
            });
            filteredModules[index]["subModule"] = sub;
        });
        return filteredModules;
    }

    getAll() { // get menu items ..
        // var decryptedAccessModules = this.encrDecr.get('123456$#@$^@1ERF', sessionStorage.getItem('accessModules'));
        // let modules = JSON.parse(decryptedAccessModules);
        // MENUITEMS =  this.getMenuList(modules.data);
        return MENUITEMS;
    }
}