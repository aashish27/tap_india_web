import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { Router } from '@angular/router';

export interface UserDetails {
    id: number
    first_name: string
    last_name: string
    email: string
    password: string
    exp: number
    iat: number
    // type: string    
}

interface TokenResponse{
    token:string
}

export interface TokenPayload{
    id: number
    first_name: string
    last_name: string
    email: string
    type:string
    password: string
}

@Injectable()
export class AuthService {
    base_uri: string = environment.base_uri;
    private token: string;
    // private headers;
    // private options;
    // private isUserLoggedIn;
    constructor(private httpClient: HttpClient, private router: Router) {
        // this.isUserLoggedIn = false;
    }

    private saveToken(token: string): void {
        localStorage.setItem('userToken', token)
        this.token = token
    }

    public getToken(): string {
        if(!this.token){
            this.token = localStorage.getItem('userToken')
        }
        return this.token
    }

    public getUserDetails():UserDetails{
        const token = this.getToken()
        let payload
        if(token){
            payload = token.split('.')[1]
            payload = window.atob(payload)
            return JSON.parse(payload)
        }else{
            return null
        }
    }

    public isLoggedIn(): boolean{
        const user = this.getUserDetails()
        if(user){
            return user.exp > Date.now() / 1000
        }else{
            return false
        }
    }

    public register(user: TokenPayload): Observable<any> {
        const base = this.httpClient.post(`/users/register`, user)

        const request = base.pipe(
            map((data:TokenResponse)=>{
                if(data.token){
                    this.saveToken(data.token)
                }
                return data
            })
        )

        return request
    }


    public login(user: TokenPayload): Observable<any> {
        const base = this.httpClient.post(`/users/login`, user)

        const request = base.pipe(
            map((data:TokenResponse)=>{
                if(data.token){
                    this.saveToken(data.token)
                }
                return data
            })
        )

        return request
    }

    public profile(): Observable<any>{
        return this.httpClient.get('users/profile', {
            headers : { Authorization: `$${this.getToken()}` }
        })
    }

    public logOut(): void{
        this.token = '';
        window.localStorage.removeItem('userToken')
        this.router.navigateByUrl('login')
    }

    // login(data) { // Login Function Call..
    //     return this.httpClient.post(this.base_uri + 'login', data);
    // }

    // setUserLoggedIn() { // Change user login status..
    //     this.isUserLoggedIn = true;
    // }

    // isLoggedIn(routesInformation){ // Check Routes is accessible or not ..
    //     // Do not delete the commented code  ..... 
    //     var decryptedAccessModules = this.encrDecr.get('123456$#@$^@1ERF', sessionStorage.getItem('accessModules'));
    //     let modules = JSON.parse(decryptedAccessModules);
    //     let moduleArray = [];
    //     modules.data.forEach((parentElm)=>{
    //       let moudles = { // Parent Module breakups 
    //         'state': parentElm.state,
    //         'action': parentElm.action,
    //         'isActive': parentElm.isActive
    //       }
    //       parentElm.subModule.forEach((subAction)=>{
    //         let moudles = {// Submodule breakups 
    //           'state': subAction.state,
    //           'action': subAction.action,
    //           'isActive': subAction.isActive
    //         }
    //         subAction.subModule.forEach(subChildAction => {
    //             let moudles = {// Submodule breakups 
    //                 'state': subChildAction.state,
    //                 'action': subChildAction.action,
    //                 'isActive': subChildAction.isActive
    //             }
    //             moduleArray.push(moudles); 
    //         });
    //         moduleArray.push(moudles); 
    //       });
    //       moduleArray.push(moudles);      
    //     });
        
    //     let status = moduleArray.find((value) => value.state == routesInformation.component);
    //     if(typeof status !== "undefined"){
    //         if(status.hasOwnProperty("isActive")){
    //             if(status.isActive){
    //                 let value = status.action.find((f) => f.action == routesInformation.action);
    //                 return value.status;    
    //             }else {
    //                 return false;    
    //             }
    //         }
    //     }
    //     // return true;
    // }

    // getUserLoggedIn() { // Check that is user logged in or not..
    //     const token = sessionStorage.getItem('token');
    //     if (token) {
    //         return true;
    //     }
    //     return false;
    // }
}
