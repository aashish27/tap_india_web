import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from './auth/auth.service';

declare const $:any;

@Injectable()
export class LoaderService {

  constructor() {}

  public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  display(value: boolean) {
      this.status.next(value);
  }
  
  showLoader($) {
    $('.gifload').html('<img id="loaderimg" src="../../../assets/images/loader1.gif">');
    $('#loader').css('display', 'block');
    $('.loadShow').css('display', 'none');
    $('.table-responsive').css('display', 'none');
  }

  hideLoader($) {
    $('#loader').css('display', 'none');
    $('.loadShow').css('display', 'block');
    $('.table-responsive').css('display', 'block');
  }

  showCustomDivLoader(value){
    $('#'+value).css('display', 'block');
  }
  
  hideCustomDivLoader(value){
    $('#'+value).css('display', 'none');
  }
}
