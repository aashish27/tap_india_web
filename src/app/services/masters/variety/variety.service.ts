import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Variety } from '../../../models/masters/variety';

@Injectable({
  providedIn: 'root'
})

export class VarietyService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getVariety(endpoints: any): Observable<Variety[]> { // Get Variety..
    return this.httpClient.get<Variety[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postVariety(endpoints: any, hero: Variety): Observable<Variety> {
      return this.httpClient.post<Variety>(this.baseUrl + endpoints, hero)
       .pipe(
         catchError(this.handleError)
       );
  }

  putVariety(endpoints: any, hero: Variety){
    return this.httpClient.put<Variety>(this.baseUrl + endpoints, hero)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

