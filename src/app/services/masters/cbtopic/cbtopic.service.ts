import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { CbTopic } from '../../../models/masters/cbtopic';

@Injectable({
  providedIn: 'root'
})

export class CbTopicService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getCbtopic(endpoints: any): Observable<CbTopic[]> { // Get CbTopic..
    return this.httpClient.get<CbTopic[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postCbtopic(endpoints: any, hero: CbTopic): Observable<CbTopic> {
      return this.httpClient.post<CbTopic>(this.baseUrl + endpoints, hero)
       .pipe(
         catchError(this.handleError)
       );
  }

  putCbtopic(endpoints: any, hero: CbTopic){
    return this.httpClient.put<CbTopic>(this.baseUrl + endpoints, hero)
       .pipe(
         catchError(this.handleError)
       );
  }


  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

