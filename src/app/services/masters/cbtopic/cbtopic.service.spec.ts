import { TestBed } from '@angular/core/testing';

import { CbtopicService } from './cbtopic.service';

describe('CbtopicService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CbtopicService = TestBed.get(CbtopicService);
    expect(service).toBeTruthy();
  });
});
