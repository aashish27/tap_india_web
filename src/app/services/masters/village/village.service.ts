/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Village } from '../../../models/masters/village';

@Injectable({
  providedIn: 'root'
})

export class VillageService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getVillages(endpoints: any): Observable<Village[]> { // Get Districts ..
    return this.httpClient.get<Village[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postVillages(endpoints: any, Village: Village): Observable<Village> {
      return this.httpClient.post<Village>(this.baseUrl + endpoints, Village)
       .pipe(
         catchError(this.handleError)
       );
  }

  putVillages(endpoints: any, Village: Village){
    return this.httpClient.put<Village>(this.baseUrl + endpoints, Village)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
