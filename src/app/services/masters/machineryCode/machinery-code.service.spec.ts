import { TestBed } from '@angular/core/testing';

import { MachineryCodeService } from './machinery-code.service';

describe('MachineryCodeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MachineryCodeService = TestBed.get(MachineryCodeService);
    expect(service).toBeTruthy();
  });
});
