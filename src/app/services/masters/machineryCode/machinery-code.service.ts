
/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { MachineryCode } from '../../../models/masters/machinerycode';

@Injectable({
  providedIn: 'root'
})

export class MachineryCodeService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getMachineryCode(endpoints: any): Observable<MachineryCode[]> { // Get Districts ..
    return this.httpClient.get<MachineryCode[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postMachineryCode(endpoints: any, MachineryCode: MachineryCode): Observable<MachineryCode> {
      return this.httpClient.post<MachineryCode>(this.baseUrl + endpoints, MachineryCode)
       .pipe(
         catchError(this.handleError)
       );
  }

  putMachineryCode(endpoints: any, MachineryCode: MachineryCode){
    return this.httpClient.put<MachineryCode>(this.baseUrl + endpoints, MachineryCode)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
