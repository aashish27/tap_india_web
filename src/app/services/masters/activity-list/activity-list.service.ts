/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { ActivityList } from '../../../models/masters/activity-list';

@Injectable({
  providedIn: 'root'
})

export class ActivityListService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getActivityList(endpoints: any): Observable<ActivityList[]> { // Get Districts ..
    return this.httpClient.get<ActivityList[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postActivityList(endpoints: any, ActivityList: ActivityList): Observable<ActivityList> {
      return this.httpClient.post<ActivityList>(this.baseUrl + endpoints, ActivityList)
       .pipe(
         catchError(this.handleError)
       );
  }

  putActivityList(endpoints: any, ActivityList: ActivityList){
    return this.httpClient.put<ActivityList>(this.baseUrl + endpoints, ActivityList)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
