
/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { District } from '../../../models/masters/district';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class DistrictService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getDistricts(endpoints: any): Observable<District[]> { // Get Districts ..
    return this.httpClient.get<District[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postDistricts(endpoints: any, district: District): Observable<District> { // Create New District ..
      return this.httpClient.post<District>(this.baseUrl + endpoints, district)
       .pipe(
         catchError(this.handleError)
       );
  }

  putDistricts(endpoints: any, district: District){
    return this.httpClient.put<District>(this.baseUrl + endpoints, district)
       .pipe(
         catchError(this.handleError)
       );
  }



  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
