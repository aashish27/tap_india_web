/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { ActivityTarget } from '../../../models/masters/activity-target';

@Injectable({
  providedIn: 'root'
})

export class ActivityTargetService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getActivityTargetList(endpoints: any): Observable<ActivityTarget[]> { // Get Districts ..
    return this.httpClient.get<ActivityTarget[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postActivityTargetList(endpoints: any, ActivityTarget: ActivityTarget): Observable<ActivityTarget> {
      return this.httpClient.post<ActivityTarget>(this.baseUrl + endpoints, ActivityTarget)
       .pipe(
         catchError(this.handleError)
       );
  }

  putActivityTargetList(endpoints: any, ActivityTarget: ActivityTarget){
    return this.httpClient.put<ActivityTarget>(this.baseUrl + endpoints, ActivityTarget)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
