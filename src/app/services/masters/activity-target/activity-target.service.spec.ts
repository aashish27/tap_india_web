import { TestBed } from '@angular/core/testing';

import { ActivityTargetService } from './activity-target.service';

describe('ActivityTargetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityTargetService = TestBed.get(ActivityTargetService);
    expect(service).toBeTruthy();
  });
});
