
/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { MachineOwner } from '../../../models/masters/machineowner';

@Injectable({
  providedIn: 'root'
})

export class MachineryOwnerService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getMachineryOwner(endpoints: any): Observable<MachineOwner[]> { // Get Districts ..
    return this.httpClient.get<MachineOwner[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postMachineryOwner(endpoints: any, MachineOwner: MachineOwner): Observable<MachineOwner> {
      return this.httpClient.post<MachineOwner>(this.baseUrl + endpoints, MachineOwner)
       .pipe(
         catchError(this.handleError)
       );
  }

  putMachineryCode(endpoints: any, MachineOwner: MachineOwner){
    return this.httpClient.put<MachineOwner>(this.baseUrl + endpoints, MachineOwner)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
