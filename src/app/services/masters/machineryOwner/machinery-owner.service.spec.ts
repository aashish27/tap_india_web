import { TestBed } from '@angular/core/testing';

import { MachineryOwnerService } from './machinery-owner.service';

describe('MachineryOwnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MachineryOwnerService = TestBed.get(MachineryOwnerService);
    expect(service).toBeTruthy();
  });
});
