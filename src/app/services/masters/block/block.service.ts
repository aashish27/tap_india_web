import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Block } from '../../../models/masters/block';

@Injectable({
  providedIn: 'root'
})

export class BlockService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getBlocks(endpoints: any): Observable<Block[]> { // Get Block..
    return this.httpClient.get<Block[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postBlock(endpoints: any, hero: Block): Observable<Block> {
      return this.httpClient.post<Block>(this.baseUrl + endpoints, hero)
       .pipe(
         catchError(this.handleError)
       );
  }

  putBlock(endpoints: any, hero: Block){
    return this.httpClient.put<Block>(this.baseUrl + endpoints, hero)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

