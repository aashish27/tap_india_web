
/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Machinery } from '../../../models/masters/machinery';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class MachineryService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getMachinery(endpoints: any): Observable<Machinery[]> { // Get Districts ..
    return this.httpClient.get<Machinery[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postMachinery(endpoints: any, Machinery: Machinery): Observable<Machinery> {
      return this.httpClient.post<Machinery>(this.baseUrl + endpoints, Machinery)
       .pipe(
         catchError(this.handleError)
       );
  }

  putMachinery(endpoints: any, Machinery: Machinery){
    return this.httpClient.put<Machinery>(this.baseUrl + endpoints, Machinery)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
