/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Crop } from '../../../models/masters/crop';

@Injectable({
  providedIn: 'root'
})

export class CropService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getCrops(endpoints: any): Observable<Crop[]> { // Get Crops ..
    return this.httpClient.get<Crop[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postCrops(endpoints: any, Crop: Crop): Observable<Crop> {
      return this.httpClient.post<Crop>(this.baseUrl + endpoints, Crop)
       .pipe(
         catchError(this.handleError)
       );
  }

  putCrops(endpoints: any, Crop: Crop){
    return this.httpClient.put<Crop>(this.baseUrl + endpoints, Crop)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
