import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Common } from '../../models/common';

@Injectable({
  providedIn: 'root'
})

export class CommonFunctionService {
  baseUrl: string = environment.base_uri;
  activityList:any;

  constructor(private httpClient: HttpClient) { }
  
  getOoscDetails(endpoints: any){
    const  headers = new  HttpHeaders().set("Authorization", "Digest " + btoa("vrindarak.v@dhwaniris.com:vrindarak"))
    .set("Access-Control-Allow-Origin","*");

    return this.httpClient.get(endpoints)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  delete(endpoints: any){
    return this.httpClient.delete<Common>(this.baseUrl + endpoints)
       .pipe(
         catchError(this.handleError)
       );
  }

  get(order){
    let activityData = JSON.parse(sessionStorage.getItem('activity'));
    
    let result = activityData.filter(obj => {
      return obj['order'] == order;
    });
    
    return result[0]._id;
  }


  setActivities(endpoints: any) { // Get Districts ..
    return this.httpClient.get(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }


  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

  removeClass(params, attrType = "id", className="card-loader"){ // Remove class from dom...
    if(attrType == "id"){
      $("#"+params).removeClass(className);
    }else {
      $(params).removeClass(className);
    }
  }

  addClass(params, attrType = "id", className="card-loader"){ // Remove class from dom...
    if(attrType == "id"){
      $("#"+params).addClass(className);
    }else {
      $(params).addClass(className);
    }
  }

  getYear(){
   return [
     {value: 2019, name:'2019'}
    ]
  }

  getMonth(){
    return [
      {value: 1, name:'Jan'},
      {value: 2, name:'Feb'},
      {value: 3, name:'Mar'},
      {value: 4, name:'Apr'},
      {value: 5, name:'May'},
      {value: 6, name:'June'},
      {value: 7, name:'July'},
      {value: 8, name:'Aug'},
      {value: 9, name:'Sept'},
      {value: 10, name:'Oct'},
      {value: 11, name:'Nov'},
      {value: 12, name:'Dec'},
     ]
  }


}

