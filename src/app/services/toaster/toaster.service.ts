import { Injectable } from '@angular/core';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';

@Injectable()
export class ToasterService {
    position = 'top-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    constructor(public toastyService: ToastyService ) { }

    getToastData(options){ // Return Toast Option data value ..
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        // this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                /* added */
            },
            onRemove: (toast: ToastData) => {
                /* removed */
            }
        };
        return toastOptions;
    }

}


