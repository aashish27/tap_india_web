import {Injectable} from '@angular/core'
import {Router, CanActivate} from '@angular/router'
import {AuthService} from '../auth/auth.service'

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private router:Router, private auth: AuthService){}

    canActivate(){
        if(!this.auth.isLoggedIn()){
            this.router.navigateByUrl("/login")
            return false
        }
        return true
    }
}