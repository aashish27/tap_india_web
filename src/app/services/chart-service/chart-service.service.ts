import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ChartServiceService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getCharts(endpoints: any) { // Get Districts ..
    return this.httpClient.get(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

  handlePieChartColor(colorNoCount, type) {
      if(type == 'pie' || type == "colorArray"){
          let colorArray: any = [];
          colorArray = [
              '#5c6bc0', '#f06292', '#00acc1', '#1de9b6', '#fb8c00',
              '#ffd600', '#1b5e20', '#afb42b', '#651fff', '#f44336',
              "#c45850", "#8e5ea2","#3cba9f" , "#3cba9f","#e8c3b9",
          ];
          return colorArray;
      }else if(type == "bar") {
          let val = colorNoCount;
          switch(val){ 
              case 0:
                  return "#5c6bc0"; 
              case 1:
                  return "#f06292";
              case 2:
                  return "#00acc1";
              case 3:
                  return "#00acc1";
              case 4:
                  return "#1de9b6";
              case 5:
                  return "#fb8c00";
              case 6:
                  return "#ffd600";
              case 7:
                  return "#1b5e20";
              case 8:
                  return "#afb42b";
              case 9:
                  return "#00cc44";
              case 10:
                  return "#660033";
              default:
                  return "#red";
          }
      }
  }

}
