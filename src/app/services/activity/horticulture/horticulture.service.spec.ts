import { TestBed } from '@angular/core/testing';

import { HorticultureService } from './horticulture.service';

describe('HorticultureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HorticultureService = TestBed.get(HorticultureService);
    expect(service).toBeTruthy();
  });
});
