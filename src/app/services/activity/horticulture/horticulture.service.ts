/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Horticulture } from '../../../models/activity/horticulture';

@Injectable({
  providedIn: 'root'
})

export class HorticultureService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getHorticulture(endpoints: any): Observable<Horticulture[]> { // Get Districts ..
    return this.httpClient.get<Horticulture[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postHorticulture(endpoints: any, Horticulture: Horticulture): Observable<Horticulture> {
      return this.httpClient.post<Horticulture>(this.baseUrl + endpoints, Horticulture)
       .pipe(
         catchError(this.handleError)
       );
  }

  putHorticulture(endpoints: any, Horticulture: Horticulture){
    return this.httpClient.put<Horticulture>(this.baseUrl + endpoints, Horticulture)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

