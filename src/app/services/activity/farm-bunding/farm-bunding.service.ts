/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Farm } from '../../../models/activity/farm';

@Injectable({
  providedIn: 'root'
})

export class FarmBundingService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getFarmBunding(endpoints: any): Observable<Farm[]> { // Get Districts ..
    return this.httpClient.get<Farm[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postFarmBunding(endpoints: any, Farm: Farm): Observable<Farm> {
      return this.httpClient.post<Farm>(this.baseUrl + endpoints, Farm)
       .pipe(
         catchError(this.handleError)
       );
  }

  putFarmBunding(endpoints: any, Farm: Farm){
    return this.httpClient.put<Farm>(this.baseUrl + endpoints, Farm)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

