import { TestBed } from '@angular/core/testing';

import { FarmBundingService } from './farm-bunding.service';

describe('FarmBundingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FarmBundingService = TestBed.get(FarmBundingService);
    expect(service).toBeTruthy();
  });
});
