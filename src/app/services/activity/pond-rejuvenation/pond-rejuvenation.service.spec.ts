import { TestBed } from '@angular/core/testing';

import { PondRejuvenationService } from './pond-rejuvenation.service';

describe('PondRejuvenationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PondRejuvenationService = TestBed.get(PondRejuvenationService);
    expect(service).toBeTruthy();
  });
});
