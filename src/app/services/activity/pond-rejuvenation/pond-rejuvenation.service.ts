/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Pond } from '../../../models/activity/pond';

@Injectable({
  providedIn: 'root'
})

export class PondRejuvenationService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getPond(endpoints: any): Observable<Pond[]> { // Get Districts ..
    return this.httpClient.get<Pond[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postPond(endpoints: any, Pond: Pond): Observable<Pond> {
      return this.httpClient.post<Pond>(this.baseUrl + endpoints, Pond)
       .pipe(
         catchError(this.handleError)
       );
  }

  putPond(endpoints: any, Pond: Pond){
    return this.httpClient.put<Pond>(this.baseUrl + endpoints, Pond)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
