import { TestBed } from '@angular/core/testing';

import { FarmPondService } from './farm-pond.service';

describe('FarmPondService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FarmPondService = TestBed.get(FarmPondService);
    expect(service).toBeTruthy();
  });
});
