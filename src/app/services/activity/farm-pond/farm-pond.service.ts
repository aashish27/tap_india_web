/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { FarmPond } from '../../../models/activity/farm-pond';

@Injectable({
  providedIn: 'root'
})

export class FarmPondService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getFarmPond(endpoints: any): Observable<FarmPond[]> { // Get Districts ..
    return this.httpClient.get<FarmPond[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postFarmPond(endpoints: any, FarmPond: FarmPond): Observable<FarmPond> {
      return this.httpClient.post<FarmPond>(this.baseUrl + endpoints, FarmPond)
       .pipe(
         catchError(this.handleError)
       );
  }

  putFarmPond(endpoints: any, FarmPond: FarmPond){
    return this.httpClient.put<FarmPond>(this.baseUrl + endpoints, FarmPond)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

