import { TestBed } from '@angular/core/testing';

import { CropDemonstrationService } from './crop-demonstration.service';

describe('CropDemonstrationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CropDemonstrationService = TestBed.get(CropDemonstrationService);
    expect(service).toBeTruthy();
  });
});
