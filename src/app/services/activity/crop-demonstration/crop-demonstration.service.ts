/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { CropDemonstration } from '../../../models/activity/crop-demonstartion';

@Injectable({
  providedIn: 'root'
})

export class CropDemonstrationService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getCropDemonstration(endpoints: any): Observable<CropDemonstration[]> { // Get Districts ..
    return this.httpClient.get<CropDemonstration[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postCropDemonstration(endpoints: any, CropDemonstration: CropDemonstration): Observable<CropDemonstration> {
      return this.httpClient.post<CropDemonstration>(this.baseUrl + endpoints, CropDemonstration)
       .pipe(
         catchError(this.handleError)
       );
  }

  putCropDemonstration(endpoints: any, CropDemonstration: CropDemonstration){
    return this.httpClient.put<CropDemonstration>(this.baseUrl + endpoints, CropDemonstration)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}

