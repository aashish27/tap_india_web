import { TestBed } from '@angular/core/testing';

import { SoilTestingService } from './soil-testing.service';

describe('SoilTestingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoilTestingService = TestBed.get(SoilTestingService);
    expect(service).toBeTruthy();
  });
});
