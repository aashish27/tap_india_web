/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { SoilTesting } from '../../../models/activity/soil-testing';

@Injectable({
  providedIn: 'root'
})

export class SoilTestingService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getSoilTesting(endpoints: any): Observable<SoilTesting[]> { // Get Districts ..
    return this.httpClient.get<SoilTesting[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postSoilTesting(endpoints: any, SoilTesting: SoilTesting): Observable<SoilTesting> {
      return this.httpClient.post<SoilTesting>(this.baseUrl + endpoints, SoilTesting)
       .pipe(
         catchError(this.handleError)
       );
  }

  putSoilTesting(endpoints: any, SoilTesting: SoilTesting){
    return this.httpClient.put<SoilTesting>(this.baseUrl + endpoints, SoilTesting)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
