/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { SolarSpray } from '../../../models/activity/solar-spray';

@Injectable({
  providedIn: 'root'
})

export class SolarSprayService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getSolarSpray(endpoints: any): Observable<SolarSpray[]> { // Get Districts ..
    return this.httpClient.get<SolarSpray[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postSolarSpray(endpoints: any, SolarSpray: SolarSpray): Observable<SolarSpray> {
      return this.httpClient.post<SolarSpray>(this.baseUrl + endpoints, SolarSpray)
       .pipe(
         catchError(this.handleError)
       );
  }

  putSolarSpray(endpoints: any, SolarSpray: SolarSpray){
    return this.httpClient.put<SolarSpray>(this.baseUrl + endpoints, SolarSpray)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
