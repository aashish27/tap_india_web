import { TestBed } from '@angular/core/testing';

import { SolarSprayService } from './solar-spray.service';

describe('SolarSprayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SolarSprayService = TestBed.get(SolarSprayService);
    expect(service).toBeTruthy();
  });
});
