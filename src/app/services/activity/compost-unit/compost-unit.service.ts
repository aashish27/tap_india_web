/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Composit } from '../../../models/activity/composit';

@Injectable({
  providedIn: 'root'
})

export class CompostUnitService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getCompostUnit(endpoints: any): Observable<Composit[]> { // Get Districts ..
    return this.httpClient.get<Composit[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postCompostUnit(endpoints: any, Composit: Composit): Observable<Composit> {
      return this.httpClient.post<Composit>(this.baseUrl + endpoints, Composit)
       .pipe(
         catchError(this.handleError)
       );
  }

  putCompostUnit(endpoints: any, Composit: Composit){
    return this.httpClient.put<Composit>(this.baseUrl + endpoints, Composit)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
