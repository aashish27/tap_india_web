import { TestBed } from '@angular/core/testing';

import { CompostUnitService } from './compost-unit.service';

describe('CompostUnitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompostUnitService = TestBed.get(CompostUnitService);
    expect(service).toBeTruthy();
  });
});
