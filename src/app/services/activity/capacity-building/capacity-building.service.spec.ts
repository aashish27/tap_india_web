import { TestBed } from '@angular/core/testing';

import { CapacityBuildingService } from './capacity-building.service';

describe('CapacityBuildingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CapacityBuildingService = TestBed.get(CapacityBuildingService);
    expect(service).toBeTruthy();
  });
});
