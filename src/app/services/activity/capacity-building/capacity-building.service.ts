/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Capacity } from '../../../models/activity/capacity';

@Injectable({
  providedIn: 'root'
})

export class CapacityBuildingService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getCapacityBuilding(endpoints: any): Observable<Capacity[]> { // Get Districts ..
    return this.httpClient.get<Capacity[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postCapacityBuilding(endpoints: any, Capacity: Capacity): Observable<Capacity> {
      return this.httpClient.post<Capacity>(this.baseUrl + endpoints, Capacity)
       .pipe(
         catchError(this.handleError)
       );
  }

  putCapacityBuilding(endpoints: any, Capacity: Capacity){
    return this.httpClient.put<Capacity>(this.baseUrl + endpoints, Capacity)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
