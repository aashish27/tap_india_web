import { TestBed } from '@angular/core/testing';

import { TrainingExposureService } from './training-exposure.service';

describe('TrainingExposureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrainingExposureService = TestBed.get(TrainingExposureService);
    expect(service).toBeTruthy();
  });
});
