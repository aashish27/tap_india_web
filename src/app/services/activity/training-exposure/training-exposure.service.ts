/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { TrainingExposure } from '../../../models/activity/training-exposure';

@Injectable({
  providedIn: 'root'
})

export class TrainingExposureService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getTrainingExposure(endpoints: any): Observable<TrainingExposure[]> { // Get Districts ..
    return this.httpClient.get<TrainingExposure[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postTrainingExposure(endpoints: any, TrainingExposure: TrainingExposure): Observable<TrainingExposure> {
      return this.httpClient.post<TrainingExposure>(this.baseUrl + endpoints, TrainingExposure)
       .pipe(
         catchError(this.handleError)
       );
  }

  putTrainingExposure(endpoints: any, TrainingExposure: TrainingExposure){
    return this.httpClient.put<TrainingExposure>(this.baseUrl + endpoints, TrainingExposure)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
