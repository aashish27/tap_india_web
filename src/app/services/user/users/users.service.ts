/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Users } from '../../../models/user/users/users';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getUsers(endpoints: any): Observable<Users[]> { // Get Districts ..
    return this.httpClient.get<Users[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postUsers(endpoints: any, Users: Users): Observable<Users> {
      return this.httpClient.post<Users>(this.baseUrl + endpoints, Users)
       .pipe(
         catchError(this.handleError)
       );
  }

  putUsers(endpoints: any, Users: Users){
    return this.httpClient.put<Users>(this.baseUrl + endpoints, Users)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
