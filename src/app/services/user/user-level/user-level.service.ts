/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { UserLevel } from '../../../models/user/user-level/user-level';

@Injectable({
  providedIn: 'root'
})

export class UserLevelService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getUserLevel(endpoints: any): Observable<UserLevel[]> { // Get Districts ..
    return this.httpClient.get<UserLevel[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  postUserLevel(endpoints: any, obj): Observable<UserLevel> { // Posting new user level record ..
      return this.httpClient.post<UserLevel>(this.baseUrl + endpoints, obj)
       .pipe(
         catchError(this.handleError)
       );
  }

  putUserLevel(endpoints: any, obj){
    return this.httpClient.put<UserLevel>(this.baseUrl + endpoints, obj)
       .pipe(
         catchError(this.handleError)
       );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
