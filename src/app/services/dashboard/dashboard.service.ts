/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Dashboard } from '../../models/dashboard';

@Injectable({
  providedIn: 'root'
})

export class DashboardService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getDashboardData(endpoints: any): Observable<Dashboard[]> { 
    return this.httpClient.get<Dashboard[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
