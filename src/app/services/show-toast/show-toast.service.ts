import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class ShowToastService {

    private _listners = new Subject<any>();

    listen(): Observable<any> {
      return this._listners.asObservable();
    }

    filter(filterBy: any, e : any = "") {
      this._listners.next(filterBy);
      this._listners.next(e);
    }

}
