/**
 * @author Vrindarak Vishwakarma <vrindarak7@gmail.com>
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError,retry } from 'rxjs/operators';
import { Beneficiary } from '../../models/beneficiary';
import { Image } from '../../models/image';

@Injectable({
  providedIn: 'root'
})

export class BeneficiaryService {
  baseUrl: string = environment.base_uri;
  constructor(private httpClient: HttpClient) { }

  getBeneficiary(endpoints: any): Observable<Beneficiary[]> { // Get Districts ..
    return this.httpClient.get<Beneficiary[]>(this.baseUrl + endpoints)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  
  postBeneficiary(endpoints: any, Beneficiary: Beneficiary): Observable<Beneficiary> {
      return this.httpClient.post<Beneficiary>(this.baseUrl + endpoints, Beneficiary)
       .pipe(
         catchError(this.handleError)
       );
  }

  putBeneficiary(endpoints: any, Beneficiary: Beneficiary){
    return this.httpClient.put<Beneficiary>(this.baseUrl + endpoints, Beneficiary)
       .pipe(
         catchError(this.handleError)
       );
  }

  getS3Url(endpoints: any, images: Image): Observable<Image>{
    return this.httpClient.post<Image>(this.baseUrl + endpoints, images)
    .pipe(
      catchError(this.handleError)
    );
  }

  postS3Url(url: any, images: any): Observable<Image>{
    return this.httpClient.put<Image>(url, images,)
    .pipe(
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

}
