import {Routes} from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';
import {AuthComponent} from './layout/auth/auth.component';
import {AuthGuard} from './pages/guard/auth.guard';
import { AuthGuardService } from './services/guards/auth-guard.service';

export const AppRoutes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full',
            },{
                path : 'home',
                loadChildren : './pages/home/home.module#HomeModule',
                canActivate: [AuthGuardService]
            },{
                path : 'masters',
                loadChildren : './pages/masters/masters.module#MastersModule'
            },{
                path : 'beneficiary',
                loadChildren : './pages/beneficiary/beneficiary.module#BeneficiaryModule'
            },{
                path : 'dashboard',
                loadChildren : './pages/dashboard/dashboard.module#DashboardModule'
            },{
                path : 'activity-data-entry',
                loadChildren : './pages/activity-data-entry/activity-data-entry.module#ActivityDataEntryModule'
            },{
                path : 'user',
                loadChildren : './pages/user/user.module#UserModule'
            },{
                path: 'unauthorized',
                loadChildren: './pages/unauthorized/unauthorized.module#UnauthorizedModule'
            }
        ]
    }, {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                loadChildren: './pages/login/login.module#LoginModule'
            }
           
        ]
    }
];