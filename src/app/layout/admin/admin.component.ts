import {Component, ElementRef, OnInit, ViewChild, Injectable, AfterViewInit} from '@angular/core';
import {animate, AUTO_STYLE, state, style, transition, trigger} from '@angular/animations';
import {MenuItems} from '../../shared/menu-items/menu-items';
import {Router} from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

declare const $: any;

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls:['./admin.component.css'],
    animations: [
        trigger('mobileMenuTop', [
            state('no-block, void',
                style({
                    overflow: 'hidden',
                    height: '0px',
                })
            ),
            state('yes-block',
                style({
                    height: AUTO_STYLE,
                })
            ),
            transition('no-block <=> yes-block', [
                animate('400ms ease-in-out')
            ])
        ]),
        trigger('fadeInOutTranslate', [
            transition(':enter', [
                style({opacity: 0}),
                animate('400ms ease-in-out', style({opacity: 1}))
            ]),
            transition(':leave', [
                style({transform: 'translate(0)'}),
                animate('400ms ease-in-out', style({opacity: 0}))
            ])
        ])
    ]
})

// @Injectable()
export class AdminComponent implements OnInit, AfterViewInit {

    @ViewChild('searchFriends') search_friends: ElementRef;
    /*  @ViewChild('toggleButton') toggle_button: ElementRef;
      @ViewChild('sideMenu') side_menu: ElementRef;*/
    menu_items: any = [];
    userData: any;
    userType: string;
    projects: any;
    project: any;
    projectId: string;
    menuArray: any;

    constructor(public menuItems: MenuItems,
                private router: Router, private auth:AuthService) {}

    ngOnInit() {
        this.projectId = sessionStorage.getItem('projectId');
        

        // let data = JSON.parse(sessionStorage.getItem('userData'));
        // if(!data.isPasswordChangedOnce){
        //     this.menu_items = [];
        // }else{
        //     this.menu_items = this.menuItems.getAll();
        // }
        this.menu_items = this.menuItems.getAll();
        // console.log("this.menu_items",this.menu_items);
    }

    handleLogoClick() {
        this.router.navigate(['/projects/overview']);
    }

    ngAfterViewInit() {
        // $('.dropdown').hover(function () {
        //     $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        // }, function () {
        //     $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
        // });
    }

    // Start: Log out function to redirect to the login Page ..
    logout() {
        this.auth.logOut();
    }
}
