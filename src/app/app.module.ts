import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutes} from './app.routing';
// import { MyInterceptor } from './my-interceptor';
// Services
import {AuthService} from './services/auth/auth.service';
import {LoaderService} from './services/loader.service';
// import {AttachDocumentsService} from './services/'

// Guard
import {AuthGuard} from './pages/guard/auth.guard';
import {SharedModule} from './shared/shared.module';

// Components
import {AppComponent} from './app.component';
import {AdminComponent} from './layout/admin/admin.component';
import {BreadcrumbsComponent} from './layout/admin/breadcrumbs/breadcrumbs.component';
import {TitleComponent} from './layout/admin/title/title.component';
import {AuthComponent} from './layout/auth/auth.component';

import {ClickOutsideModule} from 'ng-click-outside';
import {DatePipe} from '@angular/common';
import {ToasterService} from './services/toaster/toaster.service';
import {ToastyConfig, ToastyModule, ToastyService} from 'ng2-toasty';
import {NgProgressModule} from '@ngx-progressbar/core';
import { Interceptor } from './interceptor';
import { DataTablesModule } from 'angular-datatables';
import { Apisendpoints } from './apisendpoints';
import { ShowToastService } from './services/show-toast/show-toast.service';
import { EncrDecrService } from './services/encr-decr/encr-decr.service';
import { AuthGuardService } from './services/guards/auth-guard.service';
// import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
    declarations: [
        AppComponent,
        AdminComponent,
        BreadcrumbsComponent,
        TitleComponent,
        AuthComponent
    ],
    imports: [
        DataTablesModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        BrowserAnimationsModule,
        HttpModule,
        HttpClientModule,
        RouterModule.forRoot(AppRoutes),
        ClickOutsideModule,
        SharedModule,
        ToastyModule.forRoot(),
        NgProgressModule.forRoot()
        // NgxSmartModalModule.forRoot()
    ],

    providers: [
        // { // Interceptor configration .. 
        //     provide: HTTP_INTERCEPTORS,
        //     useClass: Interceptor,
        //     multi: true
        // },
        AuthService, LoaderService, AppComponent,
        AuthService, AdminComponent,
        AuthGuardService,
        AuthGuard, DatePipe,
        ToasterService, ToastyService,
        ToastyConfig, Apisendpoints,
        ShowToastService, EncrDecrService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
