import { AbstractControl } from '@angular/forms';

export function WhitespaceValidator(control: AbstractControl) {
    if(control.value == null || control.value.length == 0) {
        return null;
    }else{
        const isWhitespace = (control.value || '').toString().trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }
}