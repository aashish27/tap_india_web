import { AbstractControl } from '@angular/forms';

export function AreaValidator(control: AbstractControl): { [key: string]: any } | null {
    if(control.value == null || control.value.length == 0) {
        return null;
    }else{
        const valid = /^\d[0-9]*(\.[0-9]{1,2})?$/.test(control.value);
        return valid ? null : { invalidArea: { valid: false, value: control.value } };
    }
}