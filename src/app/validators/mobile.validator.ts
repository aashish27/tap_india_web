import { AbstractControl } from '@angular/forms';

export function mobileValidator(control: AbstractControl): { [key: string]: any } | null {
    if(control.value == null || control.value.length == 0) {
        return null;
    }else{
        const valid = /^[6-9]\d{9}$/.test(control.value);
        return valid ? null : { invalidNumber: { valid: false, value: control.value } };
    }
}