import {AbstractControl} from '@angular/forms';
export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('password').value; // to get value in input tag
       let confirmPassword = AC.get('confirm_password') ? AC.get('confirm_password').value : AC.get('new_password').value; // to get value in input tag
        if(password != confirmPassword) {
            if(AC.get('confirm_password')){
                AC.get('confirm_password').setErrors( {MatchPassword: true} )
            }else{
                AC.get('new_password').setErrors( {MatchPassword: true} )
            }
            
        } else {
            return null
        }
    }
}