import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ToasterService } from './services/toaster/toaster.service';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';
import { environment } from '../environments/environment';
import { AuthService } from './services/auth/auth.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
    position: any = "top-right";
    constructor(private toastyService: ToastyService, private toaster: ToasterService, private auth: AuthService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = this.auth.getToken();
        if (token) {
            let url = request.url.split('/')[2];
            if(url == "staging-dhwani.s3.ap-south-1.amazonaws.com"){
                request = request.clone({ headers: request.headers.set('Content-Type', 'application/x-www-form-urlencoded').set('x-access-token', token) });
            }else{
                request = request.clone({ 
                    headers: request.headers.set('Content-Type', 'application/json'),
                });
            }
        }else {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                   if(event.body && event.body.success){
                    if(request.method == "PUT" || request.method == "POST" || request.method == "DELETE"){
                        if(event.url != environment.base_uri + "getS3Url"){
                            this.toastyService.clearAll();
                            this.showToast({    
                                title: 'Successful!',
                                msg: event.body.message,
                                timeout: 3000,
                                theme: 'material',
                                position: 'top-right',
                                type: 'success'
                            });
                        }
                    }   
                   }else if(event.body && !event.body.success){
                    if(request.method == "PUT" || request.method == "POST" || request.method == "DELETE"){
                        this.toastyService.clearAll();
                        this.showToast({
                            title: 'Error!',
                            msg: event.body.message,
                            timeout: 3000,
                            theme: 'material',
                            position: 'top-right',
                            type: 'error'
                        });
                    } 
                   }
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                console.log("error",error);
                return throwError(error);
            }));
    }

    showToast(options) { // Show Toaster ..
        let toastOptions = this.toaster.getToastData(options);
        switch (options.type) {
            case 'default':
                this.toastyService.default(toastOptions);
                break;
            case 'info':
                this.toastyService.info(toastOptions);
                break;
            case 'success':
                this.toastyService.success(toastOptions);
                break;
            case 'wait':
                this.toastyService.wait(toastOptions);
                break;
            case 'error':
                this.toastyService.error(toastOptions);
                break;
            case 'warning':
                this.toastyService.warning(toastOptions);
                break;
        }
    }
}
