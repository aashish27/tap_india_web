import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { UnauthorizedComponent } from './unauthorized.component';

export const LoginRoutes: Routes = [
  {
    path: '',
    component: UnauthorizedComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    FormsModule
  ],
  declarations: [UnauthorizedComponent]
})
export class UnauthorizedModule {
}
