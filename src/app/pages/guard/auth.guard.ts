import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private auth: AuthService, private router: Router) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // if (this.auth.getUserLoggedIn() === false) {
        //     this.router.navigate(['/login']);
        // }
        // let routesInfo = next.data;
        // if (this.auth.isLoggedIn(routesInfo) === false) {
        //     this.router.navigate(['/unauthorized']);
        // }
        return true;
    }

    checkActiveStatusCustom(routesInfo){ // Custom
        // if (this.auth.getUserLoggedIn() === false) {
        //     this.router.navigate(['/login']);
        //     return false;
        // }

        // if (this.auth.isLoggedIn(routesInfo) === false) {
        //     this.router.navigate(['/unauthorized']);
        //     return false;
        // }

        return true
    }

}
