import { BeneficiaryModule } from './beneficiary.module';

describe('BeneficiaryModule', () => {
  let beneficiaryModule: BeneficiaryModule;

  beforeEach(() => {
    beneficiaryModule = new BeneficiaryModule();
  });

  it('should create an instance', () => {
    expect(beneficiaryModule).toBeTruthy();
  });
});
