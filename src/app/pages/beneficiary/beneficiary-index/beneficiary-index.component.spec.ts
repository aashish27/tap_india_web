import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryIndexComponent } from './beneficiary-index.component';

describe('BeneficiaryIndexComponent', () => {
  let component: BeneficiaryIndexComponent;
  let fixture: ComponentFixture<BeneficiaryIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
