import { BeneficiaryIndexModule } from './beneficiary-index.module';

describe('BeneficiaryIndexModule', () => {
  let beneficiaryIndexModule: BeneficiaryIndexModule;

  beforeEach(() => {
    beneficiaryIndexModule = new BeneficiaryIndexModule();
  });

  it('should create an instance', () => {
    expect(beneficiaryIndexModule).toBeTruthy();
  });
});
