
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BeneficiaryIndexComponent } from './beneficiary-index.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { PopupModule } from '../../../shared/popup/popup.module';

export const BeneficiaryIndexRoutes: Routes = [
  {
    path: '',
    component: BeneficiaryIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule,
    FormsModule,
    PopupModule,
    FormsModule,
    RouterModule.forChild(BeneficiaryIndexRoutes),
  ],
  declarations: [BeneficiaryIndexComponent]
})
export class BeneficiaryIndexModule { }