import { Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../apisendpoints';
import { BeneficiaryService } from '../../../services/beneficiary/beneficiary.service';
import { LoaderService } from '../../../services/loader.service';
import { VillageService } from '../../../services/masters/village/village.service';
import { environment } from '../../../../environments/environment';
import { AuthGuard } from '../../guard/auth.guard';

@Component({
  selector: 'app-beneficiary-index',
  templateUrl: './beneficiary-index.component.html',
  styleUrls: ['./beneficiary-index.component.css']
})
export class BeneficiaryIndexComponent implements OnInit {
  skip = 0;
  limit = 10;
  totalCount:number;
  page = 1;
  delete_id:any = "";
  beneficiaries:any = {};
  villages:any = {};
  downloadUrl:string;
  sortFlag : number = 1;
  url:string = "";
  tableHideShowStatus: boolean = false;

  name:string = "";
  village:string = "";
  father:string = "";
  code:string = "";

  constructor(private authGuard:AuthGuard, private beneficiaryService: BeneficiaryService, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router, private villageService:VillageService) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
    this.getVillages();
  }

  get(sortby = ''){ // Get Beneficiary data .
    if(sortby){
      if(this.sortFlag == -1){
        this.sortFlag = 1;
      }else{
        this.sortFlag = -1;
      }
    }
    this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + '?skip=' + this.skip + '&limit=' + this.limit + '&name=' + this.name + '&code=' + this.code + '&fatherName=' + this.father
     + '&villageId=' + this.village + '&sortby=' + sortby + '&sort=' + this.sortFlag  ).subscribe( res=> { 
          this.beneficiaries  = res;
          if(this.beneficiaries.success){
            this.totalCount = res['total'];
            this.loaderService.hideLoader($); 
            this.tableHideShowStatus = true;
          }
    });
  }

  downloadCSV(){
    if(this.authGuard.checkActiveStatusCustom({component: 'beneficiary', action: 'download'})){
      this.downloadUrl = environment.base_uri + this.baseUrl.downloadBeneficiary + '?token=' + sessionStorage.getItem('token') + '&name=' + this.name + '&code=' + this.code + '&fatherName=' + this.father + '&villageId=' + this.village;
    }
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    });
  }
  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'beneficiary', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteBeneficiary + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['beneficiary/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['beneficiary/view'], {queryParams: {id : data._id}});
    }
  }

  getDataByLimit(limit) {
    this.limit = limit;
    this.loaderService.showLoader($);
    this.get();
  }

  setPage(event: any) {
      this.page = event;
      this.skip = (this.page * this.limit) - this.limit;
      this.loaderService.showLoader($);
      this.get();
  }

}
