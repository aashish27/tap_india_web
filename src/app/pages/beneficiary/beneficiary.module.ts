import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BeneficiaryComponent } from './beneficiary.component';
import { AuthGuard } from '../guard/auth.guard';
export const BeneficiaryRoutes: Routes = [
  {
    path: '',
    component: BeneficiaryComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './beneficiary-index/beneficiary-index.module#BeneficiaryIndexModule',
        data: {component: 'beneficiary', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './beneficiary-view/beneficiary-view.module#BeneficiaryViewModule',
        data: {component: 'beneficiary', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './beneficiary-form/beneficiary-form.module#BeneficiaryFormModule',
        data: {component: 'beneficiary', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BeneficiaryRoutes)
  ],
  declarations: [BeneficiaryComponent]
})
export class BeneficiaryModule { }
