import { Component, OnInit } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import {} from 'googlemaps';
import { ViewChild, ElementRef, NgZone, } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Apisendpoints } from '../../../apisendpoints';
import { BeneficiaryService } from '../../../services/beneficiary/beneficiary.service';
import { VillageService } from '../../../services/masters/village/village.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WhitespaceValidator } from '../../../validators/whitespace.validator';
import { mobileValidator } from '../../../validators/mobile.validator';
import * as moment from 'moment';

declare const $:any;

@Component({
  selector: 'app-beneficiary-form',
  templateUrl: './beneficiary-form.component.html',
  styleUrls: ['./beneficiary-form.component.css']
})
export class BeneficiaryFormComponent implements OnInit {
  beneficiaryForm: FormGroup;
  submitted = false;
  beneficiaryData:any;
  villages:any = {};
  id:any;
  selectedFiles: FileList;

  via:string = "";
  routing:string = "";
  savedData:any;

  imageUploadData:any;
  fileData:any;

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  
	@ViewChild('search' ) public searchElement: ElementRef;

  constructor( private villageService: VillageService, 
              private router: Router , 
              private beneficiaryService: BeneficiaryService,
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute,
              private mapsAPILoader: MapsAPILoader, 
              private ngZone: NgZone) { }

  ngOnInit() {

    $('#dob-date').datepicker({
      changeMonth: true, 
      maxDate: 0,
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.beneficiaryForm.patchValue({
          dob : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    this.getVillages();

    this.beneficiaryForm = this.createFormGroup();

    //create search FormControl
    this.searchControl = new FormControl();

    this.route.queryParams.subscribe((params)=>{
      if((params['data'] && params.via != 'activity') || params.id){
        if(params.id){
          this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + '/' + params.id).subscribe((res)=>{
            this.beneficiaryData = res['data'];
            this.setFormData();
          });
        }else{
          this.beneficiaryData = JSON.parse(params['data']);
          this.setFormData();
        }
        
      }else if(params.via == 'activity'){
        this.savedData = params.data
        this.routing = params.route;
        this.via = params.via;
        this.beneficiaryForm.patchValue({
          villageId : params.village
        });
        this.setCurrentPosition();
      }else{
        //current location
        this.setCurrentPosition();
      }
    });

    this.searchLocations();
  }

  setFormData(){
    this.latitude = +this.beneficiaryData['location'].lat;
        this.longitude = +this.beneficiaryData['location'].lng;
        this.zoom = 12;
        this.id = this.beneficiaryData['_id'];
        this.beneficiaryForm.setValue({ 
          name : this.beneficiaryData['name'],
          location:{
            lat: this.beneficiaryData['location'].lat,
            lng : this.beneficiaryData['location'].lng,
          },
          fatherName : this.beneficiaryData['fatherName'],
          villageId:  this.beneficiaryData['villageId']._id,
          mobile: this.beneficiaryData['mobile'],
          dob : this.beneficiaryData['dob'] ? moment(this.beneficiaryData['dob']).format('YYYY-MM-DD') : "",
          gender : this.beneficiaryData['gender'],
          socialCategory : this.beneficiaryData['socialCategory'],
          photo : this.beneficiaryData['photo'],
          bplFamily : this.beneficiaryData['bplFamily']
        });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      fatherName : new FormControl('',[WhitespaceValidator, Validators.required]),
      villageId : new FormControl('',Validators.required),
      mobile : new FormControl('',[mobileValidator]),
      gender : new FormControl('',Validators.required),
      dob : new FormControl(''),
      socialCategory : new FormControl('',Validators.required),
      photo : new FormControl(''),
      location : new FormGroup({
        lat : new FormControl(''),
        lng : new FormControl('')
      }),
      bplFamily:new FormControl('')
    })
  }

  goBack(){
    if(this.via){
      this.router.navigate([this.routing],{
        queryParams : {
          data : this.savedData
        }
      });
    }else{
      this.router.navigate(['beneficiary/index']);
    }
   
  }

  onSubmit(value) { // Submit Form ..
    this.beneficiaryForm.setErrors({invalid : true});
    if(this.id){   
      this.beneficiaryService.putBeneficiary(this.baseUrl.putBeneficiary + this.id, this.beneficiaryForm.value ).subscribe( res=> { 
        if(res['success']){
          //image upload
          if(this.imageUploadData){
            this.saveImageApi();
          }

          if(this.via == "activity"){
            this.router.navigate([this.routing],{
              queryParams : {
                data : this.savedData
              }
            });
          }else{
            this.router.navigate(['beneficiary/index']);
          }
        }
        this.beneficiaryForm.setErrors(null);
      },(error) => {
        this.beneficiaryForm.setErrors(null);
        console.log(error);
      });
    }else{ 
      this.beneficiaryService.postBeneficiary(this.baseUrl.postBeneficiary, this.beneficiaryForm.value ).subscribe( response=> { 
        if(response['success']){
          //image upload
          if(this.imageUploadData){
            this.saveImageApi();
          }

          if(this.via == "activity"){
            this.router.navigate([this.routing],{
              queryParams : {
                data : this.savedData
              }
            });
          }else{
            this.router.navigate(['beneficiary/index']);
          }
        }
        this.beneficiaryForm.setErrors(null);
      },(error) => {
        this.beneficiaryForm.setErrors(null);
        console.log(error);
      });
    }
      

      // if (this.beneficiaryForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.beneficiaryForm.value))
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe( res=> { 
      this.villages = res;
    });
  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {// Reset Values .. 
    this.beneficiaryForm.reset();
  }

  setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.beneficiaryForm.patchValue({
          location :{
            lat : position.coords.latitude,
            lng : position.coords.longitude
          }
        });
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  searchLocations(){
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.beneficiaryForm.patchValue({
            location :{
              lat : this.latitude,
              lng : this.longitude
            }
          });
        });
      });
    });
  }

  markerDragEnd(event){
    this.longitude = event.coords.lng;
    this.latitude = event.coords.lat;

    this.beneficiaryForm.patchValue({
      location: {
        lat : this.latitude,
        lng : this.longitude
      }
    });
  }

  saveImageApi(){
    this.beneficiaryService.postS3Url(this.imageUploadData['data'].images[0].url, this.fileData).subscribe((data)=>{
      //
    });
  }

  onFileChange(event) {
    this.selectedFiles = event.target.files;
    const file = this.selectedFiles.item(0); // <--- File Object for future use.

    this.fileData = file;
    
    this.beneficiaryForm.controls['photo'].setValue(file ? file.name : ''); // <-- Set Value for Validation

    let image = {
      images : [{
        "file_name" : file.name,
        "mime_type" : file.type
      }],
      videos: []
  }
   
    this.beneficiaryService.getS3Url(this.baseUrl.getS3Url, image).subscribe((res)=>{
      this.beneficiaryForm.patchValue({
        photo : res['data'].images[0].file_url
      })

      this.imageUploadData = res;
    });
  }

}
