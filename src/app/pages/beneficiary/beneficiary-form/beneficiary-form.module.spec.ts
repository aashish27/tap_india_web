import { BeneficiaryFormModule } from './beneficiary-form.module';

describe('BeneficiaryFormModule', () => {
  let beneficiaryFormModule: BeneficiaryFormModule;

  beforeEach(() => {
    beneficiaryFormModule = new BeneficiaryFormModule();
  });

  it('should create an instance', () => {
    expect(beneficiaryFormModule).toBeTruthy();
  });
});
