import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BeneficiaryFormComponent } from './beneficiary-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../../../shared/shared.module';

export const BeneficiaryFormRoutes: Routes = [
  {
    path: '',
    component: BeneficiaryFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot(  {
      apiKey: 'AIzaSyAqYv39bL0fEwS8FwPde01x_iernmQVvGw',
	   libraries: ["places"]
    }),
    SharedModule,
    RouterModule.forChild(BeneficiaryFormRoutes),
  ],
  declarations: [BeneficiaryFormComponent]
})
export class BeneficiaryFormModule { }