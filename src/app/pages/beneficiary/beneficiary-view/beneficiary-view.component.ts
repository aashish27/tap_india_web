import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../apisendpoints';
import { AuthGuard } from '../../guard/auth.guard';
import { BeneficiaryService } from '../../../services/beneficiary/beneficiary.service';

@Component({
  selector: 'app-beneficiary-view',
  templateUrl: './beneficiary-view.component.html',
  styleUrls: ['./beneficiary-view.component.css']
})
export class BeneficiaryViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private beneficairyService:BeneficiaryService, private router: Router, private baseUrl: Apisendpoints) { }
  beneficiaryData:any;;
  delete_id:any = "";
  url:string = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.beneficairyService.getBeneficiary(this.baseUrl.getBeneficiary + '/' + params.id).subscribe((res)=>{
          this.beneficiaryData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['beneficiary/form'], {queryParams: {id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'beneficiary', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteBeneficiary + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['beneficiary/index']);
    }
  }
}
