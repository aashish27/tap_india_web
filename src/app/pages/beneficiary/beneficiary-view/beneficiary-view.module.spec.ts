import { BeneficiaryViewModule } from './beneficiary-view.module';

describe('BeneficiaryViewModule', () => {
  let beneficiaryViewModule: BeneficiaryViewModule;

  beforeEach(() => {
    beneficiaryViewModule = new BeneficiaryViewModule();
  });

  it('should create an instance', () => {
    expect(beneficiaryViewModule).toBeTruthy();
  });
});
