import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BeneficiaryViewComponent } from './beneficiary-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PopupModule } from '../../../shared/popup/popup.module';

export const BeneficiaryViewRoutes: Routes = [
  {
    path: '',
    component: BeneficiaryViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PopupModule,
    RouterModule.forChild(BeneficiaryViewRoutes),
  ],
  declarations: [BeneficiaryViewComponent]
})
export class BeneficiaryViewModule { }