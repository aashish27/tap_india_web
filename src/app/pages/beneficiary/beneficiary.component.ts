import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-beneficiary',
  template: '<router-outlet></router-outlet>'
})
export class BeneficiaryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
