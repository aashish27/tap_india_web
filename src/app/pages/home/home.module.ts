import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { NgxPaginationModule } from 'ngx-pagination';
export const HomeRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(HomeRoutes),
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
