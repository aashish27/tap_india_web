import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login.component';
import {SharedModule} from '../../shared/shared.module';
import {ToastyModule} from 'ng2-toasty';
export const LoginRoutes: Routes = [
  {
    path: '',
    component: LoginComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    FormsModule,
    SharedModule,
    ReactiveFormsModule, ToastyModule.forRoot()
  ],
  declarations: [LoginComponent]
})
export class LoginModule {
}
