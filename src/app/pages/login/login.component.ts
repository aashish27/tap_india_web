import {Component, OnInit, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth/auth.service';

import {AdminComponent} from '../../layout/admin/admin.component';
import {MenuItems} from '../../shared/menu-items/menu-items';
import {ToasterService} from '../../services/toaster/toaster.service';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';
import { CommonFunctionService } from '../../services/common-service/common-service.service';
import { UserLevelService } from '../../services/user/user-level/user-level.service';
import { EncrDecrService } from '../../services/encr-decr/encr-decr.service';
import Swiper from 'swiper';
declare var $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
    data: any;
    typeDrop: any;
    loader = false;
    chosenOption;
    message;
    menu_items: any = [];
    submitButton: string;
    buttonStatus: boolean;
    bntStyle: string;
    position = 'top-right';

    constructor(
        private toastyService: ToastyService,
        private toaster: ToasterService,
        private userService: AuthService,
        public menuItems: MenuItems,
        public adminConponent: AdminComponent,
        private common: CommonFunctionService,
        private router: Router,
        private userLevelService: UserLevelService,
        private encrDecr: EncrDecrService) {
    }

    ngOnInit() {
        this.buttonStatus = false;
        this.bntStyle = 'btn btn-info';
        this.submitButton = 'Log In';
        // var encrypted = this.encrDecr.set('123456$#@$^@1ERF', 'password@123456');
        // var decrypted = this.encrDecr.get('123456$#@$^@1ERF', encrypted);
        // console.log('Encrypted :' + encrypted);
        // console.log('Encrypted :' + decrypted);
    }

    showToast(options) { // Show Toaster ..
        let toastOptions = this.toaster.getToastData(options);
        this.position = options.position ? options.position : this.position;
        switch (options.type) {
            case 'default':
                this.toastyService.default(toastOptions);
                break;
            case 'info':
                this.toastyService.info(toastOptions);
                break;
            case 'success':
                this.toastyService.success(toastOptions);
                break;
            case 'wait':
                this.toastyService.wait(toastOptions);
                break;
            case 'error':
                this.toastyService.error(toastOptions);
                break;
            case 'warning':
                this.toastyService.warning(toastOptions);
                break;
        }
    }


    async login(form) { // Login
        this.buttonStatus = true;
        const loginData = form.value;
        loginData['platform'] = "web";
        this.submitButton = 'Loading ...';        
        try {
            const response = await this.userService.login(loginData).toPromise();
            this.toastyService.clearAll();
            this.showToast({
                title: 'Loading',
                msg: 'Please wait ...',
                timeout: 50000,
                theme: 'material',
                position: 'top-right',
                type: 'wait'
            });
            if (response['success'] === true) {
                // const data = response;
                // const usrData = response['data'];
                // sessionStorage.setItem('token', data['token']);
                // sessionStorage.setItem('userData', JSON.stringify(usrData));
                // let pms1 = new Promise((resolve, reject)=>{
                //     this.userLevelService.getUserLevel("projectmodule/listwithaction?userLevelId="+usrData.userLevelId).subscribe((res) => {
                //         var encryptedAccessModules = this.encrDecr.set('123456$#@$^@1ERF', JSON.stringify(res));
                //         sessionStorage.setItem('accessModules', encryptedAccessModules)
                //         resolve("resolve");
                //   })  
                // });

                // pms1.then((result)=>{
                //     this.common.setActivities('default.activity').subscribe((res)=>{
                //         if(res['success']){
                //             sessionStorage.setItem('activity', JSON.stringify(res['data']));
                //             if(!usrData.isPasswordChangedOnce){
                //                 this.router.navigate(['user/user-password']);
                //             }else{
                //                 this.router.navigate(['home']);
                //             }
                           
                //         }
                //     });
                // })
                this.router.navigate(['home']);

            } else {
                this.toastyService.clearAll();
                this.showToast({
                    title: 'Invalid Credential',
                    msg: 'Check your Username or Password !',
                    timeout: 5000,
                    theme: 'bootstrap',
                    position: this.position,
                    type: 'warning'
                });
            }
        } catch (error) {
            console.log("Error",error);
            this.bntStyle = 'btn btn-info';
            this.buttonStatus = false;
            this.submitButton = 'Log In';
            this.message = 'Check your Username or Password !';
            this.toastyService.clearAll();
            this.showToast({
                title: 'Invalid Credential',
                msg: 'Check your Username or Password !',
                timeout: 5000,
                theme: 'bootstrap',
                position: this.position,
                type: 'warning'
            });

        }
    }

    ngAfterViewInit(): void {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        var swiper = new Swiper('.swiper-container', {
            spaceBetween: 3000,
            centeredSlides: true,
            fadeEffect: {
                crossFade: true
            },
            effect: 'fade',
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            }
        });
    }
}

