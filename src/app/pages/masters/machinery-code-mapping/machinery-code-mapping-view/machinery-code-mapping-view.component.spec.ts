import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryCodeMappingViewComponent } from './machinery-code-mapping-view.component';

describe('MachineryCodeMappingViewComponent', () => {
  let component: MachineryCodeMappingViewComponent;
  let fixture: ComponentFixture<MachineryCodeMappingViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryCodeMappingViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryCodeMappingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
