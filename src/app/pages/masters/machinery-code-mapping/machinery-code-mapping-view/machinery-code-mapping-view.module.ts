import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryCodeMappingViewComponent } from './machinery-code-mapping-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const MachineryCodeMappingFormRoutes: Routes = [
  {
    path: '',
    component: MachineryCodeMappingViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(MachineryCodeMappingFormRoutes),
  ],
  declarations: [MachineryCodeMappingViewComponent]
})
export class MachineryCodeMappingViewModule { }
