import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { MachineryCodeService } from '../../../../services/masters/machineryCode/machinery-code.service';

@Component({
  selector: 'app-machinery-code-mapping-view',
  templateUrl: './machinery-code-mapping-view.component.html',
  styleUrls: ['./machinery-code-mapping-view.component.css']
})
export class MachineryCodeMappingViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private machineryCodeService:MachineryCodeService, private router: Router, private baseUrl: Apisendpoints) { }
  codeMapData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.machineryCodeService.getMachineryCode(this.baseUrl.getMachineryCode + '/' + params.id).subscribe((res)=>{
          this.codeMapData = res['data'];
        });
      }
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/machinery-code-mapping/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery-code-mapping', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteMachineryCode + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/machinery-code-mapping/index']);
    }
  }
}
