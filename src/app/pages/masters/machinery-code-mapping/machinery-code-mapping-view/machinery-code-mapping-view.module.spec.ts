import { MachineryCodeMappingViewModule } from './machinery-code-mapping-view.module';

describe('MachineryCodeMappingViewModule', () => {
  let machineryCodeMappingViewModule: MachineryCodeMappingViewModule;

  beforeEach(() => {
    machineryCodeMappingViewModule = new MachineryCodeMappingViewModule();
  });

  it('should create an instance', () => {
    expect(machineryCodeMappingViewModule).toBeTruthy();
  });
});
