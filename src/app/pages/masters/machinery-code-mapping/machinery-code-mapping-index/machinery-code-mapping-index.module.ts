import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryCodeMappingIndexComponent } from './machinery-code-mapping-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const MachineryCodeMappingIndexRoutes: Routes = [
  {
    path: '',
    component: MachineryCodeMappingIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(MachineryCodeMappingIndexRoutes),
  ],
  declarations: [MachineryCodeMappingIndexComponent]
})
export class MachineryCodeMappingIndexModule { }
