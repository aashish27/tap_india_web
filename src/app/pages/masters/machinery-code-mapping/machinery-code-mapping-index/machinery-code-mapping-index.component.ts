import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { MachineryCodeService } from '../../../../services/masters/machineryCode/machinery-code.service';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-machinery-code-mapping-index',
  templateUrl: './machinery-code-mapping-index.component.html',
  styleUrls: ['./machinery-code-mapping-index.component.css']
})
export class MachineryCodeMappingIndexComponent implements OnInit {
  message = '';
  codeMap : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  constructor(private machineryCodeService: MachineryCodeService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();

  }

  get(){ // Get MachineryCode data ..
    var self = this;
    this.machineryCodeService.getMachineryCode(this.baseUrl.getMachineryCode).subscribe( res=> { 
      this.codeMap = res;
      if(this.codeMap.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery-code-mapping', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteMachineryCode + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/machinery-code-mapping/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/machinery-code-mapping/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Mahcinery-code-mapping-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4,5]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
      this.table = $('#machineryCodeTable').DataTable(this.dtoptions);
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery-code-mapping', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
