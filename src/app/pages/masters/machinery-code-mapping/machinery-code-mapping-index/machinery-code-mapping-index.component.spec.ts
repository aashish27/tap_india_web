import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryCodeMappingIndexComponent } from './machinery-code-mapping-index.component';

describe('MachineryCodeMappingIndexComponent', () => {
  let component: MachineryCodeMappingIndexComponent;
  let fixture: ComponentFixture<MachineryCodeMappingIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryCodeMappingIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryCodeMappingIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
