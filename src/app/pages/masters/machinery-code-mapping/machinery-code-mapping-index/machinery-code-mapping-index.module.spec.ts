import { MachineryCodeMappingIndexModule } from './machinery-code-mapping-index.module';

describe('MachineryCodeMappingIndexModule', () => {
  let machineryCodeMappingIndexModule: MachineryCodeMappingIndexModule;

  beforeEach(() => {
    machineryCodeMappingIndexModule = new MachineryCodeMappingIndexModule();
  });

  it('should create an instance', () => {
    expect(machineryCodeMappingIndexModule).toBeTruthy();
  });
});
