import { MachineryCodeMappingModule } from './machinery-code-mapping.module';

describe('MachineryCodeMappingModule', () => {
  let machineryCodeMappingModule: MachineryCodeMappingModule;

  beforeEach(() => {
    machineryCodeMappingModule = new MachineryCodeMappingModule();
  });

  it('should create an instance', () => {
    expect(machineryCodeMappingModule).toBeTruthy();
  });
});
