import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-machinery-code-mapping',
  template: '<router-outlet></router-outlet>'
})
export class MachineryCodeMappingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
