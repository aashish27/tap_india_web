import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryCodeMappingComponent } from './machinery-code-mapping.component';

describe('MachineryCodeMappingComponent', () => {
  let component: MachineryCodeMappingComponent;
  let fixture: ComponentFixture<MachineryCodeMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryCodeMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryCodeMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
