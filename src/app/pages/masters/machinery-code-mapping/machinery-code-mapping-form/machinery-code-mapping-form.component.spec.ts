import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryCodeMappingFormComponent } from './machinery-code-mapping-form.component';

describe('MachineryCodeMappingFormComponent', () => {
  let component: MachineryCodeMappingFormComponent;
  let fixture: ComponentFixture<MachineryCodeMappingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryCodeMappingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryCodeMappingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
