import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryCodeMappingFormComponent } from './machinery-code-mapping-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const MachineryCodeMappingFormRoutes: Routes = [
  {
    path: '',
    component: MachineryCodeMappingFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(MachineryCodeMappingFormRoutes),
  ],
  declarations: [MachineryCodeMappingFormComponent]
})
export class MachineryCodeMappingFormModule { }
