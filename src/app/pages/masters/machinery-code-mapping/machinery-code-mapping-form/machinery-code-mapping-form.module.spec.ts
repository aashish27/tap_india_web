import { MachineryCodeMappingFormModule } from './machinery-code-mapping-form.module';

describe('MachineryCodeMappingFormModule', () => {
  let machineryCodeMappingFormModule: MachineryCodeMappingFormModule;

  beforeEach(() => {
    machineryCodeMappingFormModule = new MachineryCodeMappingFormModule();
  });

  it('should create an instance', () => {
    expect(machineryCodeMappingFormModule).toBeTruthy();
  });
});
