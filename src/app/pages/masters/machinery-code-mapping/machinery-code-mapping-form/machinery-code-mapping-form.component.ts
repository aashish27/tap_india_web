import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MachineryCodeService } from '../../../../services/masters/machineryCode/machinery-code.service';
import { MachineryService } from '../../../../services/masters/machinery/machinery.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-machinery-code-mapping-form',
  templateUrl: './machinery-code-mapping-form.component.html',
  styleUrls: ['./machinery-code-mapping-form.component.css']
})
export class MachineryCodeMappingFormComponent implements OnInit {
  codeMappingForm: FormGroup;
  submitted = false;
  machineData:any;
  codeMachine:any = {};
  id: any;

  constructor(private formBuilder: FormBuilder, 
              private machineryCodeService: MachineryCodeService, 
              private machineryService: MachineryService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.codeMappingForm = this.createFormGroup();

    this.route.queryParams.subscribe((params)=>{
        if(params.id){
          this.machineryCodeService.getMachineryCode(this.baseUrl.getMachineryCode + '/' + params.id).subscribe((res)=>{
            this.machineData = res['data'];
            this.id = this.machineData['_id'];
            this.codeMappingForm.setValue({ 
                machineryId : this.machineData['machineryId']._id
            });
          });
        }
    });

    this.getMachinery();
  }

  getMachinery(){
    this.machineryService.getMachinery(this.baseUrl.getMachinery).subscribe( res=> { 
      this.codeMachine = res;
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      machineryId : new FormControl('',Validators.required)
    })
  }

  onSubmit(value) { // Submit Form ..
    this.codeMappingForm.setErrors({invalid : true});
      if(this.id){  
        this.machineryCodeService.putMachineryCode(this.baseUrl.putMachineryCode + this.id, this.codeMappingForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/machinery-code-mapping/index']);
          }
         this.codeMappingForm.setErrors(null);
        },(error) => {
         this.codeMappingForm.setErrors(null);
          console.log(error);
        });
      }else{   
        this.machineryCodeService.postMachineryCode(this.baseUrl.postMachineryCode, this.codeMappingForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/machinery-code-mapping/index']);
          }
         this.codeMappingForm.setErrors(null);
        },(error) => {
         this.codeMappingForm.setErrors(null);
          console.log(error);
        });
      }

      // if (this.codeMappingForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.codeMappingForm.value))
  }

  revert() {// Reset Values .. 
    this.codeMappingForm.setValue({
      machineryId : ""
    });
  }

}


