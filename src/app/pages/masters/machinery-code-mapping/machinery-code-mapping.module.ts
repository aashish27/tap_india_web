import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryCodeMappingComponent } from './machinery-code-mapping.component';
import { AuthGuard } from '../../guard/auth.guard';
export const MachineryCodeMappingRoutes: Routes = [
  {
    path: '',
    component: MachineryCodeMappingComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './machinery-code-mapping-index/machinery-code-mapping-index.module#MachineryCodeMappingIndexModule',
        data: {component: 'machinery-code-mapping', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './machinery-code-mapping-view/machinery-code-mapping-view.module#MachineryCodeMappingViewModule',
        data: {component: 'machinery-code-mapping', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './machinery-code-mapping-form/machinery-code-mapping-form.module#MachineryCodeMappingFormModule',
        data: {component: 'machinery-code-mapping', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MachineryCodeMappingRoutes)
  ],
  declarations: [MachineryCodeMappingComponent]
})
export class MachineryCodeMappingModule { }
