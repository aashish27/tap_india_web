import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VarietyIndexComponent } from './variety-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const VarietyIndexRoutes: Routes = [
  {
    path: '',
    component: VarietyIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(VarietyIndexRoutes),
  ],
  declarations: [VarietyIndexComponent]
})
export class VarietyIndexModule { }