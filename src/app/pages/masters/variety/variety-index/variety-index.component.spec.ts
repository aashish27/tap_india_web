import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarietyIndexComponent } from './variety-index.component';

describe('VarietyIndexComponent', () => {
  let component: VarietyIndexComponent;
  let fixture: ComponentFixture<VarietyIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarietyIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarietyIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
