import { Component, OnInit } from '@angular/core';
import { Apisendpoints } from '../../../../apisendpoints';
import { VarietyService } from '../../../../services/masters/variety/variety.service';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-variety-index',
  templateUrl: './variety-index.component.html',
  styleUrls: ['./variety-index.component.css']
})
export class VarietyIndexComponent implements OnInit {
  variety : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private varietyService: VarietyService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();

  }

  get(){ // Get Variety data ..
    var self = this;
    this.varietyService.getVariety(this.baseUrl.getVariety).subscribe( res=> { 
      this.variety  = res;
      if(this.variety.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'variety', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteVariety + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/variety/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/variety/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Varities-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
      this.table = $('#varietyTable').DataTable(this.dtoptions);
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'variety', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
