import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { VarietyService } from '../../../../services/masters/variety/variety.service';

@Component({
  selector: 'app-variety-view',
  templateUrl: './variety-view.component.html',
  styleUrls: ['./variety-view.component.css']
})
export class VarietyViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private varietyService:VarietyService, private authGuard: AuthGuard, private baseUrl: Apisendpoints) { }
  
  varietyData:any;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.varietyService.getVariety(this.baseUrl.getVariety + '/' + params.id).subscribe((res)=>{
          this.varietyData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/variety/form'], {queryParams: {id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'variety', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteVariety + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/variety/index']);
    }
  }
}
