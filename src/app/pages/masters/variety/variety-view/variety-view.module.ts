import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VarietyViewComponent } from './variety-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const VarietyViewRoutes: Routes = [
  {
    path: '',
    component: VarietyViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(VarietyViewRoutes),
  ],
  declarations: [VarietyViewComponent]
})
export class VarietyViewModule { }