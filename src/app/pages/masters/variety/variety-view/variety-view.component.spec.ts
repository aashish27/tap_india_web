import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VarietyViewComponent } from './variety-view.component';

describe('VarietyViewComponent', () => {
  let component: VarietyViewComponent;
  let fixture: ComponentFixture<VarietyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VarietyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VarietyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
