import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-variety',
  template: '<router-outlet></router-outlet>'
})
export class VarietyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
