import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VarietyFormComponent } from './variety-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const VarietyFormRoutes: Routes = [
  {
    path: '',
    component: VarietyFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(VarietyFormRoutes),
  ],
  declarations: [VarietyFormComponent]
})
export class VarietyFormModule { }