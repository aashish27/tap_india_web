import { VarietyFormModule } from './variety-form.module';

describe('VarietyFormModule', () => {
  let varietyFormModule: VarietyFormModule;

  beforeEach(() => {
    varietyFormModule = new VarietyFormModule();
  });

  it('should create an instance', () => {
    expect(varietyFormModule).toBeTruthy();
  });
});
