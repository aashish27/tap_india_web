import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Apisendpoints } from '../../../../apisendpoints';
import { VarietyService } from '../../../../services/masters/variety/variety.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';

@Component({
  selector: 'app-variety-form',
  templateUrl: './variety-form.component.html',
  styleUrls: ['./variety-form.component.css']
})
export class VarietyFormComponent implements OnInit {
  varietyForm: FormGroup;
  submitted = false;
  varietyData:any;
  crops:any = {};
  id: any;

  constructor( private varietyService: VarietyService, 
              private router: Router , 
              private cropService: CropService,
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getCrops();
    this.varietyForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.varietyService.getVariety(this.baseUrl.getVariety + '/' + params.id).subscribe((res)=>{
          this.varietyData = res['data'];
          this.id = this.varietyData['_id'];
          this.varietyForm.setValue({ 
            name : this.varietyData['name'],
            cropId : this.varietyData['cropId']._id      
          });
        });
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      cropId : new FormControl('',Validators.required)
    })
  }

  onSubmit(value) { // Submit Form ..
    this.varietyForm.setErrors({invalid : true});
      if(this.id){   
        this.varietyService.putVariety(this.baseUrl.putVariety + this.id, this.varietyForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/variety/index']);
          }
          this.varietyForm.setErrors(null);
        },(error) => {
          this.varietyForm.setErrors(null);
          console.log(error);
        });
      }else{ 
        this.varietyService.postVariety(this.baseUrl.postVariety, this.varietyForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/variety/index']);
          }
          this.varietyForm.setErrors(null);
        },(error) => {
          this.varietyForm.setErrors(null);
          console.log(error);
        });
      }

      // if (this.varietyForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.varietyForm.value))
  }

  getCrops(){
    this.cropService.getCrops(this.baseUrl.getCrop).subscribe( res=> { 
      this.crops = res;
    });
  }

  revert() {// Reset Values .. 
    this.varietyForm.setValue({
      name : "",
      cropId : ""
    });
  }
}
