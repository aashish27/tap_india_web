import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VarietyComponent } from './variety.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';
export const VarietyRoutes: Routes = [
  {
    path: '',
    component: VarietyComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './variety-index/variety-index.module#VarietyIndexModule',
        data: {component: 'variety', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './variety-view/variety-view.module#VarietyViewModule',
        data: {component: 'variety', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './variety-form/variety-form.module#VarietyFormModule',
        data: {component: 'variety', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VarietyRoutes)
  ],
  declarations: [VarietyComponent]
})
export class VarietyModule { }
