import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MastersComponent } from './masters.component';
export const MastersRoutes: Routes = [
  {
    path: '',
    component: MastersComponent,
    children: [
      {
        path: 'district',
        loadChildren: './district/district.module#DistrictModule'
      },{
        path: 'block',
        loadChildren: './block/block.module#BlockModule'
      },{
        path: 'village',
        loadChildren: './village/village.module#VillageModule'
      },{
        path: 'activity',
        loadChildren: './activity/activity.module#ActivityModule'
      },{
        path: 'activity-target',
        loadChildren: './activity-target/activity-target.module#ActivityTargetModule'
      },{
        path: 'crop',
        loadChildren: './crop/crop.module#CropModule'
      },{
        path: 'variety',
        loadChildren: './variety/variety.module#VarietyModule'
      },{
        path: 'machinery',
        loadChildren: './machinery/machinery.module#MachineryModule'
      },{
        path: 'machinery-code-mapping',
        loadChildren: './machinery-code-mapping/machinery-code-mapping.module#MachineryCodeMappingModule'
      },{
        path: 'machinery-owner',
        loadChildren: './machinery-owner/machinery-owner.module#MachineryOwnerModule'
      },{
        path: 'capacity-building-topic',
        loadChildren: './capacity-building-topic/capacity-building-topic.module#CapacityBuildingTopicModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MastersRoutes),
  ],
  declarations: [MastersComponent]
})
export class MastersModule { }
