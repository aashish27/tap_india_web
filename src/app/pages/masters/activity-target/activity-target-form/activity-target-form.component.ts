import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityTargetService } from '../../../../services/masters/activity-target/activity-target.service';
import { ActivityListService } from '../../../../services/masters/activity-list/activity-list.service';
import { Apisendpoints } from '../../../../apisendpoints';

declare const $:any;

@Component({
  selector: 'app-activity-target-form',
  templateUrl: './activity-target-form.component.html',
  styleUrls: ['./activity-target-form.component.css']
})
export class ActivityTargetFormComponent implements OnInit {
  activityForm: FormGroup;
  submitted = false;
  activityData:any;
  activities:any = {};
  id: any;

  constructor( private activityService: ActivityTargetService,
              private activityListService: ActivityListService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getActivities();
    this.activityForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.activityService.getActivityTargetList(this.baseUrl.getActivityTarget + '/' + params.id).subscribe((res)=>{
          this.activityData = res['data'];
          this.id = this.activityData['_id'];
          this.activityForm.setValue({ 
            activityId : this.activityData['activityId']._id,
            target_activity : this.activityData['target_activity'],
            target_beneficiary : this.activityData['target_beneficiary']   
          });
        });
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      activityId : new FormControl('',Validators.required),
      target_activity : new FormControl('',Validators.required),
      target_beneficiary : new FormControl('',Validators.required)
    })
  }

  onSubmit(value) { // Submit Form ..
    this.activityForm.setErrors({invalid : true});
      if(this.id){
        this.activityService.putActivityTargetList(this.baseUrl.putActivityTarget + this.id, this.activityForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/activity-target/index']);
          }
          this.activityForm.setErrors(null);
        },(error) => {
          this.activityForm.setErrors(null);
          console.log(error);
        });
      }else {  
        this.activityService.postActivityTargetList(this.baseUrl.postActivityTarget, this.activityForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/activity-target/index']);
          }
          this.activityForm.setErrors(null);
        },(error) => {
          this.activityForm.setErrors(null);
          console.log(error);
        });
      }
      // if (this.activityForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.activityForm.value))
  }

  getActivities(){
    this.activityListService.getActivityList(this.baseUrl.getActivity).subscribe( res=> { 
      this.activities = res;
    });
  }
  

  revert() {// Reset Values .. 
    this.activityForm.setValue({
      activityId : "",
      target_activity : "",
      target_beneficiary : ""
    });
  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

}
