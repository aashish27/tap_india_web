import { ActivityTargetFormModule } from './activity-target-form.module';

describe('ActivityTargetFormModule', () => {
  let activityTargetFormModule: ActivityTargetFormModule;

  beforeEach(() => {
    activityTargetFormModule = new ActivityTargetFormModule();
  });

  it('should create an instance', () => {
    expect(activityTargetFormModule).toBeTruthy();
  });
});
