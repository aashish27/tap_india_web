import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityTargetFormComponent } from './activity-target-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const ActivityTargetFormRoutes: Routes = [
  {
    path: '',
    component: ActivityTargetFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(ActivityTargetFormRoutes),
  ],
  declarations: [ActivityTargetFormComponent]
})
export class ActivityTargetFormModule { }