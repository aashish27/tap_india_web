import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTargetFormComponent } from './activity-target-form.component';

describe('ActivityTargetFormComponent', () => {
  let component: ActivityTargetFormComponent;
  let fixture: ComponentFixture<ActivityTargetFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityTargetFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityTargetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
