import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { ActivityTargetService } from '../../../../services/masters/activity-target/activity-target.service';

@Component({
  selector: 'app-activity-target-view',
  templateUrl: './activity-target-view.component.html',
  styleUrls: ['./activity-target-view.component.css']
})
export class ActivityTargetViewComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private activityService: ActivityTargetService, private authGuard: AuthGuard, private baseUrl: Apisendpoints) { }
  targetData:any;;
  table:any;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.activityService.getActivityTargetList(this.baseUrl.getActivityTarget + '/' + params.id).subscribe((res)=>{
          this.targetData = res['data'];
        });
      }
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/activity-target/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'activity-target', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteActivityTarget + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/activity-target/index']);
    }
  }
  

}
