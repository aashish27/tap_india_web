import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTargetViewComponent } from './activity-target-view.component';

describe('ActivityTargetViewComponent', () => {
  let component: ActivityTargetViewComponent;
  let fixture: ComponentFixture<ActivityTargetViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityTargetViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityTargetViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
