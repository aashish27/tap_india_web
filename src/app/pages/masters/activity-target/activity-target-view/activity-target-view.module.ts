import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityTargetViewComponent } from './activity-target-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const ActivityTargetViewRoutes: Routes = [
  {
    path: '',
    component: ActivityTargetViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ActivityTargetViewRoutes),
  ],
  declarations: [ActivityTargetViewComponent]
})
export class ActivityTargetViewModule { }