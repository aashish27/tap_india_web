import { ActivityTargetViewModule } from './activity-target-view.module';

describe('ActivityTargetViewModule', () => {
  let activityTargetViewModule: ActivityTargetViewModule;

  beforeEach(() => {
    activityTargetViewModule = new ActivityTargetViewModule();
  });

  it('should create an instance', () => {
    expect(activityTargetViewModule).toBeTruthy();
  });
});
