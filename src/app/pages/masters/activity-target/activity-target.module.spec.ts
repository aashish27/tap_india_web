import { ActivityTargetModule } from './activity-target.module';

describe('ActivityTargetModule', () => {
  let activityTargetModule: ActivityTargetModule;

  beforeEach(() => {
    activityTargetModule = new ActivityTargetModule();
  });

  it('should create an instance', () => {
    expect(activityTargetModule).toBeTruthy();
  });
});
