import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityTargetComponent } from './activity-target.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';
export const ActivityTargetRoutes: Routes = [
{
    path: '',
    component: ActivityTargetComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './activity-target-index/activity-target-index.module#ActivityTargetIndexModule',
        data: {component: 'activity-target', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './activity-target-view/activity-target-view.module#ActivityTargetViewModule',
        data: {component: 'activity-target', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './activity-target-form/activity-target-form.module#ActivityTargetFormModule',
        data: {component: 'activity-target', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivityTargetRoutes)
  ],
  declarations: [ActivityTargetComponent]
})
export class ActivityTargetModule { }
