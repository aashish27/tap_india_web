import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-activity-target',
  template: '<router-outlet></router-outlet>'
})
export class ActivityTargetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
