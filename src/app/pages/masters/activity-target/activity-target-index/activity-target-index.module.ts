import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityTargetIndexComponent } from './activity-target-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const ActivityTargetIndexRoutes: Routes = [
  {
    path: '',
    component: ActivityTargetIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ActivityTargetIndexRoutes),
  ],
  declarations: [ActivityTargetIndexComponent]
})
export class ActivityTargetIndexModule { }