import { ActivityTargetIndexModule } from './activity-target-index.module';

describe('ActivityTargetIndexModule', () => {
  let activityTargetIndexModule: ActivityTargetIndexModule;

  beforeEach(() => {
    activityTargetIndexModule = new ActivityTargetIndexModule();
  });

  it('should create an instance', () => {
    expect(activityTargetIndexModule).toBeTruthy();
  });
});
