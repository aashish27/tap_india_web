import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTargetIndexComponent } from './activity-target-index.component';

describe('ActivityTargetIndexComponent', () => {
  let component: ActivityTargetIndexComponent;
  let fixture: ComponentFixture<ActivityTargetIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityTargetIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityTargetIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
