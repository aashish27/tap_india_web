import { Component, OnInit } from '@angular/core';
import { ActivityTargetService } from '../../../../services/masters/activity-target/activity-target.service'; 
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-activity-target-index',
  templateUrl: './activity-target-index.component.html',
  styleUrls: ['./activity-target-index.component.css']
})
export class ActivityTargetIndexComponent implements OnInit {
  activity : any = {};
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  tableHideShowStatus: boolean = false;

  constructor(private activityService: ActivityTargetService,  private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.activityService.getActivityTargetList(this.baseUrl.getActivityTarget).subscribe( res=> { 
      this.activity  = res;
      if(this.activity.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'activity-target', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteActivityTarget + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/activity-target/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/activity-target/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Acitivty-target-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4,5,6]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    }
    this.table = $('#activityTargetTable').DataTable(this.dtoptions);
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'activity-target', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
