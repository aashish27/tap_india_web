import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CropComponent } from './crop.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';
export const CropRoutes: Routes = [
  {
    path: '',
    component: CropComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './crop-index/crop-index.module#CropIndexModule',
        data: {component: 'crop', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './crop-view/crop-view.module#CropViewModule',
        data: {component: 'crop', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './crop-form/crop-form.module#CropFormModule',
        data: {component: 'crop', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CropRoutes)
  ],
  declarations: [CropComponent]
})
export class CropModule { }
