import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CropViewComponent } from './crop-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const CropIndexRoutes: Routes = [
  {
    path: '',
    component: CropViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(CropIndexRoutes),
  ],
  declarations: [CropViewComponent]
})
export class CropViewModule { }