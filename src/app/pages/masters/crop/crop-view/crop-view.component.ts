import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { CropService } from '../../../../services/masters/crop/crop.service';

@Component({
  selector: 'app-crop-view',
  templateUrl: './crop-view.component.html',
  styleUrls: ['./crop-view.component.css']
})
export class CropViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private cropService:CropService, private router: Router, private baseUrl: Apisendpoints) { }
  cropData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.cropService.getCrops(this.baseUrl.getCrop + '/' + params.id).subscribe((res)=>{
          this.cropData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/crop/form'], {queryParams: {id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'district', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCrop + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/crop/index']);
    }
  }
}
