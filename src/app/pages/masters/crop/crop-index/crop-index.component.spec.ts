import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CropIndexComponent } from './crop-index.component';

describe('CropIndexComponent', () => {
  let component: CropIndexComponent;
  let fixture: ComponentFixture<CropIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CropIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
