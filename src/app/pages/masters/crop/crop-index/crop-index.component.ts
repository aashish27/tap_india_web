import { Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-crop-index',
  templateUrl: './crop-index.component.html',
  styleUrls: ['./crop-index.component.css']
})
export class CropIndexComponent implements OnInit {
  crop : any = {};
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  tableHideShowStatus: boolean = false;
  constructor(private cropService: CropService,  private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();

  }

  get(){ // Get Crop data .
    var self=this;
    this.cropService.getCrops(this.baseUrl.getCrop).subscribe( res=> { 
      this.crop  = res;
      if(this.crop.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;   
      }
    });
  }
  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'crop', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCrop + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/crop/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/crop/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Crops-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
     this.table = $('#cropTable').DataTable(this.dtoptions);
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'crop', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }

}
