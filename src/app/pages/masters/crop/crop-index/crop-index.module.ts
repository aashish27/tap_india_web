import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CropIndexComponent } from './crop-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const CropIndexRoutes: Routes = [
  {
    path: '',
    component: CropIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(CropIndexRoutes),
  ],
  declarations: [CropIndexComponent]
})
export class CropIndexModule { }