import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-crop',
  template: '<router-outlet></router-outlet>'
})
export class CropComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
