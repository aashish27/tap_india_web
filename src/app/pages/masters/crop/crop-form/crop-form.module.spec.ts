import { CropFormModule } from './crop-form.module';

describe('CropFormModule', () => {
  let cropFormModule: CropFormModule;

  beforeEach(() => {
    cropFormModule = new CropFormModule();
  });

  it('should create an instance', () => {
    expect(cropFormModule).toBeTruthy();
  });
});
