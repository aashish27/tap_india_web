import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Apisendpoints } from '../../../../apisendpoints';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';

declare const $:any;

@Component({
  selector: 'app-crop-form',
  templateUrl: './crop-form.component.html',
  styleUrls: ['./crop-form.component.css']
})
export class CropFormComponent implements OnInit {
  cropForm: FormGroup;
  submitted = false;
  cropData:any;
  id:any;
  constructor(private formBuilder: FormBuilder, 
              private cropService: CropService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.cropForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.cropService.getCrops(this.baseUrl.getCrop + '/' + params.id).subscribe((res)=>{
          this.cropData = res['data'];
          this.id = this.cropData['_id'];
          this.cropForm.setValue({ 
            name : this.cropData['name'],
            tr_recovery : this.cropData['tr_recovery']
          });
        });
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      tr_recovery : new FormControl('')
    })
  }

  onSubmit(value) { // Submit Form ..
    this.cropForm.setErrors({invalid : true});
    if(this.id){
      this.cropService.putCrops(this.baseUrl.putCrop + this.id, this.cropForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['masters/crop/index']);
        }
       this.cropForm.setErrors(null);
      },(error) => {
       this.cropForm.setErrors(null);
        console.log(error);
      });

    }else{
      this.cropService.postCrops(this.baseUrl.postCrop, this.cropForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['masters/crop/index']);
        }
       this.cropForm.setErrors(null);
      },(error) => {
       this.cropForm.setErrors(null);
        console.log(error);
      });
    }
      
      // if (this.cropForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.cropForm.value))
  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {// Reset Values .. 
    this.cropForm.reset();
  }
}
