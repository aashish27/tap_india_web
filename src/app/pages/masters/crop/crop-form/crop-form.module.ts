import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CropFormComponent } from './crop-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const CropFormRoutes: Routes = [
  {
    path: '',
    component: CropFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(CropFormRoutes),
  ],
  declarations: [CropFormComponent]
})
export class CropFormModule { }