import { VillageViewModule } from './village-view.module';

describe('VillageViewModule', () => {
  let villageViewModule: VillageViewModule;

  beforeEach(() => {
    villageViewModule = new VillageViewModule();
  });

  it('should create an instance', () => {
    expect(villageViewModule).toBeTruthy();
  });
});
