import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VillageViewComponent } from './village-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const VillageViewRoutes: Routes = [
  {
    path: '',
    component: VillageViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(VillageViewRoutes),
  ],
  declarations: [VillageViewComponent]
})
export class VillageViewModule { }