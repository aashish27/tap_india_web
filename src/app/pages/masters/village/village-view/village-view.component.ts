import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-village-view',
  templateUrl: './village-view.component.html',
  styleUrls: ['./village-view.component.css']
})
export class VillageViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private authGuard: AuthGuard, private baseUrl: Apisendpoints) { }
  villageData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params['data']){
        this.villageData = JSON.parse(params['data']);
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/village/form'], {queryParams: {data: JSON.stringify(this.villageData)}, skipLocationChange: true});
    }else {
      this.router.navigate(['masters/village/view'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'village', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteVillage + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/village/index']);
    }
  }
}
