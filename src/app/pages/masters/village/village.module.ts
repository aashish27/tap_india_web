import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VillageComponent } from './village.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';
export const VillageRoutes: Routes = [
  {
    path: '',
    component: VillageComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './village-index/village-index.module#VillageIndexModule',
        data: {component: 'village', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './village-view/village-view.module#VillageViewModule',
        data: {component: 'village', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './village-form/village-form.module#VillageFormModule',
        data: {component: 'village', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VillageRoutes)
  ],
  declarations: [VillageComponent]
})
export class VillageModule { }
