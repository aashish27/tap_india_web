import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-village',
  template: '<router-outlet></router-outlet>'
})
export class VillageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
