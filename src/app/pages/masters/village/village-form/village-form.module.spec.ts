import { VillageFormModule } from './village-form.module';

describe('VillageFormModule', () => {
  let villageFormModule: VillageFormModule;

  beforeEach(() => {
    villageFormModule = new VillageFormModule();
  });

  it('should create an instance', () => {
    expect(villageFormModule).toBeTruthy();
  });
});
