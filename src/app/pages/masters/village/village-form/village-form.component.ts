import { Component, OnInit } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import {} from 'googlemaps';
import { ViewChild, ElementRef, NgZone, } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Apisendpoints } from '../../../../apisendpoints';
import { VillageService } from '../../../../services/masters/village/village.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockService } from '../../../../services/masters/block/block.service';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';


@Component({
  selector: 'app-village-form',
  templateUrl: './village-form.component.html',
  styleUrls: ['./village-form.component.css']
})
export class VillageFormComponent implements OnInit {
  villageForm: FormGroup;
  submitted = false;
  villageData:any;
  blocks:any = {};
  id:any;

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  
	@ViewChild('search' ) public searchElement: ElementRef;

  constructor( private villageService: VillageService, 
              private router: Router , 
              private blockService: BlockService,
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute,
              private mapsAPILoader: MapsAPILoader, 
              private ngZone: NgZone) { }

  ngOnInit() {
    this.villageForm = this.createFormGroup();

    //create search FormControl
    this.searchControl = new FormControl();

    this.route.queryParams.subscribe((params)=>{
      if(params['data']){
        this.villageData = JSON.parse(params['data']);
        this.id = this.villageData['_id'];

        this.villageForm.setValue({ 
          name : this.villageData['name'],
          type : this.villageData['type'],
          code : this.villageData['code'],
          latitude : this.villageData['latitude'],
          longitude : this.villageData['longitude'],
          blockId : this.villageData['blockId']._id          
        });
        this.latitude = +this.villageData['latitude'];
        this.longitude = +this.villageData['longitude'];
        this.zoom = 12;
      }else{
        //set google maps position 
        this.setCurrentPosition();
      }
    });
    this.getBlocks();
    this.searchLocations();
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      type : new FormControl('',Validators.required),
      code : new FormControl('',[Validators.required, WhitespaceValidator]),
      latitude : new FormControl(''),
      blockId : new FormControl('',Validators.required),
      longitude : new FormControl('')
    })
  }

  onSubmit(value) { // Submit Form ..
    this.villageForm.setErrors({invalid : true});
      if(this.id){
        this.villageService.putVillages(this.baseUrl.putVillage + this.id, this.villageForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/village/index']);
          }
          this.villageForm.setErrors(null);
        },(error) => {
          this.villageForm.setErrors(null);
          console.log(error);
        });
      }else{ 
        this.villageService.postVillages(this.baseUrl.postVillage, this.villageForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/village/index']);
          }
          this.villageForm.setErrors(null);
        },(error) => {
          this.villageForm.setErrors(null);
          console.log(error);
        });
      }
  }

  getBlocks(){
    this.blockService.getBlocks(this.baseUrl.getBlock).subscribe( res=> { 
      this.blocks = res;
    });
  }

  revert() {// Reset Values .. 
    this.villageForm.setValue({
      name : "",
      type : "",
      code : "",
      latitude : "",
      blockId : "",
      longitude : ""
    });
  }

  setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.villageForm.patchValue({
          latitude : position.coords.latitude,
          longitude : position.coords.longitude
        });
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  searchLocations(){
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;

          this.villageForm.patchValue({
            latitude : this.latitude,
            longitude : this.longitude
          });
        });
      });
    });
  }

  markerDragEnd(event){
    // console.log("event",event);
    this.longitude = event.coords.lng;
    this.latitude = event.coords.lat;

    this.villageForm.patchValue({
      latitude : this.latitude,
      longitude : this.longitude
    });
  }

}

