import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VillageFormComponent } from './village-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../../../../shared/shared.module';

export const VillageFormRoutes: Routes = [
  {
    path: '',
    component: VillageFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot(  {
      apiKey: 'AIzaSyAqYv39bL0fEwS8FwPde01x_iernmQVvGw',
	   libraries: ["places"]
    }),
    SharedModule,
    RouterModule.forChild(VillageFormRoutes),
  ],
  declarations: [VillageFormComponent]
})
export class VillageFormModule { }