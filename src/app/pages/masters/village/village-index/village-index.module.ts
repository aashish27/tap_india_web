import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VillageIndexComponent } from './village-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const VillageIndexRoutes: Routes = [
  {
    path: '',
    component: VillageIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(VillageIndexRoutes),
  ],
  declarations: [VillageIndexComponent]
})
export class VillageIndexModule { }