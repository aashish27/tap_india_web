import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VillageIndexComponent } from './village-index.component';

describe('VillageIndexComponent', () => {
  let component: VillageIndexComponent;
  let fixture: ComponentFixture<VillageIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VillageIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VillageIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
