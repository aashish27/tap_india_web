import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../../../services/loader.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { Router } from '@angular/router';
import { VillageService } from '../../../../services/masters/village/village.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-village-index',
  templateUrl: './village-index.component.html',
  styleUrls: ['./village-index.component.css']
})
export class VillageIndexComponent implements OnInit {
  village : any = {};
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  tableHideShowStatus: boolean = false;
  constructor(private villageService: VillageService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Villages data ..'
    var self = this;
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe( res=> { 
      this.village  = res;
      if(this.village.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'village', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteVillage + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/village/form'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }else {
      this.router.navigate(['masters/village/view'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Villages-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4,5,6,7,8]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
      this.table = $('#villageTable').DataTable(this.dtoptions);
      this.table.columns( [ 5, 6 ] ).visible( false, false );
      this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'village', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}

