import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DistrictService } from '../../../../services/masters/district/district.service';
import { BlockService } from '../../../../services/masters/block/block.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { Router, ActivatedRoute } from '@angular/router';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';


@Component({
  selector: 'app-block-form',
  templateUrl: './block-form.component.html',
  styleUrls: ['./block-form.component.css']
})
export class BlockFormComponent implements OnInit {
  blockForm: FormGroup;
  submitted = false;
  blockData:any;
  districts:any = {};
  id: any;

  constructor(private formBuilder: FormBuilder, 
              private districtService: DistrictService, 
              private blockService: BlockService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.blockForm = this.createFormGroup();
    this.getDistricts();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.blockService.getBlocks(this.baseUrl.getBlock + '/' + params.id).subscribe((res)=>{
          this.blockData = res['data'];
          this.id = this.blockData['_id'];
          this.blockForm.setValue({ 
              name : this.blockData['name'],
              districtId : this.blockData['districtId']._id
          });
        });
      }
    });
  }

  getDistricts(){
    this.districtService.getDistricts(this.baseUrl.getDistrict).subscribe( res=> { 
      this.districts = res;
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      districtId : new FormControl('',Validators.required),
    })
  }

  onSubmit(value) { // Submit Form ..
    this.blockForm.setErrors({invalid : true});
      if(this.id){  
        this.blockService.putBlock(this.baseUrl.putBlock + this.id, this.blockForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/block/index']);
          }
          this.blockForm.setErrors(null);
        },(error) => {
          this.blockForm.setErrors(null);
          console.log(error);
        });
      }else {  
        this.blockService.postBlock(this.baseUrl.postBlock, this.blockForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/block/index']);
          }
          this.blockForm.setErrors(null);
        },(error) => {
          this.blockForm.setErrors(null);
          console.log(error);
        });
      }
  }

  revert() {// Reset Values .. 
    this.blockForm.setValue({
      name : "",
      districtId : "",
    });
  }

}
