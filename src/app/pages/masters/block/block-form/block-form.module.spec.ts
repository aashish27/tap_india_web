import { BlockFormModule } from './block-form.module';

describe('BlockFormModule', () => {
  let blockFormModule: BlockFormModule;

  beforeEach(() => {
    blockFormModule = new BlockFormModule();
  });

  it('should create an instance', () => {
    expect(blockFormModule).toBeTruthy();
  });
});
