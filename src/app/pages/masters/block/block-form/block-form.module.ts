import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BlockFormComponent } from './block-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const BlockFormRoutes: Routes = [
  {
    path: '',
    component: BlockFormComponent
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(BlockFormRoutes),
  ],
  declarations: [BlockFormComponent]
})
export class BlockFormModule { }