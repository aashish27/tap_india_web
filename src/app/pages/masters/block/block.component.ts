import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-block',
  template: '<router-outlet></router-outlet>'
})
export class BlockComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
