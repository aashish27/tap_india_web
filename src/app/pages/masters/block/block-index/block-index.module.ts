import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BlockIndexComponent } from './block-index.component';
import { DataTablesModule } from 'angular-datatables';
import { BlockService } from '../../../../services/masters/block/block.service';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const BlockIndexRoutes: Routes = [
  {
    path: '',
    component: BlockIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    PopupModule,
    RouterModule.forChild(BlockIndexRoutes),
  ],
  declarations: [BlockIndexComponent],
  providers: [BlockService]
})
export class BlockIndexModule { }