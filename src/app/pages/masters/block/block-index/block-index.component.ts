import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { BlockService } from '../../../../services/masters/block/block.service';
import { Observable } from 'rxjs';
import { LoaderService } from '../../../../services/loader.service';
import { Block } from '../../../../models/masters/block';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
declare const $: any;

@Component({
  selector: 'app-block-index',
  templateUrl: './block-index.component.html',
  styleUrls: ['./block-index.component.css']
})
export class BlockIndexComponent implements OnInit {
  message = '';
  block : any = {};
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  private blockObservable : Observable<Block[]> ;
  tableHideShowStatus: boolean = false;
  constructor(private authGuard: AuthGuard, private blockService: BlockService,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Block data ..
    var self=this;
    this.blockObservable = this.blockService.getBlocks(this.baseUrl.getDistrict);
    this.blockService.getBlocks(this.baseUrl.getBlock).subscribe( res=> { 
      this.block = res;
      if(this.block.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'block', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteBlock + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/block/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/block/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Blocks-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
    this.table = $('#blockTable').DataTable(this.dtoptions);
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'block', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}

