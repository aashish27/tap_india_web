import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockIndexComponent } from './block-index.component';

describe('BlockIndexComponent', () => {
  let component: BlockIndexComponent;
  let fixture: ComponentFixture<BlockIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
