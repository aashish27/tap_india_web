import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BlockViewComponent } from './block-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const BlockViewRoutes: Routes = [
  {
    path: '',
    component: BlockViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(BlockViewRoutes),
  ],
  declarations: [BlockViewComponent]
})
export class BlockViewModule { }