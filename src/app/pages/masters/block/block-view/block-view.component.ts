import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { BlockService } from '../../../../services/masters/block/block.service';

@Component({
  selector: 'app-block-view',
  templateUrl: './block-view.component.html',
  styleUrls: ['./block-view.component.css']
})
export class BlockViewComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private blockService: BlockService, private router: Router, private baseUrl: Apisendpoints) { }
  blockData:any;;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.blockService.getBlocks(this.baseUrl.getBlock + '/' + params.id).subscribe((res)=>{
          this.blockData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/block/form'], {queryParams: { id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'block', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteBlock + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/block/index']);
    }
  }
}
