import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BlockComponent } from './block.component';
import { AuthGuard } from '../../guard/auth.guard';
export const BlockRoutes: Routes = [
  {
    path: '',
    component: BlockComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './block-index/block-index.module#BlockIndexModule',
        data: {component: 'block', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './block-view/block-view.module#BlockViewModule',
        data: {component: 'block', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './block-form/block-form.module#BlockFormModule',
        data: {component: 'block', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BlockRoutes)
  ],
  declarations: [BlockComponent]
})
export class BlockModule { }
