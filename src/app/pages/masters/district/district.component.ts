import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-district',
  template: '<router-outlet></router-outlet>'
})
export class DistrictComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
