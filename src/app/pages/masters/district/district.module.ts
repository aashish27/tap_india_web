import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DistrictComponent } from './district.component'; 
import { AuthGuard } from '../../guard/auth.guard';
export const DistrictRoutes: Routes = [
  {
    path: '',
    component: DistrictComponent,
    children: [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './district-index/district-index.module#DistrictIndexModule',
        data: {component: 'district', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './district-view/district-view.module#DistrictViewModule',
        data: {component: 'district', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './district-form/district-form.module#DistrictFormModule',
        data: {component: 'district', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DistrictRoutes)
  ],
  declarations: [DistrictComponent]
})
export class DistrictModule { }
