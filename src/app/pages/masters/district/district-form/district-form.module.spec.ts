import { DistrictFormModule } from './district-form.module';

describe('DistrictFormModule', () => {
  let districtFormModule: DistrictFormModule;

  beforeEach(() => {
    districtFormModule = new DistrictFormModule();
  });

  it('should create an instance', () => {
    expect(districtFormModule).toBeTruthy();
  });
});
