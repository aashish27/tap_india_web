import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DistrictFormComponent } from './district-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const DistrictFormRoutes: Routes = [
  {
    path: '',
    component: DistrictFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(DistrictFormRoutes),
  ],
  declarations: [DistrictFormComponent]
})
export class DistrictFormModule { }