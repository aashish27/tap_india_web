import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Apisendpoints } from '../../../../apisendpoints';
import { DistrictService } from '../../../../services/masters/district/district.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';


@Component({
  selector: 'app-district-form',
  templateUrl: './district-form.component.html',
  styleUrls: ['./district-form.component.css']
})
export class DistrictFormComponent implements OnInit {
  districtForm: FormGroup;
  submitted = false;
  districtData:any;
  id: any;

  constructor(private formBuilder: FormBuilder, 
              private districtService: DistrictService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.districtForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params['data']){
        this.districtData = JSON.parse(params['data']);
        this.id = this.districtData['_id'];
        this.districtForm.setValue({ name : this.districtData['name']});
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
    })
  }

  onSubmit(value) { // Submit Form ..
    this.districtForm.setErrors({invalid : true});
    if(this.id){
      this.districtService.putDistricts(this.baseUrl.putDistrict + this.id, this.districtForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['masters/district/index']);
        }
        this.districtForm.setErrors(null);
      },(error) => {
        this.districtForm.setErrors(null);
        console.log(error);
      });
    }else {
      this.districtService.postDistricts(this.baseUrl.postDistrict, this.districtForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['masters/district/index']);
        }
        this.districtForm.setErrors(null);
      },(error) => {
        this.districtForm.setErrors(null);
        console.log(error);
      });
    }
      

      // if (this.districtForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.districtForm.value))
  }

  revert() {// Reset Values .. 
    this.districtForm.reset();
  }

}
