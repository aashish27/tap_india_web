import { Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { DistrictService } from '../../../../services/masters/district/district.service';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../guard/auth.guard';
declare const $: any;
@Component({
  selector: 'app-district-index',
  templateUrl: './district-index.component.html',
  styleUrls: ['./district-index.component.css']
})
export class DistrictIndexComponent implements OnInit {
  district : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  guardParams: any;
  constructor(private authGuard: AuthGuard, private districtService: DistrictService,  private loaderService: LoaderService,  private baseUrl: Apisendpoints,   private renderer: Renderer, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Districts data ..
    var self=this;
    this.districtService.getDistricts(this.baseUrl.getDistrict).subscribe( res=> { 
      this.district  = res;
      if(this.district.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'district', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteDistrict + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/district/form'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }else {
      this.router.navigate(['masters/district/view'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Districts-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
     this.table = $('#districtTable').DataTable(this.dtoptions);
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'district', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
