import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DistrictIndexComponent } from './district-index.component';
import { DataTablesModule } from 'angular-datatables';
import { DistrictService } from '../../../../services/masters/district/district.service';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const DistrictIndexRoutes: Routes = [
  {
    path: '',
    component: DistrictIndexComponent
  }
];

@NgModule({
  imports: [
    DataTablesModule,
    CommonModule,
    PopupModule,
    RouterModule.forChild(DistrictIndexRoutes),
  ],
  declarations: [DistrictIndexComponent],
  providers: [DistrictService]
})
export class DistrictIndexModule { }