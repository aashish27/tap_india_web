import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistrictIndexComponent } from './district-index.component';

describe('DistrictIndexComponent', () => {
  let component: DistrictIndexComponent;
  let fixture: ComponentFixture<DistrictIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistrictIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistrictIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
