import { DistrictIndexModule } from './district-index.module';

describe('DistrictIndexModule', () => {
  let districtIndexModule: DistrictIndexModule;

  beforeEach(() => {
    districtIndexModule = new DistrictIndexModule();
  });

  it('should create an instance', () => {
    expect(districtIndexModule).toBeTruthy();
  });
});
