import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../../../node_modules/@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-district-view',
  templateUrl: './district-view.component.html',
  styleUrls: ['./district-view.component.css']
})
export class DistrictViewComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private router: Router, private baseUrl: Apisendpoints) { }
  districtData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params['data']){
        this.districtData = JSON.parse(params['data']);
      }
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/district/form'], {queryParams: {data: JSON.stringify(this.districtData)}, skipLocationChange: true});
    }else {
      this.router.navigate(['masters/district/view'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'district', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteDistrict + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/district/index']);
    }
  }
  

}
