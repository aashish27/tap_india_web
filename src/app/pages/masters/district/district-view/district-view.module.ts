import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DistrictViewComponent } from './district-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const DistrictViewRoutes: Routes = [
  {
    path: '',
    component: DistrictViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(DistrictViewRoutes),
  ],
  declarations: [DistrictViewComponent]
})
export class DistrictViewModule { }