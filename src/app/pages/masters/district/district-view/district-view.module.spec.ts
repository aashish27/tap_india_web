import { DistrictViewModule } from './district-view.module';

describe('DistrictViewModule', () => {
  let districtViewModule: DistrictViewModule;

  beforeEach(() => {
    districtViewModule = new DistrictViewModule();
  });

  it('should create an instance', () => {
    expect(districtViewModule).toBeTruthy();
  });
});
