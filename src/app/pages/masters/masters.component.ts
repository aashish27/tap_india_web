import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-masters',
  template: '<router-outlet></router-outlet>'
})
export class MastersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // console.log("Masters");
  }

}
