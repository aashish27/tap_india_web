import { ActivityViewModule } from './activity-view.module';

describe('ActivityViewModule', () => {
  let activityViewModule: ActivityViewModule;

  beforeEach(() => {
    activityViewModule = new ActivityViewModule();
  });

  it('should create an instance', () => {
    expect(activityViewModule).toBeTruthy();
  });
});
