import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityViewComponent } from './activity-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ActivityViewRoutes: Routes = [
  {
    path: '',
    component: ActivityViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ActivityViewRoutes),
  ],
  declarations: [ActivityViewComponent]
})
export class ActivityViewModule { }