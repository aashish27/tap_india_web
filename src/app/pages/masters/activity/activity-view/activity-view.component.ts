import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { ActivityListService } from '../../../../services/masters/activity-list/activity-list.service';

@Component({
  selector: 'app-activity-view',
  templateUrl: './activity-view.component.html',
  styleUrls: ['./activity-view.component.css']
})
export class ActivityViewComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private activtyService: ActivityListService, private router: Router, private baseUrl: Apisendpoints) { }
  activityData:any;;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.activtyService.getActivityList(this.baseUrl.getActivity + '/' + params.id).subscribe((res)=>{
          this.activityData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/activity/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'district', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteActivity + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/activity/index']);
    }
  }

  
}
