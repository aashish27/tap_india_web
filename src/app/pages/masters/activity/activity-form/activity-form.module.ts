import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityFormComponent } from './activity-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const ActivityFormRoutes: Routes = [
  {
    path: '',
    component: ActivityFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(ActivityFormRoutes),
  ],
  declarations: [ActivityFormComponent]
})
export class ActivityFormModule { }