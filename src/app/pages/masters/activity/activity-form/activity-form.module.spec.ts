import { ActivityFormModule } from './activity-form.module';

describe('ActivityFormModule', () => {
  let activityFormModule: ActivityFormModule;

  beforeEach(() => {
    activityFormModule = new ActivityFormModule();
  });

  it('should create an instance', () => {
    expect(activityFormModule).toBeTruthy();
  });
});
