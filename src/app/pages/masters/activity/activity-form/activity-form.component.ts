import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivityListService } from '../../../../services/masters/activity-list/activity-list.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';
import { Apisendpoints } from '../../../../apisendpoints';

@Component({
  selector: 'app-activity-form',
  templateUrl: './activity-form.component.html',
  styleUrls: ['./activity-form.component.css']
})
export class ActivityFormComponent implements OnInit {
  activityForm: FormGroup;
  submitted = false;
  activityData:any;
  id: any;

  constructor(private formBuilder: FormBuilder, 
              private activityService: ActivityListService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.activityForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.activityService.getActivityList(this.baseUrl.getActivity + '/' + params.id).subscribe((res)=>{
          this.activityData = res['data'];
          this.id = this.activityData['_id'];
          this.activityForm.setValue({ 
            name : this.activityData['name'],
            code : this.activityData['code']
          });
        });
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      code : new FormControl('',[Validators.required, WhitespaceValidator])
    })
  }

  onSubmit(value) { // Submit Form ..
    this.activityForm.setErrors({invalid : true});
      if(this.id){
        this.activityService.putActivityList(this.baseUrl.putActivity + this.id, this.activityForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/activity/index']);
          }
          this.activityForm.setErrors(null);
        },(error) => {
          this.activityForm.setErrors(null);
          console.log(error);
        });
      }else {
        this.activityService.postActivityList(this.baseUrl.postActivity, this.activityForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/activity/index']);
          }
          this.activityForm.setErrors(null);
        },(error) => {
          this.activityForm.setErrors(null);
          console.log(error);
        });
      }

      // if (this.activityForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.activityForm.value))
  }

  revert() {// Reset Values .. 
    this.activityForm.reset();
  }
}
