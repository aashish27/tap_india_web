import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityComponent } from './activity.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';

export const ActivityRoutes: Routes = [
{
    path: '',
    component: ActivityComponent,
    // children: [
    //   {
    //     path: '',
    //     redirectTo: 'index',
    //     pathMatch: 'full',
    //   },{
    //     path: 'index',
    //     loadChildren: './activity-index/activity-index.module#ActivityIndexModule'
    //   },{
    //     path: 'view',
    //     loadChildren: './activity-view/activity-view.module#ActivityViewModule'
    //   },{
    //     path: 'form',
    //     loadChildren: './activity-form/activity-form.module#ActivityFormModule'
    //   }
    // ]
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './activity-index/activity-index.module#ActivityIndexModule',
        data: {component: 'activity', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './activity-view/activity-view.module#ActivityViewModule',
        data: {component: 'activity', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './activity-form/activity-form.module#ActivityFormModule',
        data: {component: 'activity', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivityRoutes)
  ],
  declarations: [ActivityComponent]
})
export class ActivityModule { }
