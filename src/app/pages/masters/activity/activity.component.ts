import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-activity',
  template: '<router-outlet></router-outlet>'
})
export class ActivityComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
