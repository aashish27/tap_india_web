import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityIndexComponent } from './activity-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const ActivityIndexRoutes: Routes = [
  {
    path: '',
    component: ActivityIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ActivityIndexRoutes)
  ],
  declarations: [ActivityIndexComponent]
})
export class ActivityIndexModule { }