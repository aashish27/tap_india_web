import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivityListService } from '../../../../services/masters/activity-list/activity-list.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
declare const $: any;

@Component({
  selector: 'app-activity-index',
  templateUrl: './activity-index.component.html',
  styleUrls: ['./activity-index.component.css']
})
export class ActivityIndexComponent implements OnInit {
  activity : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private activityService: ActivityListService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity data ..
    var self=this;
    this.activityService.getActivityList(this.baseUrl.getActivity).subscribe( res=> { 
      this.activity  = res;
      if(this.activity.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'activity', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteActivity + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/activity/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/activity/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Activity-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
    this.table = $('#activityTable').DataTable(this.dtoptions);
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'activity', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
