import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryOwnerComponent } from './machinery-owner.component';

describe('MachineryOwnerComponent', () => {
  let component: MachineryOwnerComponent;
  let fixture: ComponentFixture<MachineryOwnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryOwnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryOwnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
