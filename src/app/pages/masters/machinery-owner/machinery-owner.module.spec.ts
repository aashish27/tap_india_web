import { MachineryOwnerModule } from './machinery-owner.module';

describe('MachineryOwnerModule', () => {
  let machineryOwnerModule: MachineryOwnerModule;

  beforeEach(() => {
    machineryOwnerModule = new MachineryOwnerModule();
  });

  it('should create an instance', () => {
    expect(machineryOwnerModule).toBeTruthy();
  });
});
