import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryOwnerViewComponent } from './machinery-owner-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const MachineryOwnerViewRoutes: Routes = [
  {
    path: '',
    component: MachineryOwnerViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(MachineryOwnerViewRoutes),
  ],
  declarations: [MachineryOwnerViewComponent]
})
export class MachineryOwnerViewModule { }