import { MachineryOwnerViewModule } from './machinery-owner-view.module';

describe('MachineryOwnerViewModule', () => {
  let machineryOwnerViewModule: MachineryOwnerViewModule;

  beforeEach(() => {
    machineryOwnerViewModule = new MachineryOwnerViewModule();
  });

  it('should create an instance', () => {
    expect(machineryOwnerViewModule).toBeTruthy();
  });
});
