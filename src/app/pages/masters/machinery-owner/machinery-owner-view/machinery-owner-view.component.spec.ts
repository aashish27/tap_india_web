import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryOwnerViewComponent } from './machinery-owner-view.component';

describe('MachineryOwnerViewComponent', () => {
  let component: MachineryOwnerViewComponent;
  let fixture: ComponentFixture<MachineryOwnerViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryOwnerViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryOwnerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
