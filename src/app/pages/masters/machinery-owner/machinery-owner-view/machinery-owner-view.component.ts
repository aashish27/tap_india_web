import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { MachineryOwnerService } from '../../../../services/masters/machineryOwner/machinery-owner.service';

@Component({
  selector: 'app-machinery-owner-view',
  templateUrl: './machinery-owner-view.component.html',
  styleUrls: ['./machinery-owner-view.component.css']
})
export class MachineryOwnerViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private machineryOwnerService:MachineryOwnerService, private router: Router, private baseUrl: Apisendpoints) { }
  machineOwnerData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.machineryOwnerService.getMachineryOwner(this.baseUrl.getMachineryOwner + '/' + params.id).subscribe((res)=>{
          this.machineOwnerData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/machinery-owner/form'], {queryParams: {id : data._id}});
    }
  }

  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery-owner', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteMachineryOwner + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/machinery-owner/index']);
    }
  }
}
