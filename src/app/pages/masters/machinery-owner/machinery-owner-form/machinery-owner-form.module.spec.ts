import { MachineryOwnerFormModule } from './machinery-owner-form.module';

describe('MachineryOwnerFormModule', () => {
  let machineryOwnerFormModule: MachineryOwnerFormModule;

  beforeEach(() => {
    machineryOwnerFormModule = new MachineryOwnerFormModule();
  });

  it('should create an instance', () => {
    expect(machineryOwnerFormModule).toBeTruthy();
  });
});
