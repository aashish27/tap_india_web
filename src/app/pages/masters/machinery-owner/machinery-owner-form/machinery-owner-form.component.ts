import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { MachineryOwnerService } from '../../../../services/masters/machineryOwner/machinery-owner.service';
import { MachineryCodeService } from '../../../../services/masters/machineryCode/machinery-code.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-machinery-owner-form',
  templateUrl: './machinery-owner-form.component.html',
  styleUrls: ['./machinery-owner-form.component.css']
})
export class MachineryOwnerFormComponent implements OnInit {
  machineOwnerForm: FormGroup;
  submitted = false;
  machineData:any;
  machines:any = {};
  beneficiaries:any = {};
  villages:any = {};
  id:any;
  showBeneficiary:boolean = false;

  constructor(private formBuilder: FormBuilder, 
              private villageService: VillageService, 
              private beneficiaryService: BeneficiaryService, 
              private machineryOwnerService: MachineryOwnerService, 
              private machineryCodeService:MachineryCodeService,
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.machineOwnerForm = this.createFormGroup();
    
    this.getMachineryCode();
    // this.getBeneficiary();
    this.getVillages();

    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.machineryOwnerService.getMachineryOwner(this.baseUrl.getMachineryOwner + '/' + params.id).subscribe((res)=>{
          this.machineData = res['data'];
          this.id = this.machineData['_id'];
          this.machineOwnerForm.setValue({ 
              machineryProfileId : this.machineData['machineryProfileId']._id || this.machineData['machineryProfileId'],
              beneficiaryId : this.machineData['beneficiaryId']._id || this.machineData['beneficiaryId'],
              villageId : this.machineData['villageId']._id || this.machineData['villageId']
          });
          this.getBeneficiary('update');
        });
      }
    });
  }

  getMachineryCode(){
    this.machineryCodeService.getMachineryCode(this.baseUrl.getMachineryCode).subscribe( res=> { 
      this.machines = res;
    });
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.machineOwnerForm && this.machineOwnerForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.machineOwnerForm && this.machineOwnerForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.machineOwnerForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.machineOwnerForm.controls['beneficiaryId'].reset("");
      }
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.machineOwnerForm.controls['beneficiaryId'].reset("");
    }
    this.machineOwnerForm.controls['villageId'].markAsTouched();
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      machineryProfileId : new FormControl('',Validators.required),
      beneficiaryId : new FormControl('',this.beneficiaryValidator),
      villageId : new FormControl('',Validators.required),
    })
  }

  redirectToBeneficiary(village_id){
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : "masters/machinery-owner/form",
        data : JSON.stringify(this.machineOwnerForm.value)
      }
    })
  }

  onSubmit(value) { // Submit Form ..
    this.machineOwnerForm.setErrors({invalid : true});
      if(this.id){   
        this.machineryOwnerService.putMachineryCode(this.baseUrl.putMachineryOwner + this.id, this.machineOwnerForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/machinery-owner/index']);
          }
          this.machineOwnerForm.setErrors(null);
        },(error) => {
          this.machineOwnerForm.setErrors(null);
          console.log(error);
        });
  
      }else{  
        this.machineryOwnerService.postMachineryOwner(this.baseUrl.postMachineryOwner, this.machineOwnerForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/machinery-owner/index']);
          }
          this.machineOwnerForm.setErrors(null);
        },(error) => {
          this.machineOwnerForm.setErrors(null);
          console.log(error);
        });
  
      }
      // if (this.machineOwnerForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.machineOwnerForm.value))
  }

  revert() {// Reset Values .. 
    this.machineOwnerForm.setValue({
      machineryProfileId : "",
      beneficiaryId : "",
      villageId : "",
    });
  }

}


