import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryOwnerFormComponent } from './machinery-owner-form.component';

describe('MachineryOwnerFormComponent', () => {
  let component: MachineryOwnerFormComponent;
  let fixture: ComponentFixture<MachineryOwnerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryOwnerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryOwnerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
