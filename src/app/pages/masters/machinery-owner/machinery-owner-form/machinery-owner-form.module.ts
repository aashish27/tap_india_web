import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryOwnerFormComponent } from './machinery-owner-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const MachineryOwnerFormRoutes: Routes = [
  {
    path: '',
    component: MachineryOwnerFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(MachineryOwnerFormRoutes),
  ],
  declarations: [MachineryOwnerFormComponent]
})
export class MachineryOwnerFormModule { }