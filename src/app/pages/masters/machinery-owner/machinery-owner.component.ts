import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-machinery-owner',
  template: '<router-outlet></router-outlet>'
})
export class MachineryOwnerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
