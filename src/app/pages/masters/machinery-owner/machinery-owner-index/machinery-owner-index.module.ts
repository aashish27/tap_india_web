import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryOwnerIndexComponent } from './machinery-owner-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const MachineryOwnerIndexRoutes: Routes = [
  {
    path: '',
    component: MachineryOwnerIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(MachineryOwnerIndexRoutes),
  ],
  declarations: [MachineryOwnerIndexComponent]
})
export class MachineryOwnerIndexModule { }