import { MachineryOwnerIndexModule } from './machinery-owner-index.module';

describe('MachineryOwnerIndexModule', () => {
  let machineryOwnerIndexModule: MachineryOwnerIndexModule;

  beforeEach(() => {
    machineryOwnerIndexModule = new MachineryOwnerIndexModule();
  });

  it('should create an instance', () => {
    expect(machineryOwnerIndexModule).toBeTruthy();
  });
});
