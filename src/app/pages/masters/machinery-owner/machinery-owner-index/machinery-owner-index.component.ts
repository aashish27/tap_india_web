import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { MachineryOwnerService } from '../../../../services/masters/machineryOwner/machinery-owner.service';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-machinery-owner-index',
  templateUrl: './machinery-owner-index.component.html',
  styleUrls: ['./machinery-owner-index.component.css']
})
export class MachineryOwnerIndexComponent implements OnInit {
  message = '';
  machineOwner : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private machineryOwnerService: MachineryOwnerService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();

  }

  get(){ // Get Machinery Owner data ..
    var self=this;
    this.machineryOwnerService.getMachineryOwner(this.baseUrl.getMachineryOwner).subscribe( res=> { 
      this.machineOwner = res;
      if(this.machineOwner.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true; 
      }
    });
  }
  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery-owner', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteMachineryOwner + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/machinery-owner/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/machinery-owner/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Mahcinery-owner-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4,5,6]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
      this.table = $('#machineryOwnerTable').DataTable(this.dtoptions);
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery-owner', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
