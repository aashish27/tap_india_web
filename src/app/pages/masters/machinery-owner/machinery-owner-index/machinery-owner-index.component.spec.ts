import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryOwnerIndexComponent } from './machinery-owner-index.component';

describe('MachineryOwnerIndexComponent', () => {
  let component: MachineryOwnerIndexComponent;
  let fixture: ComponentFixture<MachineryOwnerIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryOwnerIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryOwnerIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
