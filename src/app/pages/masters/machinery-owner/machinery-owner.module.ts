import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryOwnerComponent } from './machinery-owner.component';
import { AuthGuard } from '../../guard/auth.guard';
export const MachineryOwnerRoutes: Routes = [
  {
    path: '',
    component: MachineryOwnerComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './machinery-owner-index/machinery-owner-index.module#MachineryOwnerIndexModule',
        data: {component: 'machinery-owner', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './machinery-owner-view/machinery-owner-view.module#MachineryOwnerViewModule',
        data: {component: 'machinery-owner', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './machinery-owner-form/machinery-owner-form.module#MachineryOwnerFormModule',
        data: {component: 'machinery-owner', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MachineryOwnerRoutes)
  ],
  declarations: [MachineryOwnerComponent]
})
export class MachineryOwnerModule { }
