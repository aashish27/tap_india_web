import { MachineryModule } from './machinery.module';

describe('MachineryModule', () => {
  let machineryModule: MachineryModule;

  beforeEach(() => {
    machineryModule = new MachineryModule();
  });

  it('should create an instance', () => {
    expect(machineryModule).toBeTruthy();
  });
});
