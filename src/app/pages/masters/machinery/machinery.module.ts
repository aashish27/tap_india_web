import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryComponent } from './machinery.component';
import { AuthGuard } from '../../guard/auth.guard';
export const MachineryRoutes: Routes = [
  {
    path: '',
    component: MachineryComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './machinery-index/machinery-index.module#MachineryIndexModule',
        data: {component: 'machinery', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './machinery-view/machinery-view.module#MachineryViewModule',
        data: {component: 'machinery', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './machinery-form/machinery-form.module#MachineryFormModule',
        data: {component: 'machinery', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MachineryRoutes)
  ],
  declarations: [MachineryComponent]
})
export class MachineryModule { }
