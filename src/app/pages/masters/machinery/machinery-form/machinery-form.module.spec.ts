import { MachineryFormModule } from './machinery-form.module';

describe('MachineryFormModule', () => {
  let machineryFormModule: MachineryFormModule;

  beforeEach(() => {
    machineryFormModule = new MachineryFormModule();
  });

  it('should create an instance', () => {
    expect(machineryFormModule).toBeTruthy();
  });
});
