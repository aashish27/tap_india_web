import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryFormComponent } from './machinery-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const MachineryFormRoutes: Routes = [
  {
    path: '',
    component: MachineryFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(MachineryFormRoutes),
  ],
  declarations: [MachineryFormComponent]
})
export class MachineryFormModule { }