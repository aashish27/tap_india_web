import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MachineryService } from '../../../../services/masters/machinery/machinery.service';
import { ActivityListService } from '../../../../services/masters/activity-list/activity-list.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';

@Component({
  selector: 'app-machinery-form',
  templateUrl: './machinery-form.component.html',
  styleUrls: ['./machinery-form.component.css']
})
export class MachineryFormComponent implements OnInit {
  machineryForm: FormGroup;
  submitted = false;
  machineryData:any;
  activities:any = {};
  id:any;

  constructor( private machineryService: MachineryService,
              private activityListService: ActivityListService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getActivities();
    this.machineryForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.machineryService.getMachinery(this.baseUrl.getMachinery + '/' + params.id).subscribe((res)=>{
          this.machineryData = res['data'];
          this.id = this.machineryData['_id'];
          this.machineryForm.setValue({ 
            activityId : this.machineryData['activityId']._id,
            name : this.machineryData['name'],
            code : this.machineryData['code']   
          });
        });
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      activityId : new FormControl('',Validators.required),
      name : new FormControl('',[Validators.required, WhitespaceValidator]),
      code : new FormControl('',[Validators.required, WhitespaceValidator])
    })
  }

  onSubmit(value) { // Submit Form ..
    this.machineryForm.setErrors({invalid : true});
      if(this.id){
        this.machineryService.putMachinery(this.baseUrl.putMachinery + this.id, this.machineryForm.value ).subscribe( res=> { 
          if(res['success']){
            this.router.navigate(['masters/machinery/index']);
          }
         this.machineryForm.setErrors(null);
        },(error) => {
         this.machineryForm.setErrors(null);
          console.log(error);
        });
      }else{ 
        this.machineryService.postMachinery(this.baseUrl.postMachinery, this.machineryForm.value ).subscribe( response=> { 
          if(response['success']){
            this.router.navigate(['masters/machinery/index']);
          }
         this.machineryForm.setErrors(null);
        },(error) => {
         this.machineryForm.setErrors(null);
          console.log(error);
        });
      }

      // if (this.machineryForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.machineryForm.value))
  }

  getActivities(){
    this.activityListService.getActivityList(this.baseUrl.getActivity).subscribe( res=> { 
      this.activities = res;
    });
  }

  revert() {// Reset Values .. 
    this.machineryForm.setValue({
      name : "",
      activityId : "",
      code : ""
    });
  }
}

