import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryIndexComponent } from './machinery-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const MachineryIndexRoutes: Routes = [
  {
    path: '',
    component: MachineryIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(MachineryIndexRoutes),
  ],
  declarations: [MachineryIndexComponent]
})
export class MachineryIndexModule { }