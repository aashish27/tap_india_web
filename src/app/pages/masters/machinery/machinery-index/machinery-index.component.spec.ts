import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryIndexComponent } from './machinery-index.component';

describe('MachineryIndexComponent', () => {
  let component: MachineryIndexComponent;
  let fixture: ComponentFixture<MachineryIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
