import { MachineryIndexModule } from './machinery-index.module';

describe('MachineryIndexModule', () => {
  let machineryIndexModule: MachineryIndexModule;

  beforeEach(() => {
    machineryIndexModule = new MachineryIndexModule();
  });

  it('should create an instance', () => {
    expect(machineryIndexModule).toBeTruthy();
  });
});
