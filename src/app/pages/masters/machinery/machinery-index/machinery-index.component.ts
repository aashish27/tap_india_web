import { Component, OnInit } from '@angular/core';
import { Apisendpoints } from '../../../../apisendpoints';
import { MachineryService } from '../../../../services/masters/machinery/machinery.service';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-machinery-index',
  templateUrl: './machinery-index.component.html',
  styleUrls: ['./machinery-index.component.css']
})
export class MachineryIndexComponent implements OnInit {
  message = '';
  machinery : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private machineryService: MachineryService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();

  }

  get(){ // Get machinery data ..
    var self = this;
    this.machineryService.getMachinery(this.baseUrl.getMachinery).subscribe( res=> { 
      this.machinery = res;
      if(this.machinery.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }
  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteMachinery + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/machinery/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/machinery/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Mahcinery-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4,5]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
      this.table = $('#machineryTable').DataTable(this.dtoptions);
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  } 
}
