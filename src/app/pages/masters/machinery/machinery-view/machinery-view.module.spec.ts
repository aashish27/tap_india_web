import { MachineryViewModule } from './machinery-view.module';

describe('MachineryViewModule', () => {
  let machineryViewModule: MachineryViewModule;

  beforeEach(() => {
    machineryViewModule = new MachineryViewModule();
  });

  it('should create an instance', () => {
    expect(machineryViewModule).toBeTruthy();
  });
});
