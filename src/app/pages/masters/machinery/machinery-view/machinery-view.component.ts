import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { MachineryService } from '../../../../services/masters/machinery/machinery.service';

@Component({
  selector: 'app-machinery-view',
  templateUrl: './machinery-view.component.html',
  styleUrls: ['./machinery-view.component.css']
})
export class MachineryViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private machineryService:MachineryService, private router: Router, private baseUrl: Apisendpoints) { }
  machineryData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.machineryService.getMachinery(this.baseUrl.getMachinery + '/' + params.id).subscribe((res)=>{
          this.machineryData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/machinery/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'machinery', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteMachinery + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/machinery/index']);
    }
  }
}
