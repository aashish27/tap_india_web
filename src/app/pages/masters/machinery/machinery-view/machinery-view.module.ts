import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MachineryViewComponent } from './machinery-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const MachineryViewRoutes: Routes = [
  {
    path: '',
    component: MachineryViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(MachineryViewRoutes),
  ],
  declarations: [MachineryViewComponent]
})
export class MachineryViewModule { }
