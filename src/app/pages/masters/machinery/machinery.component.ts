import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-machinery',
  template: '<router-outlet></router-outlet>'
})
export class MachineryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
