import { CapacityBuildingTopicIndexModule } from './capacity-building-topic-index.module';

describe('CapacityBuildingTopicIndexModule', () => {
  let capacityBuildingTopicIndexModule: CapacityBuildingTopicIndexModule;

  beforeEach(() => {
    capacityBuildingTopicIndexModule = new CapacityBuildingTopicIndexModule();
  });

  it('should create an instance', () => {
    expect(capacityBuildingTopicIndexModule).toBeTruthy();
  });
});
