import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityBuildingTopicIndexComponent } from './capacity-building-topic-index.component';

describe('CapacityBuildingTopicIndexComponent', () => {
  let component: CapacityBuildingTopicIndexComponent;
  let fixture: ComponentFixture<CapacityBuildingTopicIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityBuildingTopicIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityBuildingTopicIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
