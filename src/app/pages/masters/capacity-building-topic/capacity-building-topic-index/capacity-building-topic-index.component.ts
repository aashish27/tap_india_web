import { Component, OnInit } from '@angular/core';
import { CbTopicService } from '../../../../services/masters/cbtopic/cbtopic.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-capacity-building-topic-index',
  templateUrl: './capacity-building-topic-index.component.html',
  styleUrls: ['./capacity-building-topic-index.component.css']
})
export class CapacityBuildingTopicIndexComponent implements OnInit {
  cbtopic : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions:any = {};
  table:any;
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private authGuard: AuthGuard, private cbtopicService: CbTopicService,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();

  }

  get(){ // Get CBTopic data ..
    var self=this;
    this.cbtopicService.getCbtopic(this.baseUrl.getCbtopic).subscribe( res=> { 
      this.cbtopic  = res;
      if(this.cbtopic.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;   
      }
    });
  }
  
  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'capacity-building-topic', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCbtopic + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/capacity-building-topic/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['masters/capacity-building-topic/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Capacity-building-list", 
          footer: false,
          exportOptions: {
              columns: [1,2,3]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  }
    this.table = $('#cbtopicTable').DataTable(this.dtoptions);
  }
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'capacity-building-topic', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }

}
