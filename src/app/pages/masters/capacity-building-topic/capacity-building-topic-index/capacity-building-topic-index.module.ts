import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CapacityBuildingTopicIndexComponent } from './capacity-building-topic-index.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const CbIndexRoutes: Routes = [
  {
    path: '',
    component: CapacityBuildingTopicIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(CbIndexRoutes),
  ],
  declarations: [CapacityBuildingTopicIndexComponent]
})
export class CapacityBuildingTopicIndexModule { }