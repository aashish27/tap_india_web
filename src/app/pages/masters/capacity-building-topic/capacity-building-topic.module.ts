import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CapacityBuildingTopicComponent } from './capacity-building-topic.component';
import { AuthGuard } from '../../guard/auth.guard';
export const CapacityBuildingTopicRoutes: Routes = [
  {
    path: '',
    component: CapacityBuildingTopicComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './capacity-building-topic-index/capacity-building-topic-index.module#CapacityBuildingTopicIndexModule',
        data: {component: 'capacity-building-topic', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './capacity-building-topic-view/capacity-building-topic-view.module#CapacityBuildingTopicViewModule',
        data: {component: 'capacity-building-topic', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './capacity-building-topic-form/capacity-building-topic-form.module#CapacityBuildingTopicFormModule',
        data: {component: 'capacity-building-topic', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CapacityBuildingTopicRoutes)
  ],
  declarations: [CapacityBuildingTopicComponent]
})
export class CapacityBuildingTopicModule { }
