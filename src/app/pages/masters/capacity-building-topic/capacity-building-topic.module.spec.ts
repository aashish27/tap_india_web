import { CapacityBuildingTopicModule } from './capacity-building-topic.module';

describe('CapacityBuildingTopicModule', () => {
  let capacityBuildingTopicModule: CapacityBuildingTopicModule;

  beforeEach(() => {
    capacityBuildingTopicModule = new CapacityBuildingTopicModule();
  });

  it('should create an instance', () => {
    expect(capacityBuildingTopicModule).toBeTruthy();
  });
});
