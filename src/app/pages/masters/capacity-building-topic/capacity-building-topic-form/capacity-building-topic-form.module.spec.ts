import { CapacityBuildingTopicFormModule } from './capacity-building-topic-form.module';

describe('CapacityBuildingTopicFormModule', () => {
  let capacityBuildingTopicFormModule: CapacityBuildingTopicFormModule;

  beforeEach(() => {
    capacityBuildingTopicFormModule = new CapacityBuildingTopicFormModule();
  });

  it('should create an instance', () => {
    expect(capacityBuildingTopicFormModule).toBeTruthy();
  });
});
