import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CbTopicService } from '../../../../services/masters/cbtopic/cbtopic.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';

@Component({
  selector: 'app-capacity-building-topic-form',
  templateUrl: './capacity-building-topic-form.component.html',
  styleUrls: ['./capacity-building-topic-form.component.css']
})
export class CapacityBuildingTopicFormComponent implements OnInit {
  cbtopicForm: FormGroup;
  submitted = false;
  cbtopicData:any;
  id: any;

  constructor(
              private cbtopicService: CbTopicService, 
              private router: Router , 
              private baseUrl: Apisendpoints,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.cbtopicForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.cbtopicService.getCbtopic(this.baseUrl.getCbtopic + '/' + params.id).subscribe((res)=>{
          this.cbtopicData = res['data'];
          this.id = this.cbtopicData['_id'];
          this.cbtopicForm.setValue({ 
            name : this.cbtopicData['name']
          });
        });
      }
    });
  }

  createFormGroup(){ // Set Form Validator ...
    return new FormGroup({
      name : new FormControl('',[Validators.required, WhitespaceValidator])
    })
  }

  onSubmit(value) { // Submit Form ..
    this.cbtopicForm.setErrors({invalid : true});
    if(this.id){   
      this.cbtopicService.putCbtopic(this.baseUrl.putCbtopic + this.id, this.cbtopicForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['masters/capacity-building-topic/index']);
        }
        this.cbtopicForm.setErrors(null);
      },(error) => {
        this.cbtopicForm.setErrors(null);
        console.log(error);
      });

    }else{    
      this.cbtopicService.postCbtopic(this.baseUrl.postCbtopic, this.cbtopicForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['masters/capacity-building-topic/index']);
        }
        this.cbtopicForm.setErrors(null);
      },(error) => {
        this.cbtopicForm.setErrors(null);
        console.log(error);
      });

    }
      
      // if (this.cbtopicForm.invalid) {
      //     return;
      // }
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.cbtopicForm.value))
  }

  revert() {// Reset Values .. 
    this.cbtopicForm.reset();
  }
}

