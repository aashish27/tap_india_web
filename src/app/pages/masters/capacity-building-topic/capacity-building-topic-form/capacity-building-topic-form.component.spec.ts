import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityBuildingTopicFormComponent } from './capacity-building-topic-form.component';

describe('CapacityBuildingTopicFormComponent', () => {
  let component: CapacityBuildingTopicFormComponent;
  let fixture: ComponentFixture<CapacityBuildingTopicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityBuildingTopicFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityBuildingTopicFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
