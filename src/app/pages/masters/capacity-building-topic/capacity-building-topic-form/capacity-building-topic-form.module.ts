import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CapacityBuildingTopicFormComponent } from './capacity-building-topic-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const CbFormRoutes: Routes = [
  {
    path: '',
    component: CapacityBuildingTopicFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(CbFormRoutes),
  ],
  declarations: [CapacityBuildingTopicFormComponent]
})
export class CapacityBuildingTopicFormModule { }