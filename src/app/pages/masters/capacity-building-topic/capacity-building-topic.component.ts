import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-capacity-building-topic',
  template: '<router-outlet></router-outlet>'
})
export class CapacityBuildingTopicComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
