import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityBuildingTopicComponent } from './capacity-building-topic.component';

describe('CapacityBuildingTopicComponent', () => {
  let component: CapacityBuildingTopicComponent;
  let fixture: ComponentFixture<CapacityBuildingTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityBuildingTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityBuildingTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
