import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityBuildingTopicViewComponent } from './capacity-building-topic-view.component';

describe('CapacityBuildingTopicViewComponent', () => {
  let component: CapacityBuildingTopicViewComponent;
  let fixture: ComponentFixture<CapacityBuildingTopicViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityBuildingTopicViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityBuildingTopicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
