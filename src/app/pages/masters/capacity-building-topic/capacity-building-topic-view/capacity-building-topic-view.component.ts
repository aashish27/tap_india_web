import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { CbTopicService } from '../../../../services/masters/cbtopic/cbtopic.service';

@Component({
  selector: 'app-capacity-building-topic-view',
  templateUrl: './capacity-building-topic-view.component.html',
  styleUrls: ['./capacity-building-topic-view.component.css']
})
export class CapacityBuildingTopicViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private cbtopicService:CbTopicService, private router: Router, private baseUrl: Apisendpoints) { }
  cbtopicData:any;;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.cbtopicService.getCbtopic(this.baseUrl.getCbtopic + '/' + params.id).subscribe((res)=>{
          this.cbtopicData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['masters/capacity-building-topic/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'capacity-building-topic', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCbtopic + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['masters/capacity-building-topic/index']);
    }
  }
}
