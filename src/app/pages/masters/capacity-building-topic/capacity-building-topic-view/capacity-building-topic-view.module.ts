import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CapacityBuildingTopicViewComponent } from './capacity-building-topic-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const CbViewRoutes: Routes = [
  {
    path: '',
    component: CapacityBuildingTopicViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(CbViewRoutes),
  ],
  declarations: [CapacityBuildingTopicViewComponent]
})
export class CapacityBuildingTopicViewModule { }