import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
export const DashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },{
        path: 'home',
        loadChildren: './dashboard-home/dashboard-home.module#DashboardHomeModule'
      },{
        path: 'graph-dashboard',
        loadChildren: './graph-dashboard/graph-dashboard.module#GraphDashboardModule'
      },{
        path: 'report-dashboard',
        loadChildren: './report-dashboard/report-dashboard.module#ReportDashboardModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
