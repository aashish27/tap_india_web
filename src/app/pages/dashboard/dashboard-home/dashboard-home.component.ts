import { Component, OnInit } from '@angular/core';
import { VillageService } from '../../../services/masters/village/village.service';
import { BlockService } from '../../../services/masters/block/block.service';
import { DistrictService } from '../../../services/masters/district/district.service';
import { Apisendpoints } from '../../../apisendpoints';
import { CommonFunctionService } from '../../../services/common-service/common-service.service';
import { map } from 'leaflet';
import * as L from 'leaflet';

declare var MarkerClusterer:any;
declare const $:any;

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit {
  interventionLocation: { lat: number; lng: number; }[];
  nonInterventionLocation: { lat: number; lng: number; }[];

  popupHeader:string = "";
  tableData:any;
  popUpData:any;
  tableHeader:any;
  keys:any;

  //counts
  interventionCount:string = "";
  noninterventionCount:string = "";
  blockCount:string = "";
  districtCount:string = "";

  programChartData:any = {};
  attendenceChartData:any = {};
  overviewChartData:any = {};
  learningChartData:any = {};
  dropoutChartData:any = {};
  mainstreamChartData:any = {};
  donorChartData:any = {};

  //map
  StatesJSON:any = null;
  DistrictsJSON:any = null;
  map:any = null;

  constructor(
    private villageService: VillageService, 
    private blockService: BlockService, 
    private districtService:DistrictService,
    private baseUrl: Apisendpoints,
    private common: CommonFunctionService) { }

  ngOnInit() {

    this.common.getOoscDetails("https://vrindarak.surveycto.com/api/v1/forms/data/wide/json/out_of_scholl_children").subscribe((data)=>{
      console.log(data);
      
    });

    this.loadMapGeoJson();

    $('#from-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
        //  this.cropDemonstrationForm.patchValue({
        //   activityDate : date
        //  });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    $('#to-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
        //  this.cropDemonstrationForm.patchValue({
        //   activityDate : date
        //  });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    
    // this.getTableData();
    this.getProgramChartData('bar','center-chart');
    this.getAttendenceChartData('bar', 'attendence-chart');
    this.getOverviewChartData('bar', 'overview-chart');
    this.getLearningChartData('bar', 'learning-chart');
    this.getDropoutChartData('bar', 'dropout-chart');
    this.getMainstreamChartData('line', 'mainstream-chart');
    this.getDonorWiseChartData('bar', 'donor-chart');
  }
  
  loadMapGeoJson(){
    let prmsArr = [];
    let prms1 = new Promise((resolve, reject) => {
      $.getJSON('../assets/jsonFile/state_boundries.json').done((response) => {//All State JSON Data
        this.StatesJSON = response;
        resolve();
      }).fail((failed) => {
          console.log('State Boundries getJSON request failed!', failed);
        });
      });
      prmsArr.push(prms1);
      
      let prms2 = new Promise((resolve, reject) => {
        $.getJSON('../assets/jsonFile/updated_district_9_July.json').done((resp) => {//All District JSON Data
          this.DistrictsJSON = resp;
          resolve();
        }).fail((failed) => {
          console.log('District Boundries getJSON request failed!', failed);
        });
      });
      prmsArr.push(prms2);
      
      Promise.all(prmsArr).then(()=>{
        //loadMap
        this.initMap();
      })
    }
          
  initMap(){
      this.map = L.map('mapid', {
        scrollWheelZoom: false,
        dragging: false
      }).setView([20.59, 78.96], 4.499999);

      let stateLayer =  L.geoJSON(
                this.StatesJSON,
                {style: this.stateColorStyle}
              ).addTo(this.map); 


      if(stateLayer){
        this.map.fitBounds(stateLayer.getBounds());
      }

      stateLayer.eachLayer((layer:any)=>{

        if(layer.feature.properties.ST_NM == "Haryana"){
          layer.setStyle({
            fillOpacity: 1,
            fillColor: '#00A7D2',
            weight: -1,
            color: '#cccccc',
          }); 
        }else if(layer.feature.properties.ST_NM == "Uttar Pradesh"){
          layer.setStyle({
            fillOpacity: 1,
            fillColor: '#2e8c39',
            weight: -1,
            color: '#cccccc',
          }); 
        }

        layer.on({
        mouseover: function(){
            let dummy = [
              { state: "Haryana", districts: 30, blocks: 100},
              { state: "Uttar Pradesh",districts: 150, blocks: 2000}
            ];
            let obj = null;
            obj = dummy.find(el => el.state == layer.feature.properties.ST_NM);

            if(obj != undefined){
              stateLayer.bindTooltip('<p>State : <b>' + layer.feature.properties.ST_NM + '</b> </p> <p>Districts : <b>' + obj.districts + '</b> </p> <p>Blocks : <b>' + obj.blocks + '</b> </p>',
                {className: 'tooltip-custom', opacity: 1}
              );               
            }else{
              stateLayer.bindTooltip('<b>' + layer.feature.properties.ST_NM + '</b>');
            }
          }
        });
      });
  }

  stateColorStyle(feature){
    return {
      fillColor: '#ffffff',
      // fillColor: getStateColorByStudents(feature.properties.ST_NM),
      weight: 2,
      opacity: 1,
      color: '#ece5e5',
      fillOpacity: 0.7
    }
  }

  showTable(via = ''){
    switch(via){
      case "inter":
        this.popupHeader = "Project Intervention Villages";
        this.tableHeader = ["Sr. No.","Village Name", "Block Name"];
        this.popUpData = this.tableData.inter;
        this.keys = ["name","blockId"];
      break;
      case "non":
        this.popupHeader = "Project Non-Intervention Villages";
        this.tableHeader = ["Sr. No.", "Village Name", "Block Name"];
        this.popUpData = this.tableData.non;
        this.keys = ["name","blockId"];
      break;
      case "block":
        this.popupHeader = "Block List";
        this.tableHeader = ["Sr. No.", "Block Name"];
        this.popUpData = this.tableData.block;
        this.keys = ["name"];
      break;
      case "district":
        this.popupHeader = "District List";
        this.tableHeader = ["Sr. No.","District Name"];
        this.popUpData = this.tableData.district;
        this.keys = ["name"];
      break;
    }
  }
  
  getTableData(){
    let prmsArr=[];

    let village = new Promise((resolve, reject)=>{
      this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res1)=>{ 
        if(res1['success']){
          resolve(res1['data']);
          this.common.removeClass("non-village");
          this.common.removeClass("int-village");
        }else{
          reject();
        }
      })
    });
    prmsArr.push(village);

    let block = new Promise((resolve, reject)=>{
      this.blockService.getBlocks(this.baseUrl.getBlock).subscribe((res2)=>{
        if(res2['success']){
          resolve(res2['data']);
          this.common.removeClass("block");
        }else{
          reject();
        }
      })
    });
    prmsArr.push(block);

    let district = new Promise((resolve, reject)=>{
      this.districtService.getDistricts(this.baseUrl.getDistrict).subscribe((res3)=>{
        if(res3['success']){
          resolve(res3['data']);
          this.common.removeClass("district");
        }else{
          reject();
        }
      })
    });
    prmsArr.push(district);

    Promise.all(prmsArr).then((values)=>{
      this.tableData = {
        'inter': values[0].filter((res4)=> {return res4['type'] == "Intervention"}),
        'non': values[0].filter((res5)=> {return res5['type'] =="Non Intervention"}),
        'block': values[1],
        'district': values[2]
      }
      
      this.interventionCount = this.tableData.inter.length;
      this.noninterventionCount = this.tableData.non.length;
      this.blockCount = this.tableData.block.length;
      this.districtCount = this.tableData.district.length;

      this.initMap();
    }, (rejectErr) => {
        console.log("rejectErr", rejectErr)
    }).catch((caughtError) => {
        console.log("caughtError", caughtError)
    });
  }

  getProgramChartData(chartType, chartName){
    this.programChartData = null;
    let complete = [];

    complete['labels'] = ["Badshahpur", "Basai", "Dhanwapur", "Durga Colony", "Dhanwapur", "Noida"]
    complete['datasets'] = [
      {
        label: "ECE",
        backgroundColor: "#F39C12",
        data: [133,221,783,247, 334, 556]
      }, {
        label: "NFE",
        backgroundColor: "#00A7D2",
        data: [408,547,675,734, 532, 227]
      }, {
        label: "REC",
        backgroundColor: "#FF7285",
        data: [221,734,221,547, 112, 523]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Center';
    complete['labelString'] = 'No. of Students';

    this.programChartData = complete;
  }

  getAttendenceChartData(chartType, chartName){

    this.attendenceChartData = null;
    let complete = [];

    complete['labels'] = ["Above 50%", "Above 70%"]
    complete['datasets'] = [
      {
        type: 'line',
        label: "Gross(All partners)",
        borderColor: "#00a7d2",
        yAxisID: 'average',
        fill: true,
        strokeColor : "#ff6c23",
        pointColor : "#fff",
        pointStrokeColor : "#ff6c23",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#ff6c23",
        data: [600,1000]
      }, {
        type: 'bar',
        label: "Partner 1",
        backgroundColor: "#F39C12",
        data: [408,547]
      }, {
        type: 'bar',
        label: "Partner 2",
        backgroundColor: "#00A7D2",
        data: [221,734]
      }, {
        type: 'bar',
        label: "Partner 3",
        backgroundColor: "#2E8C39",
        data: [300,400]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Attendence';
    complete['labelString'] = ['Percentage of Students','Gross Percentage of Students'];
    complete['yId'] = 'average';

    this.attendenceChartData = complete;
  }

  getOverviewChartData(chartType, chartName){
    this.overviewChartData = null;
    let complete = [];

    complete['labels'] = ["Enrolment", "Continue", "Dropout", "Mainstream"]
    complete['datasets'] = [
      {
        label: "No. of Students",
        backgroundColor: ["#F39C12", "#00A7D2", "#FF7285", "#2E8C39"],
        data: [133,221,783,247]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Overview';
    complete['labelString'] = 'No. of Students';

    this.overviewChartData = complete;
  }

  getLearningChartData(chartType, chartName){

    this.learningChartData = null;
    let complete = [];

    complete['labels'] = ["Above 50%", "Above 70%"]
    complete['datasets'] = [
      {
        type: 'line',
        label: "Gross(All partners)",
        borderColor: "#00a7d2",
        yAxisID: 'average',
        fill: true,
        strokeColor : "#ff6c23",
        pointColor : "#fff",
        pointStrokeColor : "#ff6c23",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#ff6c23",
        data: [600,1000]
      }, {
        type: 'bar',
        label: "Partner 1",
        backgroundColor: "#F39C12",
        data: [408,547]
      }, {
        type: 'bar',
        label: "Partner 2",
        backgroundColor: "#00A7D2",
        data: [221,734]
      }, {
        type: 'bar',
        label: "Partner 3",
        backgroundColor: "#2E8C39",
        data: [300,400]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Learning Score';
    complete['labelString'] = ['Percentage of Students','Gross Percentage of Students'];
    complete['yId'] = 'average';

    this.learningChartData = complete;
  }

  getDropoutChartData(chartType, chartName){

    this.dropoutChartData = null;
    let complete = [];

    complete['labels'] = ["0-10", "11-20", "20 & above"]
    complete['datasets'] = [
      {
        type: 'line',
        label: "Gross(All partners)",
        borderColor: "#00a7d2",
        yAxisID: 'average',
        fill: true,
        strokeColor : "#2E8C39",
        pointColor : "#fff",
        pointStrokeColor : "#2E8C39",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#2E8C39",
        data: [600,1000,800]
      }, {
        type: 'bar',
        label: "Partner 1",
        backgroundColor: "#FF7285",
        data: [408,547,350]
      }, {
        type: 'bar',
        label: "Partner 2",
        backgroundColor: "#00A7D2",
        data: [221,734,400]
      }, {
        type: 'bar',
        label: "Partner 3",
        backgroundColor: "#2E8C39",
        data: [300,400,600]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Dropout';
    complete['labelString'] = ['Percentage of Students','Gross Percentage of Students'];
    complete['yId'] = 'average';

    this.dropoutChartData = complete;
  }

  getMainstreamChartData(chartType, chartName){
    this.mainstreamChartData = null;
    let complete = [];

    complete['labels'] = ["2014", "2015", "2016", "2017","2018"]
    complete['datasets'] = [
      {
        label: "No. of Students",
        borderColor: "#ff778a",
        fill: true,
        strokeColor : "#ff778a",
        pointColor : "#fff",
        pointStrokeColor : "#ff778a",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "#ff778a",
        data: [200,400,150,800,578]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Year';
    complete['labelString'] = 'No. of Students';

    this.mainstreamChartData = complete;
  }

  getDonorWiseChartData(chartType, chartName){
    this.donorChartData = null;
    let complete = [];

    complete['labels'] = ["Microsoft", "Blackberry", "Genpact", "Sankshi NGO"]
    complete['datasets'] = [
      {
        label: "In School",
        backgroundColor: "#F39C12",
        data: [133,221,783,247]
      }, {
        label: "In Community",
        backgroundColor: "#00A7D2",
        data: [408,547,675,734]
      }
    ];
    complete['chartType'] = chartType;
    complete['chartTitle'] = 'Donors';
    complete['labelString'] = 'Centres';

    this.donorChartData = complete;
  }
}
