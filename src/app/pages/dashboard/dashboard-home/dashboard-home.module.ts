import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardHomeComponent } from './dashboard-home.component';
import { CommonChartModule } from '../../../shared/common-chart/common-chart.module';
import { LeafletModule } from '@asymmetrik/angular2-leaflet';


export const DashboardHomeFormRoutes: Routes = [
  {
    path: '',
    component: DashboardHomeComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    CommonChartModule,
    LeafletModule.forRoot(),
    RouterModule.forChild(DashboardHomeFormRoutes),
  ],
  declarations: [DashboardHomeComponent]
})
export class DashboardHomeModule { }