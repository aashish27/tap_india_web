import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphDashboardComponent } from './graph-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonChartModule } from '../../../shared/common-chart/common-chart.module';
import { FormsModule } from '@angular/forms';

export const graphRoute:Routes = [
  {
    path : '',
    component : GraphDashboardComponent
  }
];

@NgModule({
  declarations: [GraphDashboardComponent],
  imports: [
    CommonModule,
    CommonChartModule,
    FormsModule,
    RouterModule.forChild(graphRoute),
  ]
})
export class GraphDashboardModule { }
