import { Component, OnInit } from '@angular/core';
import { VillageService } from '../../../services/masters/village/village.service';
import { Apisendpoints } from '../../../apisendpoints';
import { DashboardService } from '../../../services/dashboard/dashboard.service';
import { Chart } from 'chart.js';
import { ChartServiceService } from '../../../services/chart-service/chart-service.service';
import { CommonFunctionService } from '../../../services/common-service/common-service.service';
declare const $:any;
@Component({
  selector: 'app-graph-dashboard',
  templateUrl: './graph-dashboard.component.html',
  styleUrls: ['./graph-dashboard.component.scss']
})
export class GraphDashboardComponent implements OnInit {

  common_chart:any;
  common_c:any;
  ctxCanvas:any;
  dtoptions: any = {};
  table:any;

  chartData:any = {};
  villages:any;
  topics:any;
  schools:any;

  monthValue: any = "";
  yearValue: any = "";
  villageValue: any = "";
  topicValue:any = "";

  months:any = [
    { value : 1, name : 'Jan'},
    { value : 2, name : 'Feb'},
    { value : 3, name : 'Mar'},
    { value : 4, name : 'Apr'},
    { value : 5, name : 'May'},
    { value : 6, name : 'Jun'},
    { value : 7, name : 'Jul'},
    { value : 8, name : 'Aug'},
    { value : 9, name : 'Sep'},
    { value : 10, name : 'Oct'},
    { value : 11, name : 'Nov'},
    { value : 12, name : 'Dec'},
  ];

  status = {
    exposure : false,
    cdemo: false,
    water : false,
    soil : false,
    thematic: false
  }

  monthList:any;
  yearList:any;

  villageId:any = "";
  year:any = "";
  month:any = "";

  //cards
  solarCard:any = "";
  soilCard:any = "";
  horticultureCard:any = "";
  compostCard:any = "";

  constructor(private villageService:VillageService, private common: CommonFunctionService, private chartService: ChartServiceService, private baseUrl:Apisendpoints, private dashboardService: DashboardService ) { }

  ngOnInit() {
    this.monthList = this.common.getMonth();
    this.yearList = this.common.getYear();
    this.getVillages();
    this.getCardCounts();
    this.getChartData('bar','thematic');
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      if(res['success']){
        this.villages = res['data'];
      }
    })
  }

  getTopicList(){
    this.chartService.getCharts(this.baseUrl.getTopicList).subscribe((res)=>{
      if(res['success']){
        this.topics = res['data'];
      }
    })
  }

  filterData(value){
    this.getCardCounts(value.village, value.year, value.month);
  }

  getCardCounts(village = "", year = "", month = ""){
    this.dashboardService.getDashboardData(this.baseUrl.getCardCount + "?village="+ village + "&year="+ year + "&month=" + month).subscribe((res)=>{
      if(res["success"]){
        let data = res['data'];
        this.soilCard = data[0].count;
        this.compostCard = data[1].count;
        this.horticultureCard = data[2].count.toFixed(2);
        this.solarCard = data[3].count;

        this.common.removeClass("soil-count");
        this.common.removeClass("compost");
        this.common.removeClass("horti");
        this.common.removeClass("solar");
      }
    })
  }

  
  getChartData(chartType, chartName, year = '', month = '', topic = '', village = ''){
    this.yearValue = year;
    this.monthValue = month;
    this.villageValue = village;
    this.topicValue = topic;

    this.status = {
      exposure : false,
      cdemo: false,
      water : false,
      soil : false,
      thematic: false
    }

    switch(chartName){
      case 'exposure':
      this.getTopicList();
        this.getExposureTrainingData('exposure', chartType, 'Training And Exposure Visit', year, month, topic);
        this.common.addClass("exposure", "id", "card-loader-2");        
        break;
      case 'cdemo':
        this.getCropDemoData('cdemo', chartType, 'Demonstration Of Crop Specific POP (%)');
        this.common.addClass("cdemo", "id", "card-loader-2");      
        break;
      case 'water':
        this.getWaterData('water', chartType, 'Capacity Created For Water Harvesting', year, month, village);
        this.common.addClass("water", "id", "card-loader-2");        
        break;
      case 'soil':
        this.getSoilData('soil', chartType, 'Deficiency Of Nutrients In Soil Samples', year, month, village);
        this.common.addClass("soil", "id", "card-loader-2");        
        break;
      case 'thematic':
        this.getThematicData('thematic', chartType, 'Thematic Area-Wise Beneficiary Benefitted');
        this.common.addClass("thematic", "id", "card-loader-2");        
        break;
    }

  }

  getThematicData(type, chartType, title){
    this.chartData = null;
    this.chartService.getCharts(this.baseUrl.getThematic).subscribe((res)=>{
      if(res){
        let response = res['data'];
        this.common.removeClass("thematic", "id", "card-loader-2");
        let data = [], complete = [], elements = [];

        elements = response.map((res)=>{
          return res.label;
        });

        if(response){
          let nutrient = {};
          nutrient['label'] = "Beneficiary Count";
          nutrient['borderColor'] = "#90ED7D";
          nutrient['backgroundColor'] = "#90ED7D";
          nutrient['borderWidth'] = 1;
          nutrient['fill'] = false;
          nutrient['data'] = response.map((values)=>{
              return parseInt(values.count).toFixed(2);
          });
          data.push(nutrient);
        }

        complete['labels'] = elements;
        complete['datasets'] = data;
        complete['chartType'] = chartType;
        complete['chartTitle'] = title;

        this.chartData = complete;    
        
        this.status = {
          exposure : false,
          cdemo: false,
          water : false,
          soil : true,
          thematic: true
        }
      }
    });
  }

  getExposureTrainingData(type, chartType, title, year = '', month = '', topic = ''){
    this.chartData = null;
    this.chartService.getCharts(this.baseUrl.getCapacityExposure + 'year=' + year + '&month=' + month + '&cbTopicId=' + topic).subscribe((res)=>{
      if(res){
        let response = res['data'];
        this.common.removeClass("exposure", "id", "card-loader-2");
        let data = [], complete = [];
        Object.keys(response).forEach((key)=>{
          switch(key){
            case 'monthly':
              let monthly = {};
              monthly['label'] = "Total Monthly";
              monthly['borderColor'] = "#7CB5EC";
              monthly['backgroundColor'] = "#7CB5EC";
              monthly['borderWidth'] = 1;
              monthly['fill'] = false;
              monthly['data'] = response.monthly.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(monthly);
            break;
            case 'ptd':
              let ptd = {};
              ptd['label'] = "Project Till Date(PTD)";
              ptd['borderColor'] = "#90ED7D";
              ptd['backgroundColor'] = "#90ED7D";
              ptd['borderWidth'] = 1;
              ptd['fill'] = false;
              ptd['data'] = response.ptd.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(ptd);
            break;
          }
        })

        // let length = response.zeroTillage.length;
        // let labelArr = [], i = 0;
        // while(length > 0){
        //   labelArr.push(this.months.map((res)=>{ return res.name })[i]);
        //   length--;
        //   i++
        // }

        complete['labels'] = this.months.map((res)=>{ return res.name });
        complete['datasets'] = data;
        complete['chartType'] = chartType;
        complete['chartTitle'] = title;

        this.chartData = complete;    
        
        this.status = {
          exposure : true,
          cdemo: false,
          water : false,
          soil : false,
          thematic: false
        }
      }
    });
  }

  getWaterData(type, chartType, title, year = '', month = '', village = ''){
    this.chartData = null;
    this.chartService.getCharts(this.baseUrl.getWaterHarvestedGraph + '?year=' + year + '&month=' + month + '&village=' + village).subscribe((res)=>{
      if(res){
        let response = res['data'];
        this.common.removeClass("water", "id", "card-loader-2");

        let data = [], complete = [];
        Object.keys(response).forEach((key)=>{
          switch(key){
            case 'farmBunding':
              let farmBunding = {};
              farmBunding['label'] = "Farm Bunding (Kilo Litres)";
              farmBunding['borderColor'] = "#7CB5EC";
              farmBunding['backgroundColor'] = "#7CB5EC";
              farmBunding['borderWidth'] = 1;
              farmBunding['fill'] = false;
              farmBunding['data'] = response.farmBunding.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(farmBunding);
            break;
            case 'farmPond':
              let farmPond = {};
              farmPond['label'] = "Farm Pond (Kilo Litres)";
              farmPond['borderColor'] = "#26b587";
              farmPond['backgroundColor'] = "#26b587";
              farmPond['borderWidth'] = 1;
              farmPond['fill'] = false;
              farmPond['data'] = response.farmPond.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(farmPond);
            break;
            case 'pondRejuvination':
              let pondRejuvination = {};
              pondRejuvination['label'] = "Pond Rejuvenation (Kilo Litres)";
              pondRejuvination['borderColor'] = "#90ed7d";
              pondRejuvination['backgroundColor'] = "#90ed7d";
              pondRejuvination['borderWidth'] = 1;
              pondRejuvination['fill'] = false;
              pondRejuvination['data'] = response.pondRejuvination.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(pondRejuvination);
            break;
            case 'monthly':
              let monthly = {};
              monthly['label'] = "Total Monthly";
              monthly['borderColor'] = "#f7a35c";
              monthly['backgroundColor'] = "#f7a35c";
              monthly['borderWidth'] = 1;
              monthly['fill'] = false;
              monthly['data'] = response.monthly.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(monthly);
            break;
            case 'ptd':
              let ptd = {};
              ptd['label'] = "Project Till Date(PTD)";
              ptd['borderColor'] = "#ef6bea";
              ptd['backgroundColor'] = "#ef6bea";
              ptd['borderWidth'] = 1;
              ptd['fill'] = false;
              ptd['data'] = response.ptd.map((values)=>{
                  return parseInt(values.value).toFixed(2);
              });
              data.push(ptd);
            break;
          }
        })

        complete['labels'] = this.months.map((res)=>{ return res.name });
        complete['datasets'] = data;
        complete['chartType'] = chartType;
        complete['chartTitle'] = title;

        this.chartData = complete;    
        
        this.status = {
          exposure : false,
          cdemo: false,
          water : true,
          soil : false,
          thematic: false
        }
      }
      
    });
  }

  getSoilData(type, chartType, title, year = '', month = '', village = ''){
    this.chartData = null;
    this.chartService.getCharts(this.baseUrl.getSoilTestingGraph + '?village=' + village + '&year=' + year + '&month=' + month).subscribe((res)=>{
      if(res){
        let response = res['data'];
        this.common.removeClass("soil", "id", "card-loader-2");
        let data = [], complete = [], elements = [];

        elements = response.map((res)=>{
          return res.label;
        });

        if(response){
          let nutrient = {};
          nutrient['label'] = "Deficiency of nutrients (%)";
          nutrient['borderColor'] = "#90ED7D";
          nutrient['backgroundColor'] = "#90ED7D";
          nutrient['borderWidth'] = 1;
          nutrient['fill'] = false;
          nutrient['data'] = response.map((values)=>{
              return parseInt(values.count).toFixed(2);
          });
          data.push(nutrient);
        }

        complete['labels'] = elements;
        complete['datasets'] = data;
        complete['chartType'] = chartType;
        complete['chartTitle'] = title;

        this.chartData = complete;    
        
        this.status = {
          exposure : false,
          cdemo: false,
          water : false,
          soil : true,
          thematic: false
        }
      }
    });
  }

  getCropDemoData(type, chartType, title){
    this.chartData = null;
    this.chartService.getCharts(this.baseUrl.getCropDemo).subscribe((res)=>{
      let data = res['data'];
      this.common.removeClass("cdemo", "id", "card-loader-2");
      if(data){
        let complete = [], dataset = [], control = {}, demo = {}, ptd = {};

        control['label'] = "Control Plot Yield (kg/Acre)";
        control['borderColor'] = "#7CB5EC";
        control['backgroundColor'] = "#7CB5EC";
        control['borderWidth'] = 1;
        control['fill'] = false;
        control['data'] = data.map((values)=>{
            return parseInt(values.controlPlotyield).toFixed(2);
        });
        dataset.push(control);

        demo['label'] = "Demo Plot Yield (kg/Acre)";
        demo['borderColor'] = "#26b587";
        demo['backgroundColor'] = "#26b587";
        demo['borderWidth'] = 1;
        demo['fill'] = false;
        demo['data'] = data.map((values)=>{
            return parseInt(values.demoPlotYield).toFixed(2);
        });
        dataset.push(demo);

        ptd['label'] = "Project Till Date (PTD)";
        ptd['borderColor'] = "#ef6bea";
        ptd['backgroundColor'] = "#ef6bea";
        ptd['borderWidth'] = 1;
        ptd['type'] = "line";
        ptd['fill'] = false;
        ptd['data'] = data.map((values)=>{
            return parseInt(values.ptd).toFixed(2);
        });
        dataset.push(ptd);

        
        complete['labels'] = data.map((value)=>{ return value.year});
        complete['datasets'] = dataset;
        complete['chartType'] = chartType;
        complete['chartTitle'] = title;

        this.chartData = complete;
        this.status = {
          exposure : false,
          cdemo: true,
          water : false,
          soil : false,
          thematic: false
        }
      }
    });
  }

}
