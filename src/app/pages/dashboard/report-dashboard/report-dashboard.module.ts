import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportDashboardComponent } from './report-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonChartModule } from '../../../shared/common-chart/common-chart.module';
import { FormsModule } from '@angular/forms';

export const ReportRoute:Routes = [
  {
    path : '',
    component : ReportDashboardComponent
  }
];

@NgModule({
  declarations: [ReportDashboardComponent],
  imports: [
    CommonModule,
    CommonChartModule,
    FormsModule,
    RouterModule.forChild(ReportRoute),
  ]
})
export class ReportDashboardModule { }
