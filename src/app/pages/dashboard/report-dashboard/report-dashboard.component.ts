import { Component, OnInit } from '@angular/core';
import { VillageService } from '../../../services/masters/village/village.service';
import { Apisendpoints } from '../../../apisendpoints';
import { DashboardService } from '../../../services/dashboard/dashboard.service';
import { CommonFunctionService } from '../../../services/common-service/common-service.service';
import { DistrictService } from '../../../services/masters/district/district.service';
import { BlockService } from '../../../services/masters/block/block.service';

declare const $:any;

@Component({
  selector: 'app-report-dashboard',
  templateUrl: './report-dashboard.component.html',
  styleUrls: ['./report-dashboard.component.scss']
})
export class ReportDashboardComponent implements OnInit {
  dtoptions: any = {};
  reportTable:any;
  reportData:any;
  headers:any;
  count:number = 0;
  Arr = Array; 

  //filter
  villages:any;
  villageId:any = "";
  districts:any;
  districtId:any = "";
  blocks:any = "";
  blockId:any = "";
  activityTimes:any = "";

  maleCount:number;
  femaleCount:number;
  beneficiaryCount:number;

  constructor(
    private villageService: VillageService, 
    private districtService: DistrictService,
    private blockService: BlockService,
    private common: CommonFunctionService, 
    private baseUrl:Apisendpoints, 
    private dashboardService: DashboardService ) { }

  ngOnInit() {
    this.getReportData();
    this.getBlocks();
  }

  // getDistricts(){
  //   this.districtService.getDistricts(this.baseUrl.getDistrict).subscribe((res)=>{
  //     if(res['success']){
  //       this.districts = res['data'];
  //     }
  //   })
  // }

  getBlocks(){
    this.blockService.getBlocks(this.baseUrl.getBlock).subscribe((res)=>{
      if(res['success']){
        this.blocks = res['data'];
      }
    })
  }

  getVillages(block = ""){
    this.villages = null;
    this.villageId = "";
    if(block != ""){
      this.villageService.getVillages(this.baseUrl.getVillage + '?block=' + block).subscribe((res)=>{
        if(res['success']){
          this.villages = res['data'];
        }
      });
    }
  }

  filterData(value){
    this.getReportData(value.times, value.block, value.village, "filter");
  }

  getReportData(times = "", block = "", village = "", via = ""){
    var self = this;
    this.dashboardService.getDashboardData(this.baseUrl.getReportData + "block=" + block + "&village=" + village).subscribe((res)=>{
      // if(res['success']){
        if(res['data'].length && via == ""){
          this.headers = res['data'][0].activityArr;
        }

        if(times != ""){
          let filter = res['data'].filter((data)=> data.activityParticipate == +times);
          this.reportData['data'] = filter;
         
          this.count = this.headers.length + 1;
  
          this.beneficiaryCount = this.reportData['data'].length;
          
          let male = this.reportData['data'].filter((data)=> data.benGender == "male");
          let female = this.reportData['data'].filter((data)=> data.benGender == "female");
  
          this.maleCount = male.length;
          this.femaleCount = female.length;
        }else{
          this.reportData = res;
          this.count = this.headers.length + 1;
  
          this.beneficiaryCount = res['data'].length;
          
          let male = res['data'].filter((data)=> data.benGender == "male");
          let female = res['data'].filter((data)=> data.benGender == "female");
  
          this.maleCount = male.length;
          this.femaleCount = female.length;
        }
        if(via == ""){
          setTimeout(function(){
            self.plotReport();
          }, 500);
        }
      // }
    })
  }

  plotReport(){
    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", (e) => {
        $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
    });

      this.dtoptions = {
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        "bFilter": false,
        "bAutoWidth": false,
        scrollY: "40vh",
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Overall filters",
        },
        dom: 'lBfrtip',
        buttons: [
            {extend: 'csv', text: 'Download CSV', filename: "Beneficiary-wise-activity-coverage"}
            //'copy', 'csv', 'excel', 'pdf', 'print'
        ]
      }

      this.reportTable = $('#report-table').DataTable(this.dtoptions);
  }

  exportCSV(val){   // export csv button outside of table 
    this.reportTable.button( '.buttons-csv' ).trigger();
  }

}
