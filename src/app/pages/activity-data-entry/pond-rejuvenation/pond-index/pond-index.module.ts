import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PondIndexComponent } from './pond-index.component';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const IndexRoutes : Routes = [
  {
    path : '',
    component : PondIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(IndexRoutes)
  ],
  declarations: [PondIndexComponent]
})
export class PondIndexModule { }
