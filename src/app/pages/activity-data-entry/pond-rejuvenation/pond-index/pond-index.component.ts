import { Component, OnInit } from '@angular/core';
import { PondRejuvenationService } from '../../../../services/activity/pond-rejuvenation/pond-rejuvenation.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router'
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-pond-index',
  templateUrl: './pond-index.component.html',
  styleUrls: ['./pond-index.component.css']
})
export class PondIndexComponent implements OnInit {
  pond : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private pondService: PondRejuvenationService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.pondService.getPond(this.baseUrl.getPond).subscribe( res=> { 
      this.pond  = res;
      if(this.pond.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'pond-rejuvenation', action: 'delete'})){
      this.delete_id = this.baseUrl.deletePond + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/pond-rejuvenation/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['activity-data-entry/pond-rejuvenation/view'], {queryParams: {id : data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Pond-rejuvenation",
            footer: false,
            exportOptions: {
              columns: [1,2,3,4,5,6,7,8,9,10,11]
            }
          }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#pondTable').DataTable(this.dtoptions);
    this.table.columns( [ 1, 2, 6, 7, 8, 10, 11, 12, 13, 14 ] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }
  
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'pond-rejuvenation', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
