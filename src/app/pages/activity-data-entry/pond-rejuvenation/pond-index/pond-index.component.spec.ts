import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PondIndexComponent } from './pond-index.component';

describe('PondIndexComponent', () => {
  let component: PondIndexComponent;
  let fixture: ComponentFixture<PondIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PondIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PondIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
