import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PondRejuvenationComponent } from './pond-rejuvenation.component';

describe('PondRejuvenationComponent', () => {
  let component: PondRejuvenationComponent;
  let fixture: ComponentFixture<PondRejuvenationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PondRejuvenationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PondRejuvenationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
