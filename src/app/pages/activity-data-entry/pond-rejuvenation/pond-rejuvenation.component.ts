import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-pond-rejuvenation',
  templateUrl: './pond-rejuvenation.component.html',
  animations:[
    topToBottom
  ]
})
export class PondRejuvenationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
