import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PondRejuvenationComponent } from './pond-rejuvenation.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';

export const PondRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './pond-index/pond-index.module#PondIndexModule',
        data: {component: 'pond-rejuvenation', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './pond-view/pond-view.module#PondViewModule',
        data: {component: 'pond-rejuvenation', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './pond-form/pond-form.module#PondFormModule',
        data: {component: 'pond-rejuvenation', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PondRoutes)
  ],
  declarations: [PondRejuvenationComponent]
})
export class PondRejuvenationModule { }
