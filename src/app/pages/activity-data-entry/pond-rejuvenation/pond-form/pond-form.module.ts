import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PondFormComponent } from './pond-form.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const FormRoutes : Routes = [
  {
    path : '',
    component : PondFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(FormRoutes)
  ],
  declarations: [PondFormComponent]
})
export class PondFormModule { }
