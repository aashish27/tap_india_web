import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PondRejuvenationService } from '../../../../services/activity/pond-rejuvenation/pond-rejuvenation.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import * as moment from 'moment';
import { VillageService } from '../../../../services/masters/village/village.service';

declare const $:any;

@Component({
  selector: 'app-pond-form',
  templateUrl: './pond-form.component.html',
  styleUrls: ['./pond-form.component.css']
})
export class PondFormComponent implements OnInit {
  pondForm: FormGroup;
  pondData:any;
  id:any = "";
  submitted = false;
  villages:any = {};

  activityId:any;
  order:any = 7;
  
  constructor(
    private pondService: PondRejuvenationService,
    private villageService:VillageService,
    private router: Router , 
    private baseUrl: Apisendpoints,
    private common: CommonFunctionService,
    private route: ActivatedRoute
  ) {
    this.activityId = this.common.get(this.order);
  }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.pondForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    this.getVillages();

    this.pondForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.pondService.getPond(this.baseUrl.getPondById + params.id).subscribe((res)=>{
            this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });
  }

  setFormData(val){
    this.pondData = val['data'];
    this.id = this.pondData['_id'];
    this.pondForm.setValue({ 
      activityId : this.activityId,
      activityDate : this.pondData['activityDate'] ? moment(this.pondData['activityDate']).format('YYYY-MM-DD') : "",
      beneficiaryBenefitted : this.pondData['beneficiaryBenefitted'],   
      areaBefore : this.pondData['areaBefore'],
      areaBeforeL : this.pondData['areaBeforeL'],
      areaBeforeW : this.pondData['areaBeforeW'],
      areaBeforeD : this.pondData['areaBeforeD'],
      areaAfter : this.pondData['areaAfter'],
      areaAfterL : this.pondData['areaAfterL'],
      areaAfterW : this.pondData['areaAfterW'],
      areaAfterD : this.pondData['areaAfterD'],
      waterStorageCapacityBefore : this.pondData['waterStorageCapacityBefore'],
      waterStorageCapacityAfter : this.pondData['waterStorageCapacityAfter'],
      increaseInWaterStorageCapacity : this.pondData['increaseInWaterStorageCapacity'],
      villageId : this.pondData['villageData']?this.pondData['villageData']._id : this.pondData['villageId'],
    });
  }

  createFormGroup(){
    return new FormGroup({
      activityDate: new FormControl('',Validators.required),
      activityId: new FormControl(this.activityId,Validators.required),
      beneficiaryBenefitted: new FormControl('',Validators.required),
      areaBefore: new FormControl(''),
      areaBeforeL: new FormControl(''),
      areaBeforeW: new FormControl(''),
      areaBeforeD: new FormControl(''),
      areaAfter: new FormControl(''),
      areaAfterL: new FormControl(''),
      areaAfterW: new FormControl(''),
      areaAfterD: new FormControl(''),
      waterStorageCapacityBefore : new FormControl(''),
      waterStorageCapacityAfter : new FormControl(''),
      increaseInWaterStorageCapacity : new FormControl(''),
      villageId : new FormControl('',Validators.required)
    })
  }

  status(event:any){
    return event.target.value = "";
  }

  revert() {
    this.pondForm.patchValue({
      activityDate: "",
      beneficiaryBenefitted: "",
      areaBefore: "",
      areaBeforeL: "",
      areaBeforeW: "",
      areaBeforeD: "",
      areaAfter: "",
      areaAfterL: "",
      areaAfterW: "",
      areaAfterD: "",
      waterStorageCapacityBefore : "",
      waterStorageCapacityAfter : "",
      increaseInWaterStorageCapacity : "",
      villageId : ""
    });
  }
  
  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

 onSubmit(value) { // Submit Form ..
  this.pondForm.setErrors({invalid : true}); 
    if(this.id){ 
      this.pondService.putPond(this.baseUrl.putPond + this.id, this.pondForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/pond-rejuvenation/index']);
        }
        this.pondForm.setErrors(null); 
      },(error) => {
        this.pondForm.setErrors(null); 
        console.log(error);
      });
    }else {   
      this.pondService.postPond(this.baseUrl.postPond, this.pondForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['activity-data-entry/pond-rejuvenation/index']);
        }
        this.pondForm.setErrors(null); 
      },(error) => {
        this.pondForm.setErrors(null); 
        console.log(error);
      });
    }
    // if (this.pondForm.invalid) {
    //     return;
    // }
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.pondForm.value))
  }

  //area calculations
  areaBeforeCalculate(){
    let l = parseFloat(this.pondForm.controls['areaBeforeL'].value);
    let w = parseFloat(this.pondForm.controls['areaBeforeW'].value);
    let d = parseFloat(this.pondForm.controls['areaBeforeD'].value);

    this.pondForm.patchValue({
      areaBefore: this.areaFormula(l, w, d),
      waterStorageCapacityBefore : this.areaFormula(l, w, d)
    });
    this.calculateIncrease();
  }
  areaAfterCalculate(){
    let l = parseFloat(this.pondForm.controls['areaAfterL'].value);
    let w = parseFloat(this.pondForm.controls['areaAfterW'].value);
    let d = parseFloat(this.pondForm.controls['areaAfterD'].value);

    this.pondForm.patchValue({
      areaAfter: this.areaFormula(l, w, d),
      waterStorageCapacityAfter: this.areaFormula(l, w, d)
    });
    this.calculateIncrease();
  }

  calculateIncrease(){
    this.pondForm.patchValue({
      increaseInWaterStorageCapacity: this.pondForm.controls['waterStorageCapacityAfter'].value - this.pondForm.controls['waterStorageCapacityBefore'].value 
    });
  }

  areaFormula(l, w, d){
    let area = ((l || 0) * (w || 0) * (d|| 0)).toFixed(2);
    return area;
  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  checkInt(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  
}
