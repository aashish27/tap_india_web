import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PondViewComponent } from './pond-view.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : PondViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [PondViewComponent]
})
export class PondViewModule { }
