import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { PondRejuvenationService } from '../../../../services/activity/pond-rejuvenation/pond-rejuvenation.service';

@Component({
  selector: 'app-pond-view',
  templateUrl: './pond-view.component.html',
  styleUrls: ['./pond-view.component.css']
})
export class PondViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private pondService: PondRejuvenationService, private router: Router, private baseUrl: Apisendpoints) { }
  pondData:any;
  table:any;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.pondService.getPond(this.baseUrl.getPondById + params.id).subscribe((res)=>{
          this.pondData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/pond-rejuvenation/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'pond-rejuvenation', action: 'delete'})){
      this.delete_id = this.baseUrl.deletePond + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/pond-rejuvenation/index']);
    }
  }
}
