import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PondViewComponent } from './pond-view.component';

describe('PondViewComponent', () => {
  let component: PondViewComponent;
  let fixture: ComponentFixture<PondViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PondViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PondViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
