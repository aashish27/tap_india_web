import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmBundingComponent } from './farm-bunding.component';

describe('FarmBundingComponent', () => {
  let component: FarmBundingComponent;
  let fixture: ComponentFixture<FarmBundingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmBundingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmBundingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
