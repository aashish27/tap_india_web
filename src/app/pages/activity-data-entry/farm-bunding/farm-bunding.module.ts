import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmBundingComponent } from './farm-bunding.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';

export const FarmRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'form',
        loadChildren: './farm-bunding-form/farm-bunding-form.module#FarmBundingFormModule',
        data: {component: 'farm-bunding', action: 'create'},
        canActivate: [AuthGuard]
      },{
        path: 'index',
        loadChildren: './farm-bunding-index/farm-bunding-index.module#FarmBundingIndexModule',
        data: {component: 'farm-bunding', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './farm-bunding-view/farm-bunding-view.module#FarmBundingViewModule',
        data: {component: 'farm-bunding', action: 'view'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FarmRoutes)
  ],
  declarations: [FarmBundingComponent]
})
export class FarmBundingModule { }
