import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { VarietyService } from '../../../../services/masters/variety/variety.service';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { AreaValidator } from '../../../../validators/area.validator';
import { FarmBundingService } from '../../../../services/activity/farm-bunding/farm-bunding.service';

declare const $:any;

@Component({
  selector: 'app-farm-bunding-form',
  templateUrl: './farm-bunding-form.component.html',
  styleUrls: ['./farm-bunding-form.component.css']
})
export class FarmBundingFormComponent implements OnInit {
  farmForm: FormGroup;
  submitted = false;
  farmData:any;
  id: any;
  showBeneficiary:boolean = false;
  villages:any = {};
  beneficiaries:any = {};

  activityId:any;
  order:any = 6;

  //beneficiary list dropdown
  dropdownList:any = [];
  dropdownSettings = {};

  flag:boolean = true;

  constructor(
    private beneficiaryService: BeneficiaryService, 
    private varietyService: VarietyService,
    private cropService: CropService,
    private villageService: VillageService, 
    private farmService: FarmBundingService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute,
    private common: CommonFunctionService
    ) { 
      this.activityId = this.common.get(this.order);
    }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.farmForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });
    // this.onChanges();

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    }; 

    this.getVillages();
    
    this.farmForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.farmService.getFarmBunding(this.baseUrl.getFarmBundingById + params.id).subscribe((res)=>{
          this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });

  }

  setFormData(val){
    this.farmData = val['data'];
    this.id = this.farmData['_id'];
    this.farmForm.setValue({ 
      activityDate: this.farmData['activityDate'] ? moment(this.farmData['activityDate']).format('YYYY-MM-DD') : "",
      activityId: this.activityId,
      villageId: this.farmData['villageData']? this.farmData['villageData']._id : this.farmData['villageId'],
      beneficiaryId: this.farmData['beneficiaryData']? [{ "id" : this.farmData['beneficiaryData']._id, "itemName" : this.farmData['beneficiaryData'].code + '-' + this.farmData['beneficiaryData'].name + '-' + this.farmData['beneficiaryData'].fatherName}] :this.farmData['beneficiaryId'],
      area: this.farmData['area'],
      length: this.farmData['length'],
      averageAnnualRainfall: this.farmData['averageAnnualRainfall'],
      waterHarvested: this.farmData['waterHarvested']
    });
    this.getBeneficiary('update');
  }

  // onChanges(): void {
  //   this.farmForm.statusChanges.subscribe(val => {
  //         console.log(val);
          
  //   });
  // }

  createFormGroup(){
    return new FormGroup({
      activityId: new FormControl(this.activityId,Validators.required),
      activityDate: new FormControl('',Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      length: new FormControl('',AreaValidator),
      area: new FormControl('',AreaValidator),
      averageAnnualRainfall: new FormControl('',AreaValidator),
      waterHarvested : new FormControl('')
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.farmForm && this.farmForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.farmForm && this.farmForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.farmForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.farmForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.farmForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.farmForm.controls['beneficiaryId'].reset("");
    }
    this.farmForm.controls['villageId'].markAsTouched();
  }

  redirectToBeneficiary(village_id){
    this.farmForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : 'activity-data-entry/farm-bunding/form',
        data : JSON.stringify(this.farmForm.value)
      }
    })
  }
  
  onChanges(): void {
    this.farmForm.patchValue({
      waterHarvested : ((parseFloat(this.farmForm.controls['averageAnnualRainfall'].value || 0)/1000) * 4047 * parseFloat(this.farmForm.controls['area'].value || 0) * 0.30).toFixed(2) || 0
    });
  }

  onSubmit(val) {
    this.farmForm.value['beneficiaryId'] = this.farmForm.value['beneficiaryId'][0].id;
    this.farmForm.setErrors({invalid : true});   
    if(this.id){
      this.farmService.putFarmBunding(this.baseUrl.putFarmBunding + this.id, this.farmForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/farm-bunding/index']);
        }
        this.farmForm.setErrors(null);  
      },(error) => {
        this.farmForm.setErrors(null);  
        console.log(error);
      });
    }else {
      this.farmService.postFarmBunding(this.baseUrl.postFarmBunding, this.farmForm.value ).subscribe( response=> { 
        if(response['success']){   
          this.router.navigate(['activity-data-entry/farm-bunding/index']);
        }
        this.farmForm.setErrors(null);  
      },(error) => {
        this.farmForm.setErrors(null);  
        console.log(error);
      });
    }

  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {
    this.dropdownList = [];
    this.farmForm.patchValue({
      activityId: "",
      activityDate: "",
      villageId: "",
      beneficiaryId: [],
      length: "",
      area: "",
      averageAnnualRainfall: "",
      waterHarvested : ""
    });
  }
}

