import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmBundingFormComponent } from './farm-bunding-form.component';

describe('FarmBundingFormComponent', () => {
  let component: FarmBundingFormComponent;
  let fixture: ComponentFixture<FarmBundingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmBundingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmBundingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
