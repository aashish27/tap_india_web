
import { Component, OnInit } from '@angular/core';
import { FarmBundingService } from '../../../../services/activity/farm-bunding/farm-bunding.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../guard/auth.guard';

@Component({
  selector: 'app-farm-bunding-index',
  templateUrl: './farm-bunding-index.component.html',
  styleUrls: ['./farm-bunding-index.component.css']
})
export class FarmBundingIndexComponent implements OnInit {
  farmBunding : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private FarmBundingService: FarmBundingService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.FarmBundingService.getFarmBunding(this.baseUrl.getFarmBunding).subscribe( res=> { 
      this.farmBunding  = res;
      if(this.farmBunding.success){
        setTimeout(function(){
          self.inittable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'farm-bunding', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteFarmBunding + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/farm-bunding/form'], {queryParams: { id: data._id}});
    }else {
      this.router.navigate(['activity-data-entry/farm-bunding/view'], {queryParams: { id: data._id}});
    }
  }

  inittable(){ // Data table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Farm-Bunding",
            footer: false,
            exportOptions: {
              columns: [1,2,3,4,5,6,7,8,9,10]
          }
        }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#farmTable').DataTable(this.dtoptions);
    this.table.columns( [ 1, 2, 5, 6 ] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'distrcrop-demonstrationict', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}

