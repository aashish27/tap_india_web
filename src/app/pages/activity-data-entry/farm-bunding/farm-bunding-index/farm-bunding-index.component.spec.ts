import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmBundingIndexComponent } from './farm-bunding-index.component';

describe('FarmBundingIndexComponent', () => {
  let component: FarmBundingIndexComponent;
  let fixture: ComponentFixture<FarmBundingIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmBundingIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmBundingIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
