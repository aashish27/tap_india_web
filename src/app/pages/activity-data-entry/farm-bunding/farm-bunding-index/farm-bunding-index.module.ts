import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmBundingIndexComponent } from './farm-bunding-index.component';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';


export const IndexRoutes : Routes = [
  {
    path : '',
    component : FarmBundingIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(IndexRoutes),
    PopupModule
  ],
  declarations: [FarmBundingIndexComponent]
})
export class FarmBundingIndexModule { }
