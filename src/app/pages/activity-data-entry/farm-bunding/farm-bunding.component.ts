import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farm-bunding',
  template : '<router-outlet></router-outlet>'
})
export class FarmBundingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
