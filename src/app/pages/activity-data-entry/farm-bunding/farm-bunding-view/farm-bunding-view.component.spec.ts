import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmBundingViewComponent } from './farm-bunding-view.component';

describe('FarmBundingViewComponent', () => {
  let component: FarmBundingViewComponent;
  let fixture: ComponentFixture<FarmBundingViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmBundingViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmBundingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
