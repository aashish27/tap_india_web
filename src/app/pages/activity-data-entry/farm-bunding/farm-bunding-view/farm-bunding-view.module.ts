import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FarmBundingViewComponent } from './farm-bunding-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : FarmBundingViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [FarmBundingViewComponent]
})
export class FarmBundingViewModule { }
