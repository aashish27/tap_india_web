import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../guard/auth.guard';
import { FarmBundingService } from '../../../../services/activity/farm-bunding/farm-bunding.service';

@Component({
  selector: 'app-farm-bunding-view',
  templateUrl: './farm-bunding-view.component.html',
  styleUrls: ['./farm-bunding-view.component.css']
})
export class FarmBundingViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private farmService:FarmBundingService, private baseUrl: Apisendpoints, private authGuard: AuthGuard) { }
  farmData:any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.farmService.getFarmBunding(this.baseUrl.getFarmBundingById + params.id).subscribe((res)=>{
          this.farmData = res['data'];
        })
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/farm-bunding/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['activity-data-entry/farm-bunding/index'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'farm-bunding', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteFarmBunding + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/farm-bunding/index']);
    }
  }
}

