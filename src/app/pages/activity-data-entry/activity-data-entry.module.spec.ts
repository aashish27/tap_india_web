import { ActivityDataEntryModule } from './activity-data-entry.module';

describe('ActivityDataEntryModule', () => {
  let activityDataEntryModule: ActivityDataEntryModule;

  beforeEach(() => {
    activityDataEntryModule = new ActivityDataEntryModule();
  });

  it('should create an instance', () => {
    expect(activityDataEntryModule).toBeTruthy();
  });
});
