import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilViewComponent } from './soil-view.component';

describe('SoilViewComponent', () => {
  let component: SoilViewComponent;
  let fixture: ComponentFixture<SoilViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
