import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../guard/auth.guard';
import { SoilTestingService } from '../../../../services/activity/soil-testing/soil-testing.service';

@Component({
  selector: 'app-soil-view',
  templateUrl: './soil-view.component.html',
  styleUrls: ['./soil-view.component.css']
})
export class SoilViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private soilService:SoilTestingService, private authGuard: AuthGuard, private baseUrl: Apisendpoints) { }
  soilData: any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.soilService.getSoilTesting(this.baseUrl.getSoilTestingById + params.id).subscribe((res)=>{
          this.soilData = res['data'];
        });
      }
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/soil-testing/form'], {queryParams: {id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'soil-testing', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteSoilTesting + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/soil-testing/index']);
    }
  }
}
