import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SoilViewComponent } from './soil-view.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : SoilViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [SoilViewComponent]
})
export class SoilViewModule { }
