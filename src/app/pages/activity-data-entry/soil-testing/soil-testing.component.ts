import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-soil-testing',
  template: '<router-outlet></router-outlet>'
})
export class SoilTestingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
