import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SoilFormComponent } from './soil-form.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/multiselect.component';

export const FormRoutes : Routes = [
  {
    path : '',
    component : SoilFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(FormRoutes),
    AngularMultiSelectModule
  ],
  declarations: [SoilFormComponent]
})
export class SoilFormModule { }
