import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilFormComponent } from './soil-form.component';

describe('SoilFormComponent', () => {
  let component: SoilFormComponent;
  let fixture: ComponentFixture<SoilFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
