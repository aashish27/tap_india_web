import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { SoilTestingService } from '../../../../services/activity/soil-testing/soil-testing.service';

declare const $:any;

@Component({
  selector: 'app-soil-form',
  templateUrl: './soil-form.component.html',
  styleUrls: ['./soil-form.component.css']
})
export class SoilFormComponent implements OnInit {
  soilForm: FormGroup;
  soilData:any;
  id: any;
  activityId:any;
  order:any = 1;
  status:string = "";

  showBeneficiary:boolean = false;

  villages:any = {};
  beneficiaries:any = {};

  //farmer list dropdown
  dropdownList:any = [];
  dropdownSettings = {};

  flag:boolean = true;

  constructor(
    private common: CommonFunctionService,
    private formBuilder: FormBuilder, 
    private beneficiaryService: BeneficiaryService, 
    private villageService: VillageService, 
    private soilService: SoilTestingService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute
    ) { 
      this.activityId = this.common.get(this.order);
    }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.soilForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e1) => {
        if (e1.keyCode == 8 || e1.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    $('#collection-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.status = "";
         this.soilForm.patchValue({
          dateSampleCollection : date,
          dateSampleTested : ""
         });
      
         $('#test-date').datepicker({
          minDate: this.soilForm.controls['dateSampleCollection'].value,
          changeMonth: true, 
          changeYear: true, 
          dateFormat: 'yy-mm-dd',
          yearRange: "-90:+00",
          onSelect: (date) => {
             this.soilForm.patchValue({
              dateSampleTested : date
             });
          }
        }).keyup((e2) => {
            if (e2.keyCode == 8 || e2.keyCode == 46) {
                $.datepicker._clearDate(this);
            }
        });
      }
    }).keyup((e3) => {
        if (e3.keyCode == 8 || e3.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Farmer",
      enableSearchFilter: true,
      classes:"form-control"
    };  

    this.getVillages();

    this.soilForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.soilService.getSoilTesting(this.baseUrl.getSoilTestingById + params.id).subscribe((res)=>{
            this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });
  }

  setFormData(val){
    this.soilData = val['data'];
    this.id = this.soilData['_id'];
    this.soilForm.setValue({ 
      activityDate: this.soilData['activityDate'] ? moment(this.soilData['activityDate']).format('YYYY-MM-DD') : "",
      activityId: this.activityId,
      villageId: this.soilData['villageData'] ? this.soilData['villageData']._id : this.soilData['villageId'],
      beneficiaryId: this.soilData['beneficiaryData']? [{ "id" : this.soilData['beneficiaryData']._id, "itemName" : this.soilData['beneficiaryData'].code + '-' + this.soilData['beneficiaryData'].name + '-' + this.soilData['beneficiaryData'].fatherName}] :this.soilData['beneficiaryId'],
      soilType: this.soilData['soilType'],
      ecValue: this.soilData['ecValue'],
      nValue: this.soilData['nValue'],
      pValue: this.soilData['pValue'],
      kValue: this.soilData['kValue'],
      znValue: this.soilData['znValue'],
      feValue: this.soilData['feValue'],
      sValue: this.soilData['sValue'],
      orgcValue: this.soilData['orgcValue'],
      bValue: this.soilData['bValue'],
      caValue: this.soilData['caValue'],
      mnValue: this.soilData['mnValue'],
      phValue: this.soilData['phValue'],
      farmerAware: this.soilData['farmerAware'],
      dateSampleTested: this.soilData['dateSampleTested'] ? moment(this.soilData['dateSampleTested']).format('YYYY-MM-DD') : "",
      dateSampleCollection: this.soilData['dateSampleCollection'] ? moment(this.soilData['dateSampleCollection']).format('YYYY-MM-DD') : ""
    });
    this.status = "disable";
    this.getBeneficiary('update');
  }

  createFormGroup(){
    return new FormGroup({
      activityDate: new FormControl('',Validators.required),
      activityId: new FormControl(this.activityId,Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      soilType: new FormControl(''),
      ecValue: new FormControl(''),
      nValue: new FormControl(''),
      pValue: new FormControl(''),
      kValue: new FormControl(''),
      znValue: new FormControl(''),
      feValue: new FormControl(''),
      sValue: new FormControl(''),
      orgcValue: new FormControl(''),
      bValue: new FormControl(''),
      caValue: new FormControl(''),
      mnValue: new FormControl(''),
      phValue: new FormControl(''),
      farmerAware: new FormControl(''),
      dateSampleTested: new FormControl(''),
      dateSampleCollection: new FormControl('', Validators.required)
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  getBeneficiary(via = ''){
    let id = this.soilForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.soilForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
      this.beneficiaries = res;
      let dropData = res['data'];
      let data = dropData.map((data)=>{
        return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
      });

      if(data.length){
        this.dropdownList = data;
      }else{
        this.dropdownList = [];
        this.soilForm.patchValue({
          beneficiaryId : []
        });
      }

      this.flag = true;
      
        // this.dropdownList = res['data'];
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.soilForm.controls['beneficiaryId'].reset("");
    }
    this.soilForm.controls['villageId'].markAsTouched();
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.soilForm && this.soilForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.soilForm && this.soilForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  redirectToBeneficiary(village_id){
    this.soilForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : "activity-data-entry/soil-testing/form",
        data : JSON.stringify(this.soilForm.value)
      }
    })
  }

  // check(event){
  //   if(event.keyCode == 109){
  //     return event.preventDefault();
  //   } 
  // }

  onSubmit(val) {
    this.soilForm.value['beneficiaryId'] = this.soilForm.value['beneficiaryId'][0].id;
    this.soilForm.setErrors({invalid : true});
    if(this.id){ 
      this.soilService.putSoilTesting(this.baseUrl.putSoilTesting + this.id, this.soilForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/soil-testing/index']);
        }
        this.soilForm.setErrors(null);
      },(error) => {
        this.soilForm.setErrors(null);
        console.log(error);
      });
    }else {
      this.soilService.postSoilTesting(this.baseUrl.postSoilTesting, this.soilForm.value ).subscribe( response=> { 
        if(response['success']){ 
          this.router.navigate(['activity-data-entry/soil-testing/index']);
        }
        this.soilForm.setErrors(null);
      },(error) => {
        this.soilForm.setErrors(null);
        console.log(error);
      });
    }

  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {
    this.dropdownList = [];
    this.soilForm.setValue({
      activityDate: "",
      activityId : this.activityId,
      villageId: "",
      beneficiaryId: [],
      soilType: "",
      ecValue: "",
      nValue: "",
      pValue: "",
      kValue: "",
      znValue: "",
      feValue: "",
      sValue: "",
      orgcValue: "",
      bValue: "",
      caValue: "",
      mnValue: "",
      phValue: "",
      farmerAware: "",
      dateSampleTested: "",
      dateSampleCollection: ""
    });
  }
}
