import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SoilIndexComponent } from './soil-index.component';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const IndexRoutes : Routes = [
  {
    path : '',
    component : SoilIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(IndexRoutes)
  ],
  declarations: [SoilIndexComponent]
})
export class SoilIndexModule { }
