import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SoilTestingService } from '../../../../services/activity/soil-testing/soil-testing.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../guard/auth.guard';

@Component({
  selector: 'app-soil-index',
  templateUrl: './soil-index.component.html',
  styleUrls: ['./soil-index.component.css']
})
export class SoilIndexComponent implements OnInit {
  soil : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private soilService: SoilTestingService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get soil data ..
    var self=this;
    this.soilService.getSoilTesting(this.baseUrl.getSoilTesting).subscribe( res=> { 
      this.soil  = res;
      if(this.soil.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'soil-testing', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteSoilTesting + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/soil-testing/form'], {queryParams: {id: data._id}});
    }else {
      this.router.navigate(['activity-data-entry/soil-testing/view'], {queryParams: {id: data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Soil-Testing", 
            footer: false,
            exportOptions: {
                columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]
            }
          }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#soilTable').DataTable(this.dtoptions);
    this.table.columns( [ 1, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'soil-testing', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
