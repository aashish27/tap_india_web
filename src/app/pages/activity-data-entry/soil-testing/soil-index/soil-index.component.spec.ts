import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilIndexComponent } from './soil-index.component';

describe('SoilIndexComponent', () => {
  let component: SoilIndexComponent;
  let fixture: ComponentFixture<SoilIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
