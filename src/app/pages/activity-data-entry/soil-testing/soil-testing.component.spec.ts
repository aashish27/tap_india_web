import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoilTestingComponent } from './soil-testing.component';

describe('SoilTestingComponent', () => {
  let component: SoilTestingComponent;
  let fixture: ComponentFixture<SoilTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoilTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoilTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
