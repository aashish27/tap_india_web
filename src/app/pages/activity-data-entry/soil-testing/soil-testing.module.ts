import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SoilTestingComponent } from './soil-testing.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';

export const SoilRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'form',
        loadChildren: './soil-form/soil-form.module#SoilFormModule',
        data: {component: 'soil-testing', action: 'create'},
        canActivate: [AuthGuard]
      },{
        path: 'index',
        loadChildren: './soil-index/soil-index.module#SoilIndexModule',
        data: {component: 'soil-testing', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './soil-view/soil-view.module#SoilViewModule',
        data: {component: 'soil-testing', action: 'view'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SoilRoutes)
  ],
  declarations: [SoilTestingComponent]
})
export class SoilTestingModule { }
