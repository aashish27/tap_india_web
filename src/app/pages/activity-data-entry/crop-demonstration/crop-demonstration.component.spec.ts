import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CropDemonstrationComponent } from './crop-demonstration.component';

describe('CropDemonstrationComponent', () => {
  let component: CropDemonstrationComponent;
  let fixture: ComponentFixture<CropDemonstrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropDemonstrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CropDemonstrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
