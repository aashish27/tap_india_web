import { CropDemonstrationModule } from './crop-demonstration.module';

describe('CropDemonstrationModule', () => {
  let cropDemonstrationModule: CropDemonstrationModule;

  beforeEach(() => {
    cropDemonstrationModule = new CropDemonstrationModule();
  });

  it('should create an instance', () => {
    expect(cropDemonstrationModule).toBeTruthy();
  });
});
