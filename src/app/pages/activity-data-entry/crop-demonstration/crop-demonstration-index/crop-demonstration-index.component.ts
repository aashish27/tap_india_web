
import { Component, OnInit } from '@angular/core';
import { CropDemonstrationService } from '../../../../services/activity/crop-demonstration/crop-demonstration.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-crop-index',
  templateUrl: './crop-demonstration-index.component.html',
  styleUrls: ['./crop-demonstration-index.component.css']
})
export class cropDemonstrationIndexComponent implements OnInit {
  cropDemonstration : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private cropDemonstrationService: CropDemonstrationService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.cropDemonstrationService.getCropDemonstration(this.baseUrl.getCropDemonstration).subscribe( res=> { 
      this.cropDemonstration  = res;
      if(this.cropDemonstration.success){
        setTimeout(function(){
          self.inittable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'demonstration-of-cop-specific-pop', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCropDemonstration + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/crop-demonstration/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['activity-data-entry/crop-demonstration/view'], {queryParams: {id : data._id}});
    }
  }

  inittable(){ // Data table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Demonstration-of-crop-specific-pop",
            footer: false,
            exportOptions: {
                columns: [1,2,3,4,5,6,7,8,9,10,11,12,13]
            }
          }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#cropDemonstration').DataTable(this.dtoptions);
    this.table.columns( [ 1, 2, 7, 8 ] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'demonstration-of-cop-specific-pop', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}

