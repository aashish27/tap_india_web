import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CropDemonstrationIndexComponent } from './crop-index.component';

describe('CropDemonstrationIndexComponent', () => {
  let component: CropDemonstrationIndexComponent;
  let fixture: ComponentFixture<CropDemonstrationIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropDemonstrationIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CropDemonstrationIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
