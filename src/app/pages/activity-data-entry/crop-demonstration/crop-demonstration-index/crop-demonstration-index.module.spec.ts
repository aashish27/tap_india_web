import { CropDemonstrationIndexModule } from './crop-index.module';

describe('CropDemonstrationIndexModule', () => {
  let cropIndexModule: CropDemonstrationIndexModule;

  beforeEach(() => {
    cropIndexModule = new CropDemonstrationIndexModule();
  });

  it('should create an instance', () => {
    expect(cropIndexModule).toBeTruthy();
  });
});
