import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { cropDemonstrationIndexComponent } from './crop-demonstration-index.component';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';


export const IndexRoutes : Routes = [
  {
    path : '',
    component : cropDemonstrationIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(IndexRoutes),
    PopupModule
  ],
  declarations: [cropDemonstrationIndexComponent]
})
export class CropDemonstrationIndexModule { }
