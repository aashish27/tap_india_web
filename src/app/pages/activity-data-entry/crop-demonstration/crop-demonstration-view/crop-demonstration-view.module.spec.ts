import { CropDemonstrationViewModule } from './crop-demonstration-view.module';

describe('CropDemonstrationViewModule', () => {
  let cropViewModule: CropDemonstrationViewModule;

  beforeEach(() => {
    cropViewModule = new CropDemonstrationViewModule();
  });

  it('should create an instance', () => {
    expect(cropViewModule).toBeTruthy();
  });
});
