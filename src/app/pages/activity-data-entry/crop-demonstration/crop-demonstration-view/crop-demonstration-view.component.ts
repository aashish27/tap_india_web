import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { CropDemonstrationService } from '../../../../services/activity/crop-demonstration/crop-demonstration.service';

@Component({
  selector: 'app-crop-demonstration-view',
  templateUrl: './crop-demonstration-view.component.html',
  styleUrls: ['./crop-demonstration-view.component.css']
})
export class CropDemonstrationViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private cropService:CropDemonstrationService, private baseUrl: Apisendpoints, private authGuard: AuthGuard) { }
  cropDemonstrationData:any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.cropService.getCropDemonstration(this.baseUrl.getCropDemoById + params.id).subscribe((res)=>{
          this.cropDemonstrationData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/crop-demonstration/form'], {queryParams: { id: data._id}});
    }else {
      this.router.navigate(['activity-data-entry/crop-demonstration/index'], {queryParams: { id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'demonstration-of-cop-specific-pop', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCropDemonstration + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/crop-demonstration/index']);
    }
  }
}

