import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CropDemonstrationViewComponent } from './crop-view.component';

describe('CropDemonstrationViewComponent', () => {
  let component: CropDemonstrationViewComponent;
  let fixture: ComponentFixture<CropDemonstrationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropDemonstrationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CropDemonstrationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
