import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CropDemonstrationViewComponent } from './crop-demonstration-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : CropDemonstrationViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [CropDemonstrationViewComponent]
})
export class CropDemonstrationViewModule { }
