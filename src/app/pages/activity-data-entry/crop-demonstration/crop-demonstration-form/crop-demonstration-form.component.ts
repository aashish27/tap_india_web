import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { ActivityListService } from '../../../../services/masters/activity-list/activity-list.service';
import { VillageService } from '../../../../services/masters/village/village.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { CropDemonstrationService } from '../../../../services/activity/crop-demonstration/crop-demonstration.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { VarietyService } from '../../../../services/masters/variety/variety.service';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { AreaValidator } from '../../../../validators/area.validator';

declare const $:any;

@Component({
  selector: 'app-crop-form',
  templateUrl: './crop-demonstration-form.component.html',
  styleUrls: ['./crop-demonstration-form.component.css']
})
export class CropDemonstrationFormComponent implements OnInit {
  cropDemonstrationForm: FormGroup;
  submitted = false;
  cropDemonstrationData:any;
  id: any;
  showBeneficiary:boolean = false;
  villages:any = {};
  beneficiaries:any = {};
  crops:any = {};
  varities:any = {};
  percent:number = 0;

  activityId:any;
  order:any = 4;

   //Beneficiary list dropdown
   dropdownList:any = [];
   dropdownSettings = {};
 
   flag:boolean = true;

  constructor(private formBuilder: FormBuilder, 
    private activityService: ActivityListService, 
    private beneficiaryService: BeneficiaryService, 
    private varietyService: VarietyService,
    private cropService: CropService,
    private villageService: VillageService, 
    private cropDemonstrationService: CropDemonstrationService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute,
    private common: CommonFunctionService
    ) { 
      this.activityId = this.common.get(this.order);
    }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.cropDemonstrationForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });
    // this.onChanges();

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    };  

    this.getVillages();
    this.getCrops();

    this.cropDemonstrationForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.cropDemonstrationService.getCropDemonstration(this.baseUrl.getCropDemoById + params.id).subscribe((res)=>{
          this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });
    
  }
  
  setFormData(val){
    this.cropDemonstrationData = val['data'];
    this.id = this.cropDemonstrationData['_id'];
    this.cropDemonstrationForm.setValue({ 
      activityDate: this.cropDemonstrationData['activityDate'] ? moment(this.cropDemonstrationData['activityDate']).format('YYYY-MM-DD') : "",
      activityId: this.activityId,
      villageId: this.cropDemonstrationData['villageData']? this.cropDemonstrationData['villageData']._id : this.cropDemonstrationData['villageId'],
      beneficiaryId: this.cropDemonstrationData['beneficiaryData']? [{ "id" : this.cropDemonstrationData['beneficiaryData']._id, "itemName" : this.cropDemonstrationData['beneficiaryData'].code + '-' + this.cropDemonstrationData['beneficiaryData'].name + '-' + this.cropDemonstrationData['beneficiaryData'].fatherName}] :this.cropDemonstrationData['beneficiaryId'],
      cropId: this.cropDemonstrationData['cropData']? this.cropDemonstrationData['cropData']._id : this.cropDemonstrationData['cropId'],
      area: this.cropDemonstrationData['area'],
      varietyId: this.cropDemonstrationData['varietyData'] ? this.cropDemonstrationData['varietyData']._id : this.cropDemonstrationData['varietyId'],
      controlPlotyield: this.cropDemonstrationData['controlPlotyield'],
      demoPlotYield: this.cropDemonstrationData['demoPlotYield'],
      percentageIncreaseInYield: this.cropDemonstrationData['percentageIncreaseInYield'],
      demagedPlot: this.cropDemonstrationData['demagedPlot'] || ""
    });
    this.getBeneficiary('update');
    this.getVarietyByCrop(this.cropDemonstrationData['cropData'] ? this.cropDemonstrationData['cropData']._id : this.cropDemonstrationData['cropId']);
  }

  // onChanges(): void {
  //   this.cropDemonstrationForm.statusChanges.subscribe(val => {
  //         console.log(val);
          
  //   });
  // }

  createFormGroup(){
    return new FormGroup({
      activityId: new FormControl(this.activityId,Validators.required),
      activityDate: new FormControl('',Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      cropId: new FormControl('', Validators.required),
      varietyId: new FormControl('', Validators.required),
      area: new FormControl(''),
      controlPlotyield: new FormControl('',AreaValidator),
      demoPlotYield: new FormControl('',AreaValidator),
      percentageIncreaseInYield : new FormControl(''),
      demagedPlot: new FormControl('')
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.cropDemonstrationForm && this.cropDemonstrationForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.cropDemonstrationForm && this.cropDemonstrationForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.cropDemonstrationForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.cropDemonstrationForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.cropDemonstrationForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.cropDemonstrationForm.controls['beneficiaryId'].reset("");
    }
    this.cropDemonstrationForm.controls['villageId'].markAsTouched();
  }

  getCrops(){
    this.cropService.getCrops(this.baseUrl.getCrop).subscribe((res)=>{
      this.crops = res;
    }, (error)=>{
      console.log(error);
    });
  }

  getVarietyByCrop(value){
    this.varietyService.getVariety(this.baseUrl.getVariety + '?cropId=' + value ).subscribe((res)=>{
      this.varities = res;
    }, (error)=>{
      console.log(error);
    });
  }

  redirectToBeneficiary(village_id){
    this.cropDemonstrationForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : 'activity-data-entry/crop-demonstration/form',
        data : JSON.stringify(this.cropDemonstrationForm.value)
      }
    })
  }
  
  onChanges(): void {
    let cp = parseFloat(this.cropDemonstrationForm.controls['controlPlotyield'].value) || 0;
    let dp = parseFloat(this.cropDemonstrationForm.controls['demoPlotYield'].value) || 0;

    let result = ((dp - cp)/cp * 100).toFixed(2) || 0;
    
    this.cropDemonstrationForm.patchValue({
      percentageIncreaseInYield :  isFinite(+result) ? result : 0
    });
  }

  onSubmit(val) {
    this.cropDemonstrationForm.value['beneficiaryId'] = this.cropDemonstrationForm.value['beneficiaryId'][0].id;
    this.cropDemonstrationForm.setErrors({invalid : true});   
    if(this.id){
      this.cropDemonstrationService.putCropDemonstration(this.baseUrl.putCropDemonstration + this.id, this.cropDemonstrationForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/crop-demonstration/index']);
        }
        this.cropDemonstrationForm.setErrors(null);  
      },(error) => {
        this.cropDemonstrationForm.setErrors(null);  
        console.log(error);
      });
    }else {
      this.cropDemonstrationService.postCropDemonstration(this.baseUrl.postCropDemonstration, this.cropDemonstrationForm.value ).subscribe( response=> { 
        if(response['success']){   
          this.router.navigate(['activity-data-entry/crop-demonstration/index']);
        }
        this.cropDemonstrationForm.setErrors(null);  
      },(error) => {
        this.cropDemonstrationForm.setErrors(null);  
        console.log(error);
      });
    }

  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  checkInt(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {
    this.dropdownList = [];
    this.cropDemonstrationForm.patchValue({
      activityDate: "",
      villageId: "",
      beneficiaryId: [],
      cropId: "",
      varietyId: "",
      area: "",
      controlPlotyield:"",
      demoPlotYield:"",
      percentageIncreaseInYield : "",
      demagedPlot: ""
    });
  }
}

