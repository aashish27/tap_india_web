import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CropDemonstrationFormComponent } from './crop-form.component';

describe('CropDemonstrationFormComponent', () => {
  let component: CropDemonstrationFormComponent;
  let fixture: ComponentFixture<CropDemonstrationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropDemonstrationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CropDemonstrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
