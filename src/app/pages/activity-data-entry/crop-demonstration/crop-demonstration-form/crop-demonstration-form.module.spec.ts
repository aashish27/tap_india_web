import { CropDemonstrationFormModule } from './crop-demonstration-form.module';

describe('CropDemonstrationFormModule', () => {
  let cropFormModule: CropDemonstrationFormModule;

  beforeEach(() => {
    cropFormModule = new CropDemonstrationFormModule();
  });

  it('should create an instance', () => {
    expect(cropFormModule).toBeTruthy();
  });
});
