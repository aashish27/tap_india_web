import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CropDemonstrationComponent } from './crop-demonstration.component';
import { AuthGuard } from '../../guard/auth.guard';

export const CropRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'form',
        loadChildren: './crop-demonstration-form/crop-demonstration-form.module#CropDemonstrationFormModule',
        data: {component: 'crop-demonstration', action: 'create'},
        canActivate: [AuthGuard]
      },{
        path: 'index',
        loadChildren: './crop-demonstration-index/crop-demonstration-index.module#CropDemonstrationIndexModule',
        data: {component: 'crop-demonstration', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './crop-demonstration-view/crop-demonstration-view.module#CropDemonstrationViewModule',
        data: {component: 'crop-demonstration', action: 'view'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CropRoutes)
  ],
  declarations: [CropDemonstrationComponent]
})
export class CropDemonstrationModule { }
