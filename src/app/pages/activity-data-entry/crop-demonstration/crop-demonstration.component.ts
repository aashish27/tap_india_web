import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crop-demonstration',
  template : '<router-outlet></router-outlet>'
})
export class CropDemonstrationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
