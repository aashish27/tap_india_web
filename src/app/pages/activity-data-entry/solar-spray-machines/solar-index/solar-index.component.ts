import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SolarSprayService } from '../../../../services/activity/solar-spray/solar-spray.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-solar-index',
  templateUrl: './solar-index.component.html',
  styleUrls: ['./solar-index.component.css']
})
export class SolarIndexComponent implements OnInit {
  solar : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private solarService: SolarSprayService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get solar data ..
    var self=this;
    this.solarService.getSolarSpray(this.baseUrl.getSolarSpray).subscribe( res=> { 
      this.solar  = res;
      if(this.solar.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'solar-spray-machines', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteSolarSpray + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/solar-spray-machines/form'], {queryParams: {id: data._id}});
    }else {
      this.router.navigate(['activity-data-entry/solar-spray-machines/view'], {queryParams: {id: data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv',
            text: 'Download CSV', 
            filename: "Solar Spray Machines",
            footer: false,
            exportOptions: {
              columns: [1,2,3,4,5,6,7,8,9,10,11,12]
             }
          }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#solarTable').DataTable(this.dtoptions);
    this.table.columns( [ 1, 2, 8, 9, 10] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw

    $('.dataTables_filter input').addClass('form-control form-control-sm m-0'); // adding bootstrap classes to search input
    $('select[name=solarTable_length]').addClass('form-control form-control-sm w-auto d-inline'); // adding bootstrap classes to lengthMenu select
    
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'solar-spray-machines', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
