import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolarIndexComponent } from './solar-index.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const FormRoutes : Routes = [
  {
    path : '',
    component : SolarIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(FormRoutes)
  ],
  declarations: [SolarIndexComponent],
})
export class SolarIndexModule { }
