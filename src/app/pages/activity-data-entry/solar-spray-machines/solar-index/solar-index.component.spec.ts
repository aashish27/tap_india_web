import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarIndexComponent } from './solar-index.component';

describe('SolarIndexComponent', () => {
  let component: SolarIndexComponent;
  let fixture: ComponentFixture<SolarIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
