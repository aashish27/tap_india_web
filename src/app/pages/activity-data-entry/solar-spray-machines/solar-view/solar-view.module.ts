import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolarViewComponent } from './solar-view.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const FormRoutes : Routes = [
  {
    path : '',
    component : SolarViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(FormRoutes)
  ],
  declarations: [SolarViewComponent]
})
export class SolarViewModule { }
