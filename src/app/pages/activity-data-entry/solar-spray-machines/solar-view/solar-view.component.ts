import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { SolarSprayService } from '../../../../services/activity/solar-spray/solar-spray.service';

@Component({
  selector: 'app-solar-view',
  templateUrl: './solar-view.component.html',
  styleUrls: ['./solar-view.component.css']
})
export class SolarViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private solarService:SolarSprayService, private authGuard: AuthGuard, private baseUrl: Apisendpoints) { }
  solarData:any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.solarService.getSolarSpray(this.baseUrl.getSolarSprayById + params.id).subscribe((res)=>{
          this.solarData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/solar-spray-machines/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'solar-spray-machines', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteSolarSpray + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/solar-spray-machines/index']);
    }
  }
}
