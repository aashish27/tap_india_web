import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarViewComponent } from './solar-view.component';

describe('SolarViewComponent', () => {
  let component: SolarViewComponent;
  let fixture: ComponentFixture<SolarViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
