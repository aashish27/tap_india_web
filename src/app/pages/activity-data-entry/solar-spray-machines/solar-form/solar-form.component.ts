import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { SolarSprayService } from '../../../../services/activity/solar-spray/solar-spray.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { MachineryOwnerService } from '../../../../services/masters/machineryOwner/machinery-owner.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';

declare const $:any;

@Component({
  selector: 'app-solar-form',
  templateUrl: './solar-form.component.html',
  styleUrls: ['./solar-form.component.css']
})
export class SolarFormComponent implements OnInit {
  solarForm: FormGroup;
  submitted = false;
  solarData:any;
  id: any;
  machineries:any = [];
  showBeneficiary:boolean = false;

  villages:any = {};
  beneficiaries:any = {};
  activities: any = {};
  crops: any = {};
  activityId:any;
  order:any = 7;

   //beneficiary list dropdown
   dropdownList:any = [];
   dropdownSettings = {};
 
   flag:boolean = true;

  constructor(
    private beneficiaryService: BeneficiaryService, 
    private cropService: CropService, 
    private villageService: VillageService, 
    private solarService: SolarSprayService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private common: CommonFunctionService,
    private route: ActivatedRoute,
    private machineryOwnerService: MachineryOwnerService,
  ) {
    this.activityId = this.common.get(this.order);
  }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.solarForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    };  

    this.getVillages();
    this.getCrops();
    this.getMachinery();

    this.solarForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.solarService.getSolarSpray(this.baseUrl.getSolarSprayById + params.id).subscribe((res)=>{
          this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });

  }

  setFormData(val){
    this.solarData = val['data'];
    this.id = this.solarData['_id'];
    this.solarForm.setValue({ 
      activityDate: this.solarData['activityDate'] ? moment(this.solarData['activityDate']).format('YYYY-MM-DD') : "",
      machineryOwnerId : this.solarData['machineryOwnerData']?this.solarData['machineryOwnerData']._id : this.solarData['machineryOwnerId'],
      // activityId: this.solarData['activityData']._id,
      activityId: this.activityId,
      villageId: this.solarData['villageData']?this.solarData['villageData']._id : this.solarData['villageId'],
      beneficiaryId: this.solarData['beneficiaryData']? [{ "id" : this.solarData['beneficiaryData']._id, "itemName" : this.solarData['beneficiaryData'].code + '-' + this.solarData['beneficiaryData'].name + '-' + this.solarData['beneficiaryData'].fatherName}] :this.solarData['beneficiaryId'],
      cropId: this.solarData['cropData']?this.solarData['cropData']._id : this.solarData['cropId'],
      area: this.solarData['area'],
      timeTakenUsingManualOperatedMachine: this.solarData['timeTakenUsingManualOperatedMachine'],
      timeTakenUsingSolarSprayMachine: this.solarData['timeTakenUsingSolarSprayMachine'],
      timeSaved: this.solarData['timeSaved']
    });
    this.getBeneficiary('update');
  }

  createFormGroup(){
    return new FormGroup({
      machineryOwnerId : new FormControl('',Validators.required),
      activityDate: new FormControl('',Validators.required),
      activityId: new FormControl(this.activityId, Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      cropId: new FormControl(''),
      area: new FormControl(''),
      timeTakenUsingManualOperatedMachine: new FormControl(''),
      timeTakenUsingSolarSprayMachine: new FormControl(''),
      timeSaved: new FormControl('')
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  getCrops(){
    this.cropService.getCrops(this.baseUrl.getCrop).subscribe((res)=>{
      this.crops = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.solarForm && this.solarForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.solarForm && this.solarForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  onChanges(): void {
    if(this.solarForm.controls['timeTakenUsingManualOperatedMachine'].value == ''){
      this.solarForm.patchValue({
        timeTakenUsingSolarSprayMachine : ''
      })
    }
    if(this.solarForm.controls['timeTakenUsingSolarSprayMachine'].value != ''){
      if(parseInt(this.solarForm.controls['timeTakenUsingManualOperatedMachine'].value) >= parseInt(this.solarForm.controls['timeTakenUsingSolarSprayMachine'].value ) ){
        if(this.solarForm.controls['timeTakenUsingSolarSprayMachine'].hasError('greaterError')){
          this.solarForm.controls['timeTakenUsingSolarSprayMachine'].setErrors(null);
        }
      }else{
        this.solarForm.controls['timeTakenUsingSolarSprayMachine'].setErrors({greaterError: true});
      }
    }

    this.solarForm.patchValue({
      timeSaved : (parseFloat(this.solarForm.controls['timeTakenUsingManualOperatedMachine'].value || 0) - parseFloat(this.solarForm.controls['timeTakenUsingSolarSprayMachine'].value || 0)).toFixed(2) || 0
    });
  }

  getBeneficiary(via = ''){
    let id = this.solarForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.solarForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.solarForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.solarForm.controls['beneficiaryId'].reset("");
    }
    this.solarForm.controls['villageId'].markAsTouched();
  }

  getMachinery(){
    this.machineryOwnerService.getMachineryOwner(this.baseUrl.getMachineryOwner + '?activityId='+ this.activityId).subscribe( res=> { 
      this.machineries = res;
    }, (error)=>{
      console.log(error);
    });
  }

  redirectToBeneficiary(village_id){
    this.solarForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : 'activity-data-entry/solar-spray-machines/form',
        data : JSON.stringify(this.solarForm.value)
      }
    })
  }

  onSubmit(val) {
    this.solarForm.value['beneficiaryId'] = this.solarForm.value['beneficiaryId'][0].id;
    this.solarForm.setErrors({invalid : true}); 
    if(this.id){ 
      this.solarService.putSolarSpray(this.baseUrl.putSolarSpray + this.id, this.solarForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/solar-spray-machines/index']);
        }
        this.solarForm.setErrors(null); 
      },(error) => {
        this.solarForm.setErrors(null); 
        console.log(error);
      });
    }else {  
      this.solarService.postSolarSpray(this.baseUrl.postSolarSpray, this.solarForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['activity-data-entry/solar-spray-machines/index']);
        }
        this.solarForm.setErrors(null);
      },(error) => {
        this.solarForm.setErrors(null); 
        console.log(error);
      });
    }
  }

  checkInt(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }


  revert() {
    this.dropdownList = [];
    this.solarForm.patchValue({
      machineryOwnerId : "",
      activityDate: "",
      villageId: "",
      beneficiaryId: [],
      cropId: "",
      area: "",
      timeTakenUsingManualOperatedMachine: "",
      timeTakenUsingSolarSprayMachine: "",
      timeSaved: ""
    });
  }
}
