import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarFormComponent } from './solar-form.component';

describe('SolarFormComponent', () => {
  let component: SolarFormComponent;
  let fixture: ComponentFixture<SolarFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
