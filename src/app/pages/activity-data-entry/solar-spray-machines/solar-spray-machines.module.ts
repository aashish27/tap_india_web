import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SolarSprayMachinesComponent } from './solar-spray-machines.component';
import { AuthGuard } from '../../guard/auth.guard';
export const SolarRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './solar-index/solar-index.module#SolarIndexModule',
        data: {component: 'solar-spray-machines', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './solar-view/solar-view.module#SolarViewModule',
        data: {component: 'solar-spray-machines', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './solar-form/solar-form.module#SolarFormModule',
        data: {component: 'solar-spray-machines', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SolarRoutes)
  ],
  declarations: [SolarSprayMachinesComponent]
})
export class SolarSprayMachinesModule { }
