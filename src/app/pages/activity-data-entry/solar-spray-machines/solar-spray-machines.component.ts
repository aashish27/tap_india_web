import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-solar-spray-machines',
  templateUrl: './solar-spray-machines.component.html',
  animations: [
    topToBottom
  ]
})
export class SolarSprayMachinesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
