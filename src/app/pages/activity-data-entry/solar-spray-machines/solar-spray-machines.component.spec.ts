import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolarSprayMachinesComponent } from './solar-spray-machines.component';

describe('SolarSprayMachinesComponent', () => {
  let component: SolarSprayMachinesComponent;
  let fixture: ComponentFixture<SolarSprayMachinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolarSprayMachinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolarSprayMachinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
