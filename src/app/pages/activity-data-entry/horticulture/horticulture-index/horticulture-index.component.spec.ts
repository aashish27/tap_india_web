import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorticultureIndexComponent } from './horticulture-index.component';

describe('HorticultureIndexComponent', () => {
  let component: HorticultureIndexComponent;
  let fixture: ComponentFixture<HorticultureIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorticultureIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorticultureIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
