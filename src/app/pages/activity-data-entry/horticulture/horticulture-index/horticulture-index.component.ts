import { Component, OnInit } from '@angular/core';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../guard/auth.guard';
import { HorticultureService } from '../../../../services/activity/horticulture/horticulture.service';


@Component({
  selector: 'app-horticulture-index',
  templateUrl: './horticulture-index.component.html',
  styleUrls: ['./horticulture-index.component.css']
})
export class HorticultureIndexComponent implements OnInit {
  horticulture : any = {};
  tableHideShowStatus: boolean = false;
  dtOption: any = {};
  table:any;
  delete_id:any = "";
  
  constructor(private horticultureService: HorticultureService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.horticultureService.getHorticulture(this.baseUrl.getHorticulture).subscribe( res=> { 
      this.horticulture  = res;
      if(this.horticulture.success){
        setTimeout(function(){
          self.inittable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'horticulture', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteHorticulture + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/horticulture/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['activity-data-entry/horticulture/view'], {queryParams: {id: data._id}});
    }
  }

  inittable(){ // Data table Initialization after record fetched ..
    this.dtOption = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Horticulture",
            footer : false,
            exportOptions: {
              columns: [1,2,3,4,5,6,7,8,9,10]
          }
        }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#hortiTable').DataTable(this.dtOption);
    this.table.columns( [ 1, 2, 6, 8] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'horticulture', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}


