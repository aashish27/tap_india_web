import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HorticultureIndexComponent } from './horticulture-index.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const IndexRoutes : Routes = [
  {
    path : '',
    component : HorticultureIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(IndexRoutes)
  ],
  declarations:[HorticultureIndexComponent]
})  
export class HorticultureIndexModule { }
