import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorticultureFormComponent } from './horticulture-form.component';

describe('HorticultureFormComponent', () => {
  let component: HorticultureFormComponent;
  let fixture: ComponentFixture<HorticultureFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorticultureFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorticultureFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
