import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HorticultureFormComponent } from './horticulture-form.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/multiselect.component';

export const FormRoutes : Routes = [
  {
    path : '',
    component : HorticultureFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    AngularMultiSelectModule,
    RouterModule.forChild(FormRoutes)
  ],
  declarations:[HorticultureFormComponent]
})  
export class HorticultureFormModule { }
