import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { AreaValidator } from '../../../../validators/area.validator';
import * as moment from 'moment';
import { HorticultureService } from '../../../../services/activity/horticulture/horticulture.service';

declare const $:any;

@Component({
  selector: 'app-horticulture-form',
  templateUrl: './horticulture-form.component.html',
  styleUrls: ['./horticulture-form.component.css']
})
export class HorticultureFormComponent implements OnInit {
  hortiForm: FormGroup;
  submitted = false;
  hortiData:any;
  id: any;
  showBeneficiary:boolean = false;
  villages:any = {};
  beneficiaries:any = {};
  crops: any = {};

  activityId:any;
  order:any = 5;

   //beneficiary list dropdown
   dropdownList:any = [];
   dropdownSettings = {};
 
   flag:boolean = true;

  constructor(
    private beneficiaryService: BeneficiaryService, 
    private villageService: VillageService, 
    private hortiService: HorticultureService,
    private cropService:CropService,
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute,
    private common: CommonFunctionService
    ) {
      this.activityId = this.common.get(this.order);
     }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.hortiForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });
    // this.onChanges();

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    }; 

    this.getVillages();
    this.getCrops();

    this.hortiForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.hortiService.getHorticulture(this.baseUrl.getHorticultureById + params.id).subscribe((res)=>{
          this.setFormData(res);
        });
      }
      else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      } 
    });
    
  }
  
  setFormData(val){
    this.hortiData = val['data'];
    this.id = this.hortiData['_id'];
    this.hortiForm.setValue({ 
      activityDate: this.hortiData['activityDate'] ? moment(this.hortiData['activityDate']).format('YYYY-MM-DD') : "",
      activityId : this.activityId,
      villageId: this.hortiData['villageData']?this.hortiData['villageData']._id : this.hortiData['villageId'],
      beneficiaryId: this.hortiData['beneficiaryData']? [{ "id" : this.hortiData['beneficiaryData']._id, "itemName" : this.hortiData['beneficiaryData'].code + '-' + this.hortiData['beneficiaryData'].name + '-' + this.hortiData['beneficiaryData'].fatherName}] :this.hortiData['beneficiaryId'],
      area: this.hortiData['area'],
      cropId: this.hortiData['cropData']?this.hortiData['cropData']._id : this.hortiData['cropId'],
      irrigationMethod: this.hortiData['irrigationMethod'],
      numOfPlant: this.hortiData['numOfPlant']
    });
    this.getBeneficiary("param");
  }

  createFormGroup(){
    return new FormGroup({
      activityId: new FormControl(this.activityId,Validators.required),
      activityDate: new FormControl('',Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      cropId: new FormControl('',Validators.required),
      irrigationMethod: new FormControl(''),
      area: new FormControl('',[AreaValidator]),
      numOfPlant: new FormControl('')
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  getCrops(){
    this.cropService.getCrops(this.baseUrl.getCrop).subscribe((res)=>{
      this.crops = res;
    }, (error)=>{
      console.log(error);
    });
  }
  
  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.hortiForm && this.hortiForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.hortiForm && this.hortiForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.hortiForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.hortiForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.hortiForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.hortiForm.controls['beneficiaryId'].reset("");
    }
    this.hortiForm.controls['villageId'].markAsTouched();
  }
  
  redirectToBeneficiary(village_id){
    this.hortiForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : 'activity-data-entry/horticulture/form',
        data : JSON.stringify(this.hortiForm.value)
      }
    })
  }
  
  onSubmit(val) {
    this.hortiForm.value['beneficiaryId'] = this.hortiForm.value['beneficiaryId'][0].id;
    this.hortiForm.setErrors({invalid : true});  
    if(this.id){   
      this.hortiService.putHorticulture(this.baseUrl.putHorticulture + this.id, this.hortiForm.value ).subscribe( res=> { 
        if(res['success']){  
          this.router.navigate(['activity-data-entry/horticulture/index']);
        }
        this.hortiForm.setErrors(null); 
      },(error) => {
        this.hortiForm.setErrors(null); 
        console.log(error);
      });
    }else {  
      this.hortiService.postHorticulture(this.baseUrl.postHorticulture, this.hortiForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['activity-data-entry/horticulture/index']);
        }
        this.hortiForm.setErrors(null); 
      },(error) => {
        this.hortiForm.setErrors(null); 
        console.log(error);
      });
    }

  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  checkInt(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {
    this.dropdownList = [];
    this.hortiForm.patchValue({
      activityId: "",
      activityDate: "",
      villageId: "",
      beneficiaryId: [],
      cropId: "",
      irrigationMethod: "",
      area: "",
      numOfPlant: ""
    });
  }
}


