import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-horticulture',
  template : '<router-outlet></router-outlet>'
})
export class HorticultureComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
