import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HorticultureComponent } from './horticulture.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';
export const CropRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './horticulture-index/horticulture-index.module#HorticultureIndexModule',
        data: {component: 'horticulture', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './horticulture-view/horticulture-view.module#HorticultureViewModule',
        data: {component: 'horticulture', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './horticulture-form/horticulture-form.module#HorticultureFormModule',
        data: {component: 'horticulture', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CropRoutes)
  ],
  declarations: [HorticultureComponent]
})
export class HorticultureModule { }
