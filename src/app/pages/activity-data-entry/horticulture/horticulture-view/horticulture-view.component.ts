import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../guard/auth.guard';
import { HorticultureService } from '../../../../services/activity/horticulture/horticulture.service';

@Component({
  selector: 'app-horticulture-view',
  templateUrl: './horticulture-view.component.html',
  styleUrls: ['./horticulture-view.component.css']
})
export class HorticultureViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private hortiService: HorticultureService, private router: Router, private baseUrl: Apisendpoints) { }
  hortiData:any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.hortiService.getHorticulture(this.baseUrl.getHorticultureById + params.id).subscribe((res)=>{
          this.hortiData = res['data'];
        });
      }
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/horticulture/form'], {queryParams: { id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'horticulture', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteHorticulture + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/horticulture/index']);
    }
  }
}
