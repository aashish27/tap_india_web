import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HorticultureViewComponent } from './horticulture-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const ViewRoutes : Routes = [
  {
    path : '',
    component : HorticultureViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations:[HorticultureViewComponent]
})  
export class HorticultureViewModule { }
