import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorticultureViewComponent } from './horticulture-view.component';

describe('HorticultureViewComponent', () => {
  let component: HorticultureViewComponent;
  let fixture: ComponentFixture<HorticultureViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorticultureViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorticultureViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
