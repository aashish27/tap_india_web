import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorticultureComponent } from './horticulture.component';

describe('HorticultureComponent', () => {
  let component: HorticultureComponent;
  let fixture: ComponentFixture<HorticultureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorticultureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorticultureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
