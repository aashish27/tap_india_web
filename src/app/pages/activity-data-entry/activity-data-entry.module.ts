import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ActivityDataEntryComponent } from './activity-data-entry.component';

export const ActivityRoutes: Routes = [
  {
    path : '',
    children : [
      
      { path: 'composting',   redirectTo: 'composit-unit', pathMatch: 'full' },
      { path: 'demonstration-of-cop-specific-pop',   redirectTo: 'crop-demonstration', pathMatch: 'full' },
      { path: 'promotion-of-solar-spray-pumps',   redirectTo: 'solar-spray-machines', pathMatch: 'full' },

      {
        path : 'soil-testing',
        loadChildren : './soil-testing/soil-testing.module#SoilTestingModule'
      },
      {
        path : 'composit-unit',
        loadChildren : './composit-unit/composit-unit.module#CompositUnitModule'
      },
      {
        path : 'crop-demonstration',
        loadChildren : './crop-demonstration/crop-demonstration.module#CropDemonstrationModule'
      },
      {
        path : 'horticulture',
        loadChildren : './horticulture/horticulture.module#HorticultureModule'
      },
      {
        path : 'farm-bunding',
        loadChildren : './farm-bunding/farm-bunding.module#FarmBundingModule'
      },
      {
        path : 'training-and-exposure-visit',
        loadChildren : './training-and-exposure-visit/training-and-exposure-visit.module#TrainingAndExposureVisitModule'
      },
      {
        path : 'solar-spray-machines',
        loadChildren : './solar-spray-machines/solar-spray-machines.module#SolarSprayMachinesModule'
      },
      {
        path : 'capacity-building',
        loadChildren : './capacity-building/capacity-building.module#CapacityBuildingModule'
      },
      {
        path : 'pond-rejuvenation',
        loadChildren : './pond-rejuvenation/pond-rejuvenation.module#PondRejuvenationModule'
      },
      {
        path : 'farm-pond',
        loadChildren : './farm-pond/farm-pond.module#FarmPondModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivityRoutes),
  ],
  declarations: [ActivityDataEntryComponent]
})
export class ActivityDataEntryModule { }
