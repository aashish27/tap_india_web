import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmPondComponent } from './farm-pond.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../guard/auth.guard';

export const PondRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './farm-pond-index/farm-pond-index.module#FarmPondIndexModule',
        data: {component: 'farm-pond-rejuvenation', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './farm-pond-view/farm-pond-view.module#FarmPondViewModule',
        data: {component: 'farm-pond-rejuvenation', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './farm-pond-form/farm-pond-form.module#FarmPondFormModule',
        data: {component: 'farm-pond-rejuvenation', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PondRoutes)
  ],
  declarations: [FarmPondComponent]
})
export class FarmPondModule { }
