import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farm-pond',
  template : '<router-outlet></router-outlet>'
})
export class FarmPondComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
