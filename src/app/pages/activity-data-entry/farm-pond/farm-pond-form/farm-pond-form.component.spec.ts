import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmPondFormComponent } from './farm-pond-form.component';

describe('FarmPondFormComponent', () => {
  let component: FarmPondFormComponent;
  let fixture: ComponentFixture<FarmPondFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmPondFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmPondFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
