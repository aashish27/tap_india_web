import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { CropService } from '../../../../services/masters/crop/crop.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { AreaValidator } from '../../../../validators/area.validator';
import { FarmPondService } from '../../../../services/activity/farm-pond/farm-pond.service';

declare const $:any;

@Component({
  selector: 'app-farm-pond-form',
  templateUrl: './farm-pond-form.component.html',
  styleUrls: ['./farm-pond-form.component.css']
})
export class FarmPondFormComponent implements OnInit {
  farmPondForm: FormGroup;
  submitted = false;
  farmPondData:any;
  id: any;
  showBeneficiary:boolean = false;
  villages:any = {};
  beneficiaries:any = {};
  crops:any = {};

  activityId:any;
  order:any = 9;

  //beneficiary list dropdown
  dropdownList:any = [];
  dropdownSettings = {};

  flag:boolean = true;

  constructor(
    private beneficiaryService: BeneficiaryService, 
    private cropService: CropService,
    private villageService: VillageService, 
    private farmPondService: FarmPondService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute,
    private common: CommonFunctionService
    ) { 
      this.activityId = this.common.get(this.order);
    }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.farmPondForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });
    // this.onChanges();

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    };  

    this.getVillages();
    this.getCrops();

    this.farmPondForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.farmPondService.getFarmPond(this.baseUrl.getFarmPondById + params.id).subscribe((res)=>{
          this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });

  }

  setFormData(val){
    this.farmPondData = val['data'];
    this.id = this.farmPondData['_id'];
    this.farmPondForm.setValue({ 
      activityDate: this.farmPondData['activityDate'] ? moment(this.farmPondData['activityDate']).format('YYYY-MM-DD') : "",
      activityId: this.activityId,
      villageId: this.farmPondData['villageData']? this.farmPondData['villageData']._id : this.farmPondData['villageId'],
      beneficiaryId: this.farmPondData['beneficiaryData']? [{ "id" : this.farmPondData['beneficiaryData']._id, "itemName" : this.farmPondData['beneficiaryData'].code + '-' + this.farmPondData['beneficiaryData'].name + '-' + this.farmPondData['beneficiaryData'].fatherName}] :this.farmPondData['beneficiaryId'],
      cropId: this.farmPondData['cropData']? this.farmPondData['cropData']._id : this.farmPondData['cropId'],
      areaUnderFarmPond: this.farmPondData['areaUnderFarmPond'],
      areaUnderFarmPondL: this.farmPondData['areaUnderFarmPondL'],
      areaUnderFarmPondW: this.farmPondData['areaUnderFarmPondW'],
      areaUnderFarmPondD: this.farmPondData['areaUnderFarmPondD'],
      WaterStorageCapacity: this.farmPondData['WaterStorageCapacity']
    });
    this.getBeneficiary('update');
  }

  // onChanges(): void {
  //   this.farmPondForm.statusChanges.subscribe(val => {
  //         console.log(val);
          
  //   });
  // }

  createFormGroup(){
    return new FormGroup({
      activityId: new FormControl(this.activityId,Validators.required),
      activityDate: new FormControl('',Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      cropId: new FormControl('',Validators.required),
      areaUnderFarmPond: new FormControl(''),
      areaUnderFarmPondW: new FormControl(''),
      areaUnderFarmPondL: new FormControl(''),
      areaUnderFarmPondD: new FormControl(''),
      WaterStorageCapacity: new FormControl('',AreaValidator)
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.farmPondForm && this.farmPondForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.farmPondForm && this.farmPondForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.farmPondForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.farmPondForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.farmPondForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.farmPondForm.controls['beneficiaryId'].reset("");
    }
    this.farmPondForm.controls['villageId'].markAsTouched();
  }

  getCrops(){
    this.cropService.getCrops(this.baseUrl.getCrop).subscribe((res)=>{
      this.crops = res;
    }, (error)=>{
      console.log(error);
    });
  }

  redirectToBeneficiary(village_id){
    this.farmPondForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : 'activity-data-entry/farm-pond/form',
        data : JSON.stringify(this.farmPondForm.value)
      }
    })
  }
  
  onChanges(): void {
    this.farmPondForm.patchValue({
      areaUnderFarmPond : ((parseFloat(this.farmPondForm.controls['areaUnderFarmPondW'].value || 0)) * (parseFloat(this.farmPondForm.controls['areaUnderFarmPondL'].value || 0)) * (parseFloat(this.farmPondForm.controls['areaUnderFarmPondD'].value || 0))).toFixed(2) || 0 ,
      WaterStorageCapacity : ((parseFloat(this.farmPondForm.controls['areaUnderFarmPondW'].value || 0)) * (parseFloat(this.farmPondForm.controls['areaUnderFarmPondL'].value || 0)) * (parseFloat(this.farmPondForm.controls['areaUnderFarmPondD'].value || 0))).toFixed(2) || 0
    });
  }

  onSubmit(val) {
    this.farmPondForm.value['beneficiaryId'] = this.farmPondForm.value['beneficiaryId'][0].id;
    this.farmPondForm.setErrors({invalid : true});   
    if(this.id){
      this.farmPondService.putFarmPond(this.baseUrl.putFarmPond + this.id, this.farmPondForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/farm-pond/index']);
        }
        this.farmPondForm.setErrors(null);  
      },(error) => {
        this.farmPondForm.setErrors(null);  
        console.log(error);
      });
    }else {
      this.farmPondService.postFarmPond(this.baseUrl.postFarmPond, this.farmPondForm.value ).subscribe( response=> { 
        if(response['success']){   
          this.router.navigate(['activity-data-entry/farm-pond/index']);
        }
        this.farmPondForm.setErrors(null);  
      },(error) => {
        this.farmPondForm.setErrors(null);  
        console.log(error);
      });
    }

  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  checkInt(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {
    this.dropdownList = [];
    this.farmPondForm.patchValue({
      activityDate: "",
      villageId: "",
      beneficiaryId: [],
      cropId: "",
      areaUnderFarmPond: "",
      areaUnderFarmPondW: "",
      areaUnderFarmPondL: "",
      areaUnderFarmPondD: "",
      WaterStorageCapacity:""
    });
  }
}

