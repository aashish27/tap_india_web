import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmPondComponent } from './farm-pond.component';

describe('FarmPondComponent', () => {
  let component: FarmPondComponent;
  let fixture: ComponentFixture<FarmPondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmPondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmPondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
