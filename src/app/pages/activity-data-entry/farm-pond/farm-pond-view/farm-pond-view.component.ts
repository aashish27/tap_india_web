import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../guard/auth.guard';
import { FarmPondService } from '../../../../services/activity/farm-pond/farm-pond.service';

@Component({
  selector: 'app-farm-pond-view',
  templateUrl: './farm-pond-view.component.html',
  styleUrls: ['./farm-pond-view.component.css']
})
export class FarmPondViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private pondService:FarmPondService, private baseUrl: Apisendpoints, private authGuard: AuthGuard) { }
  farmPond:any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.pondService.getFarmPond(this.baseUrl.getFarmPondById + params.id).subscribe((res)=>{
          this.farmPond = res['data'];
        })
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/farm-pond/form'], {queryParams: { id: data._id}});
    }else {
      this.router.navigate(['activity-data-entry/farm-pond/index'], {queryParams: { id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'farm-pond', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteFarmPond + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/farm-pond/index']);
    }
  }
}

