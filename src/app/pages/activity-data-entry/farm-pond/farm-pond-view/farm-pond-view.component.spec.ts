import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmPondViewComponent } from './farm-pond-view.component';

describe('FarmPondViewComponent', () => {
  let component: FarmPondViewComponent;
  let fixture: ComponentFixture<FarmPondViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmPondViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmPondViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
