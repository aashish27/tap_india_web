import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmPondIndexComponent } from './farm-pond-index.component';

describe('FarmPondIndexComponent', () => {
  let component: FarmPondIndexComponent;
  let fixture: ComponentFixture<FarmPondIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmPondIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmPondIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
