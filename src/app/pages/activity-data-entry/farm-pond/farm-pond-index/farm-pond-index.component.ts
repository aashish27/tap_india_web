
import { Component, OnInit } from '@angular/core';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../guard/auth.guard';
import { FarmPondService } from '../../../../services/activity/farm-pond/farm-pond.service';

@Component({
  selector: 'app-farm-pond-index',
  templateUrl: './farm-pond-index.component.html',
  styleUrls: ['./farm-pond-index.component.css']
})
export class FarmPondIndexComponent implements OnInit {
  farmPond : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private farmPondService: FarmPondService, private authGuard: AuthGuard,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.farmPondService.getFarmPond(this.baseUrl.getFarmPond).subscribe( res=> { 
      this.farmPond  = res;
      if(this.farmPond.success){
        setTimeout(function(){
          self.inittable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'farm-pond', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteFarmPond + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/farm-pond/form'], {queryParams: {id : data._id}});
    }else {
      this.router.navigate(['activity-data-entry/farm-pond/view'], {queryParams: {id : data._id}});
    }
  }

  inittable(){ // Data table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Farm-Pond",
            footer: false,
            exportOptions: {
                columns: [1,2,3,4,5,6,7,8,9,10,11,12]
            }}
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#farmPondTable').DataTable(this.dtoptions);
    this.table.columns( [ 1, 2, 7, 8, 9 ] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'farm-pond', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}

