import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FarmPondIndexComponent } from './farm-pond-index.component';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';


export const IndexRoutes : Routes = [
  {
    path : '',
    component : FarmPondIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(IndexRoutes),
    PopupModule
  ],
  declarations: [FarmPondIndexComponent]
})
export class FarmPondIndexModule { }
