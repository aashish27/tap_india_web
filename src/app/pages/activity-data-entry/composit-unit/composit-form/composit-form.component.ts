import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VillageService } from '../../../../services/masters/village/village.service';
import { CompostUnitService } from '../../../../services/activity/compost-unit/compost-unit.service';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
declare const $:any;

@Component({
  selector: 'app-composit-form',
  templateUrl: './composit-form.component.html',
  styleUrls: ['./composit-form.component.css']
})
export class CompositFormComponent implements OnInit {
  compositForm: FormGroup;
  compostData:any;
  id:any = "";
  submitted = false;
  showBeneficiary:boolean = false;

  villages:any = {};
  beneficiaries:any = {};

  activityId:any;
  order:any = 2;

  //beneficiary list dropdown
  dropdownList:any = [];
  dropdownSettings = {};

  flag:boolean = true;
  
  constructor(
    private villageService: VillageService,
    private compostService: CompostUnitService,
    private beneficiaryService: BeneficiaryService,
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute,
    private common: CommonFunctionService
  ) {
    this.activityId = this.common.get(this.order);
  }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.compositForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });
    // this.onChanges();

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    };  

    this.getVillages();

    this.compositForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.compostService.getCompostUnit(this.baseUrl.getCompostUnitById + params.id).subscribe((res)=>{
          this.setFormData(res);
        });
      }else if(params['data']){
        let val = {};
        val["data"] = JSON.parse(params['data']);
        this.setFormData(val);
      }
    });
  }

  setFormData(val){
    this.compostData = val['data'];
    this.id = this.compostData['_id'];
    this.compositForm.setValue({ 
      activityId : this.activityId,
      activityDate : this.compostData['activityDate'] ? moment(this.compostData['activityDate']).format('YYYY-MM-DD') : "",
      villageId : this.compostData['villageData']?this.compostData['villageData']._id : this.compostData['villageId'],   
      beneficiaryId: this.compostData['beneficiaryData']? [{ "id" : this.compostData['beneficiaryData']._id, "itemName" : this.compostData['beneficiaryData'].code + '-' + this.compostData['beneficiaryData'].name + '-' + this.compostData['beneficiaryData'].fatherName}] :this.compostData['beneficiaryId'],
      quantity : this.compostData['quantity'],   
      // farmerUsingCompostInField : this.compostData['farmerUsingCompostInField']
    });
    this.getBeneficiary("param");
  }

  createFormGroup(){
    return new FormGroup({
      activityDate: new FormControl('',Validators.required),
      activityId: new FormControl(this.activityId,Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl([],this.beneficiaryValidator),
      quantity: new FormControl(''),
      // farmerUsingCompostInField: new FormControl('',Validators.required)
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe( res=> { 
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.compositForm && this.compositForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.compositForm && this.compositForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.compositForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.compositForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.compositForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.compositForm.controls['beneficiaryId'].reset("");
    }
    this.compositForm.controls['villageId'].markAsTouched();
  }

  status(event:any){
    return event.target.value = "";
  }

  revert() {
    this.dropdownList = [];
    this.compositForm.patchValue({
      activityDate: "",
      villageId: "",
      beneficiaryId: [],
      quantity: "",
      // farmerUsingCompostInField: ""
    });
  }
  
  redirectToBeneficiary(village_id){
    this.compositForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : 'activity-data-entry/composit-unit/form',
        data : JSON.stringify(this.compositForm.value)
      }
    })
  }

  onSubmit(value) { // Submit Form ..
    this.compositForm.value['beneficiaryId'] = this.compositForm.value['beneficiaryId'][0].id;
    this.compositForm.setErrors({invalid : true});   
    if(this.id){
      this.compostService.putCompostUnit(this.baseUrl.putCompostUnit + this.id, this.compositForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/composit-unit/index']);
        }
        this.compositForm.setErrors(null);    
      },(error) => {
        this.compositForm.setErrors(null);    
        console.log(error);
      });
    }else {
      this.compostService.postCompostUnit(this.baseUrl.postCompostUnit, this.compositForm.value ).subscribe( (response)=> { 
        if(response['success']){  
          this.router.navigate(['activity-data-entry/composit-unit/index']);
        }
        this.compositForm.setErrors(null);    
      },(error) => {
        this.compositForm.setErrors(null);    
        console.log(error);
      });
    }
    // if (this.compositForm.invalid) {
    //     return;
    // }
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.compositForm.value))
  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }
}
