import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompositFormComponent } from './composit-form.component';

describe('CompositFormComponent', () => {
  let component: CompositFormComponent;
  let fixture: ComponentFixture<CompositFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompositFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
