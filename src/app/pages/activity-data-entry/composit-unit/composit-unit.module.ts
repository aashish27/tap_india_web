import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CompositUnitComponent } from './composit-unit.component';
import { AuthGuard } from '../../guard/auth.guard';

export const CompositRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'form',
        loadChildren: './composit-form/composit-form.module#CompositFormModule',
        data: {component: 'composit-unit', action: 'create'},
        canActivate: [AuthGuard]
      },{
        path: 'index',
        loadChildren: './composit-index/composit-index.module#CompositIndexModule',
        data: {component: 'composit-unit', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './composit-view/composit-view.module#CompositViewModule',
        data: {component: 'composit-unit', action: 'view'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CompositRoutes)
  ],
  declarations: [CompositUnitComponent]
})
export class CompositUnitModule { }
