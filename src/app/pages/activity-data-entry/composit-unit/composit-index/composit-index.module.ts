import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompositIndexComponent } from './composit-index.component';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const IndexRoutes : Routes = [
  {
    path : '',
    component : CompositIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(IndexRoutes)
  ],
  declarations: [CompositIndexComponent]
})
export class CompositIndexModule { }
