import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompositIndexComponent } from './composit-index.component';

describe('CompositIndexComponent', () => {
  let component: CompositIndexComponent;
  let fixture: ComponentFixture<CompositIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompositIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
