import { Component, OnInit } from '@angular/core';
import { CompostUnitService } from '../../../../services/activity/compost-unit/compost-unit.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-composit-index',
  templateUrl: './composit-index.component.html',
  styleUrls: ['./composit-index.component.css']
})
export class CompositIndexComponent implements OnInit {
  compost : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  constructor(private compostService: CompostUnitService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.compostService.getCompostUnit(this.baseUrl.getCompostUnit).subscribe( res=> { 
      this.compost  = res;
      if(this.compost.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'composit-unit', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCompostUnit + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/composit-unit/form'], {queryParams: { id: data._id}});
    }else {
      this.router.navigate(['activity-data-entry/composit-unit/view'], {queryParams: { id: data._id}});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csv', 
          text: 'Download CSV', 
          filename: "Compost-Unit", 
          footer: false,
          exportOptions: {
              columns: [1,2,3,4,5,6,7]
          }
        }
        //'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    }
    this.table = $('#compostTable').DataTable(this.dtoptions);
    this.table.columns( [1] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }

  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'composit-unit', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
