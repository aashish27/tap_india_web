import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompositViewComponent } from './composit-view.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : CompositViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [CompositViewComponent]
})
export class CompositViewModule { }
