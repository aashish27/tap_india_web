import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { CompostUnitService } from '../../../../services/activity/compost-unit/compost-unit.service';

@Component({
  selector: 'app-composit-view',
  templateUrl: './composit-view.component.html',
  styleUrls: ['./composit-view.component.css']
})
export class CompositViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private compostService:CompostUnitService, private baseUrl: Apisendpoints, private authGuard: AuthGuard) { }
  compostData:any;
  table:any;
  delete_id:any = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.compostService.getCompostUnit(this.baseUrl.getCompostUnitById + params.id).subscribe((res)=>{
          this.compostData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/composit-unit/form'], {queryParams: { id: data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'composit-unit', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteCompostUnit + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/composit-unit/index']);
    }
  }
  
}
