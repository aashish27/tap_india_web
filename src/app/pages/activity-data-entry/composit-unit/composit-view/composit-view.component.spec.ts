import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompositViewComponent } from './composit-view.component';

describe('CompositViewComponent', () => {
  let component: CompositViewComponent;
  let fixture: ComponentFixture<CompositViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompositViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
