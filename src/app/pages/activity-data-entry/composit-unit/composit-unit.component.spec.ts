import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompositUnitComponent } from './composit-unit.component';

describe('CompositUnitComponent', () => {
  let component: CompositUnitComponent;
  let fixture: ComponentFixture<CompositUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompositUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
