import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation'
;
@Component({
  selector: 'app-composit-unit',
  template: '<router-outlet></router-outlet>'
})
export class CompositUnitComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
