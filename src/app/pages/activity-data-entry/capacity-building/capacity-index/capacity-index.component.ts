import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CapacityBuildingService } from '../../../../services/activity/capacity-building/capacity-building.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';

@Component({
  selector: 'app-capacity-index',
  templateUrl: './capacity-index.component.html',
  styleUrls: ['./capacity-index.component.css']
})
export class CapacityIndexComponent implements OnInit {
  capacity : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  id:any = "";
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private capacityService: CapacityBuildingService,  private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router, private route: ActivatedRoute, private common:CommonFunctionService) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    let data = JSON.parse(sessionStorage.getItem('constant'));
    this.id = data['_id'];
    this.get();
  }

  get(){ // Get capacity data ..
    var self=this;
    this.capacityService.getCapacityBuilding(this.baseUrl.getCapacityBuilding + "?trainingAndExposureVisitId=" + this.id).subscribe( res=> { 
      this.capacity  = res;
      if(this.capacity.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    this.delete_id = this.baseUrl.deleteCapacityBuilding + id;
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/capacity-building/form'], {queryParams: { id : data._id , trainingId : this.id, via : 'row'}});
    }else {
      this.router.navigate(['activity-data-entry/capacity-building/view'], {queryParams:{ id: data._id, trainingId : this.id } });
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Capacity-building",
            footer: false,
            exportOptions: {
                columns: [1,2,3,4,5,6,7]
            }
          }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  }
    this.table = $('#capacityTable').DataTable(this.dtoptions);
    this.table.columns( [ 1, 5 ] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
  }
  
  exportCSV(){   // export csv button outside of table 
      this.table.button( '.buttons-csv' ).trigger();
  }
}
