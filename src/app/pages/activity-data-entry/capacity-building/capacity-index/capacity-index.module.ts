import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';
import { CapacityIndexComponent } from './capacity-index.component';

export const IndexRoutes : Routes = [
  {
    path : '',
    component : CapacityIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(IndexRoutes)
  ],
  declarations: [CapacityIndexComponent]
})
export class CapacityIndexModule { }
