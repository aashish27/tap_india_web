import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityIndexComponent } from './capacity-index.component';

describe('CapacityIndexComponent', () => {
  let component: CapacityIndexComponent;
  let fixture: ComponentFixture<CapacityIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
