import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { BeneficiaryService } from '../../../../services/beneficiary/beneficiary.service';
import { TrainingExposureService } from '../../../../services/activity/training-exposure/training-exposure.service';
import { CapacityBuildingService } from '../../../../services/activity/capacity-building/capacity-building.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import * as moment from 'moment';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { PARAMETERS } from '@angular/core/src/util/decorators';
declare const $:any;

@Component({
  selector: 'app-capacity-form',
  templateUrl: './capacity-form.component.html',
  styleUrls: ['./capacity-form.component.css']
})
export class CapacityFormComponent implements OnInit {
  capacityForm: FormGroup;
  submitted = false;
  capacityData:any;
  id: any;
  value:string="";
  showBeneficiary:boolean = false;
  trainId:any = "";

  villages:any = {};
  beneficiaries:any = {};
  topics: any = {};

  activityId:any;
  order:any = 3;

  //beneficiary list dropdown
  dropdownList:any = [];
  dropdownSettings = {};

  flag:boolean = true;

  constructor(
    private formBuilder: FormBuilder, 
    private route:ActivatedRoute,
    private beneficiaryService: BeneficiaryService, 
    private villageService: VillageService, 
    private trainingService: TrainingExposureService, 
    private capacityService: CapacityBuildingService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private common: CommonFunctionService
    ) {
      this.activityId = this.common.get(this.order);
     }

 ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.capacityForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    this.dropdownSettings = { 
      singleSelection: true, 
      text:"Select Beneficiary",
      enableSearchFilter: true,
      classes:"form-control"
    };  

    this.getVillages();
    this.getTopics();
    this.capacityForm = this.createFormGroup();

    this.route.queryParams.subscribe((param)=>{
      if(param.via == 'button'){
        this.value = "create";
        this.capacityData = JSON.parse(sessionStorage.getItem('constant'));
        this.trainId = this.capacityData['_id'];
        this.capacityForm.setValue({ 
          activityDate: this.capacityData['topicDate'] ? moment(this.capacityData['topicDate']).format('YYYY-MM-DD') : "",
          activityId: this.activityId,
          villageId: this.capacityData['villageData'] ? this.capacityData['villageData']._id : "",
          // beneficiaryId: this.capacityData['beneficiaryData'] ? this.capacityData['beneficiaryData']._id : "",
          beneficiaryId: this.capacityData['beneficiaryData']? [{ "id" : this.capacityData['beneficiaryData']._id, "itemName" : this.capacityData['beneficiaryData'].code + '-' + this.capacityData['beneficiaryData'].name + '-' + this.capacityData['beneficiaryData'].fatherName}] :[],
          cbtopicId: this.capacityData['cbTopicData'] ? this.capacityData['cbTopicData']._id : "",
          trainingAndExposureVisitId: this.trainId
        });
      }
      else{
        let data = JSON.parse(sessionStorage.getItem('constant'));
        this.trainId = data['_id'];
        if(param['data']){
          let val = {};
          val["data"] = JSON.parse(param['data']);
          this.setFormData(val, data);
        }
        else if(param.id){
          this.capacityService.getCapacityBuilding(this.baseUrl.getCapacityBuildingById + param.id + "?trainingAndExposureVisitId=" + param.trainingId).subscribe((res)=>{
              this.setFormData(res, data);
          });
        }
      }
    });
  }

  setFormData(val, data){
    this.capacityData = val['data'];
    this.id = this.capacityData['_id'];
    this.capacityForm.setValue({ 
      activityDate: this.capacityData['trainingAndeXposureVisitData'] ? moment(this.capacityData['trainingAndeXposureVisitData'].topicDate).format('YYYY-MM-DD') : this.capacityData['activityDate'] || "",
      activityId: this.activityId,
      villageId: this.capacityData['villageData'] ? this.capacityData['villageData']._id : this.capacityData['villageId'] || "",
      // beneficiaryId: this.capacityData['beneficiaryData'] ? this.capacityData['beneficiaryData']._id : this.capacityData['beneficiaryId'] || "",
      beneficiaryId: this.capacityData['beneficiaryData']? [{ "id" : this.capacityData['beneficiaryData']._id, "itemName" : this.capacityData['beneficiaryData'].code + '-' + this.capacityData['beneficiaryData'].name + '-' + this.capacityData['beneficiaryData'].fatherName}] :this.capacityData['beneficiaryId'] || [],
      cbtopicId: data['cbTopicData']._id,
      trainingAndExposureVisitId: this.trainId
    });

    this.getBeneficiary('update');
  }

  createFormGroup(){
    return new FormGroup({
      activityDate: new FormControl({value: '', disabled: true},Validators.required),
      activityId: new FormControl(this.activityId,Validators.required),
      villageId: new FormControl('',Validators.required),
      beneficiaryId: new FormControl('',this.beneficiaryValidator),
      cbtopicId: new FormControl({value:'', disabled : true},Validators.required),
      trainingAndExposureVisitId : new FormControl('',Validators.required)
    })
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      this.villages = res;
    }, (error)=>{
      console.log(error);
    });
  }

  getTopics(){
    this.trainingService.getTrainingExposure(this.baseUrl.getTrainingExposure).subscribe((res)=>{
      this.topics = res;
    }, (error)=>{
      console.log(error);
    });
  }

  beneficiaryValidator = (control: AbstractControl) => {
    if(!control.value && this.capacityForm && this.capacityForm.controls['villageId'].value != ""){
      return { required: true};
    }else if(!control.value && this.capacityForm && this.capacityForm.controls['villageId'].value == ""){
      return { required: true};
    }
    return null;
  }

  getBeneficiary(via = ''){
    let id = this.capacityForm.controls['villageId'].value;
    if(id){
      if(via == ''){
        this.capacityForm.controls['beneficiaryId'].reset("");
      }
      this.flag = false;
      this.beneficiaryService.getBeneficiary(this.baseUrl.getBeneficiary + "?villageId="+id).subscribe((res)=>{
        this.beneficiaries = res;
        let dropData = res['data'];
        let data = dropData.map((data)=>{
          return { "id": data._id, "itemName": data.code + '-' + data.name + '-' + data.fatherName}
        });

        if(data.length){
          this.dropdownList = data;
        }else{
          this.dropdownList = [];
          this.capacityForm.patchValue({
            beneficiaryId : []
          });
        }

        this.flag = true;
        if(!res['data'].length){
          this.showBeneficiary = true;
        }else{
          this.showBeneficiary = false;
        }
      }, (error)=>{
        console.log(error);
      });
    }else{
      this.beneficiaries = {};
      this.capacityForm.controls['beneficiaryId'].reset("");
    }
    this.capacityForm.controls['villageId'].markAsTouched();
  }

  redirectToBeneficiary(village_id){
    this.capacityForm.value['activityDate'] = this.capacityForm.controls['activityDate'].value;
    this.capacityForm.value["_id"] = this.id;
    this.router.navigate(['beneficiary/form'],{
      queryParams:{
        via : 'activity',
        village : village_id,
        route : "activity-data-entry/capacity-building/form",
        data : JSON.stringify(this.capacityForm.value)
      }
    })
  }

  onSubmit(val) {
    this.capacityForm.value['beneficiaryId'] = this.capacityForm.value['beneficiaryId'][0].id;
    this.capacityForm.setErrors({invalid : true});   
    if(this.id){    
      this.capacityService.putCapacityBuilding(this.baseUrl.putCapacityBuilding + this.id, this.capacityForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/capacity-building/index']);
        }
        this.capacityForm.setErrors(null);   
      },(error) => {
        this.capacityForm.setErrors(null);   
        console.log(error);
      });
    }else { 
      this.capacityService.postCapacityBuilding(this.baseUrl.postCapacityBuilding, this.capacityForm.value ).subscribe( (response)=> { 
        if(response['success']){ 
          this.router.navigate(['activity-data-entry/capacity-building/index']);
        }
        this.capacityForm.setErrors(null);   
      },(error) => {
        this.capacityForm.setErrors(null);   
        console.log(error);
      });
    }

  }

  revert() {
    this.dropdownList = [];
    this.capacityForm.patchValue({
      villageId: "",
      beneficiaryId: [],
    });
  }
}
