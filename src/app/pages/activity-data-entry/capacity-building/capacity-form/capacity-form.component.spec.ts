import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityFormComponent } from './capacity-form.component';

describe('CapacityFormComponent', () => {
  let component: CapacityFormComponent;
  let fixture: ComponentFixture<CapacityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
