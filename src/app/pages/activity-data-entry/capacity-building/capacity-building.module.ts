import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CapacityBuildingComponent } from './capacity-building.component';

export const CapacityRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path : '',
        redirectTo : 'index',
        pathMatch : 'full',
      },
      {
        path : 'form',
        loadChildren : './capacity-form/capacity-form.module#CapacityFormModule'
      },
      {
        path : 'index',
        loadChildren : './capacity-index/capacity-index.module#CapacityIndexModule'
      },
      {
        path : 'view',
        loadChildren : './capacity-view/capacity-view.module#CapacityViewModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CapacityRoutes)
  ],
  declarations: [CapacityBuildingComponent]
})
export class CapacityBuildingModule { }
