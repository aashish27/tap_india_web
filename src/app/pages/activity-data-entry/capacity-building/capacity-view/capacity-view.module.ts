import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';
import { CapacityViewComponent } from './capacity-view.component';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : CapacityViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [CapacityViewComponent]
})
export class CapacityViewModule { }