import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { CapacityBuildingService } from '../../../../services/activity/capacity-building/capacity-building.service';

@Component({
  selector: 'app-capacity-view',
  templateUrl: './capacity-view.component.html',
  styleUrls: ['./capacity-view.component.css']
})
export class CapacityViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private capacityService: CapacityBuildingService, private baseUrl: Apisendpoints) { }
  capacityData:any;
  delete_id:any = "";
  trainingId:string = "";
  rowId:string = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.trainingId = params.trainingId;
        this.rowId = params.id;
        this.capacityService.getCapacityBuilding(this.baseUrl.getCapacityBuildingById + params.id + "?trainingAndExposureVisitId=" + params.trainingId).subscribe((res)=>{
          this.capacityData = res['data'];
        })
      }
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/capacity-building/form'], {queryParams: {id: this.rowId, trainingId : this.trainingId}});
    }
  }

  delete(id){
    this.delete_id = this.baseUrl.deleteCapacityBuilding + id;
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/capacity-building/index']);
    }
  }
}
