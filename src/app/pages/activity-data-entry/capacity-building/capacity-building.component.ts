import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-capacity-building',
  template: '<router-outlet></router-outlet>'
})
export class CapacityBuildingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
