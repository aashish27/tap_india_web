import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingAndExposureVisitComponent } from './training-and-exposure-visit.component';

describe('TrainingAndExposureVisitComponent', () => {
  let component: TrainingAndExposureVisitComponent;
  let fixture: ComponentFixture<TrainingAndExposureVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingAndExposureVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingAndExposureVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
