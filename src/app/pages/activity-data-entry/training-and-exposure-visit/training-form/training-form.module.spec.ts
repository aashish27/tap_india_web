import { TrainingFormModule } from './training-form.module';

describe('TrainingFormModule', () => {
  let trainingFormModule: TrainingFormModule;

  beforeEach(() => {
    trainingFormModule = new TrainingFormModule();
  });

  it('should create an instance', () => {
    expect(trainingFormModule).toBeTruthy();
  });
});
