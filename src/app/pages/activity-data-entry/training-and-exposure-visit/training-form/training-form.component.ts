import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TrainingExposureService } from '../../../../services/activity/training-exposure/training-exposure.service';
import { CbTopicService } from '../../../../services/masters/cbtopic/cbtopic.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';
import * as moment from 'moment';

declare const $:any;

@Component({
  selector: 'app-training-form',
  templateUrl: './training-form.component.html',
  styleUrls: ['./training-form.component.css']
})
export class TrainingFormComponent implements OnInit {
  exposureForm: FormGroup;
  submitted = false;
  trainingData:any;
  id: any;

  topics:any = {};
  activityId:any;
  order:any = 3;

  constructor(
    private formBuilder: FormBuilder, 
    private trainingService: TrainingExposureService,
    private cbtopicService: CbTopicService, 
    private router: Router , 
    private baseUrl: Apisendpoints,
    private common: CommonFunctionService,
    private route: ActivatedRoute
    ) {
      this.activityId = this.common.get(this.order);
     }

  ngOnInit() {
    $('#activity-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.exposureForm.patchValue({
          activityDate : date
         });
      }
    }).keyup((e) => {
        if (e.keyCode == 8 || e.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });
    $('#topic-date').datepicker({
      maxDate: 0,
      changeMonth: true, 
      changeYear: true, 
      dateFormat: 'yy-mm-dd',
      yearRange: "-90:+00",
      onSelect: (date) => {
         this.exposureForm.patchValue({
          topicDate : date
         });
      }
    }).keyup((e1) => {
        if (e1.keyCode == 8 || e1.keyCode == 46) {
            $.datepicker._clearDate(this);
        }
    });

    this.getTopics();

    this.exposureForm = this.createFormGroup();
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.trainingService.getTrainingExposure(this.baseUrl.getTrainingExposureById + params.id).subscribe((res)=>{
          this.trainingData = res['data'];
          this.id = this.trainingData['_id'];
          this.exposureForm.setValue({ 
            topicDate: this.trainingData['topicDate'] ? moment(this.trainingData['topicDate']).format('YYYY-MM-DD') : "",
            activityId: this.activityId,
            totalFarmers: this.trainingData['totalFarmers'],
            cbTopicId: this.trainingData['cbTopicData']._id,
            location: this.trainingData['location']
          });
        });
      }
    });

  }

  createFormGroup(){
    return new FormGroup({
      activityId: new FormControl(this.activityId,Validators.required),
      topicDate: new FormControl('',Validators.required),
      cbTopicId: new FormControl('',Validators.required),
      location: new FormControl('',[Validators.required, WhitespaceValidator]),
      totalFarmers: new FormControl('',[Validators.required, WhitespaceValidator])
    })
  }

  getTopics(){
    this.cbtopicService.getCbtopic(this.baseUrl.getCbtopic).subscribe((res)=>{
      this.topics = res;
    });
  }

  status(event:any){
    return event.target.value = "";
  }

  revert() {
    this.exposureForm.patchValue({
      topicDate: "",
      cbTopicId: "",
      location: "",
      totalFarmers: ""
    });
  }
  
  onSubmit(val) {
    this.exposureForm.setErrors({invalid : true});
    if(this.id){  
      this.trainingService.putTrainingExposure(this.baseUrl.putTrainingExposure + this.id, this.exposureForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['activity-data-entry/training-and-exposure-visit/index']);
        }
        this.exposureForm.setErrors(null);
      },(error) => {
        this.exposureForm.setErrors(null);
        console.log(error);
      });
    }else {   
      this.trainingService.postTrainingExposure(this.baseUrl.postTrainingExposure, this.exposureForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['activity-data-entry/training-and-exposure-visit/index']);
        }
        this.exposureForm.setErrors(null);
      },(error) => {
        this.exposureForm.setErrors(null);
        console.log(error);
      });
    }

  }
}
