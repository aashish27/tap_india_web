import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingIndexComponent } from './training-index.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const IndexRoutes : Routes = [
  {
    path : '',
    component : TrainingIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(IndexRoutes)
  ],
  declarations: [TrainingIndexComponent]
})
export class TrainingIndexModule { }
