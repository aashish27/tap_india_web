import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TrainingExposureService } from '../../../../services/activity/training-exposure/training-exposure.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { CommonFunctionService } from '../../../../services/common-service/common-service.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-training-index',
  templateUrl: './training-index.component.html',
  styleUrls: ['./training-index.component.css']
})
export class TrainingIndexComponent implements OnInit {
  training : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";
  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private trainingService: TrainingExposureService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router:Router, private common:CommonFunctionService) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get training data ..
    var self=this;
    this.trainingService.getTrainingExposure(this.baseUrl.getTrainingExposure).subscribe( res=> { 
      this.training  = res;
      if(this.training.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'training-and-exposure-visit', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteTrainingExposure + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/training-and-exposure-visit/form'], {queryParams: {id : data._id}});
    }else if(to == 'view') {
      this.router.navigate(['activity-data-entry/training-and-exposure-visit/view'], {queryParams: {id : data._id}});
    }else{
      sessionStorage.setItem('constant' , JSON.stringify(data));
      this.router.navigate(['activity-data-entry/capacity-building/index']);
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {
            extend: 'csv', 
            text: 'Download CSV', 
            filename: "Training-and-exposure-visit",
            footer: false,
            exportOptions: {
                columns: [2,3,4,5,6,7,8]
            }
          }
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  }
    this.table = $('#trainingTable').DataTable(this.dtoptions);
    this.table.columns( [2] ).visible( false, false );
    this.table.columns.adjust().draw( false ); // adjust column sizing and redraw
    }
  
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'training-and-exposure-visit', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}
