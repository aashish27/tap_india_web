import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingIndexComponent } from './training-index.component';

describe('TrainingIndexComponent', () => {
  let component: TrainingIndexComponent;
  let fixture: ComponentFixture<TrainingIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
