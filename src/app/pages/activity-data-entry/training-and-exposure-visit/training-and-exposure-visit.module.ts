import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TrainingAndExposureVisitComponent } from './training-and-exposure-visit.component';
import { AuthGuard } from '../../guard/auth.guard';

export const TrainingRoutes : Routes = [
  {
    path : '',
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './training-index/training-index.module#TrainingIndexModule',
        data: {component: 'training-and-exposure-visit', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './training-view/training-view.module#TrainingViewModule',
        data: {component: 'training-and-exposure-visit', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './training-form/training-form.module#TrainingFormModule',
        data: {component: 'training-and-exposure-visit', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TrainingRoutes)
  ],
  declarations: [TrainingAndExposureVisitComponent]
})
export class TrainingAndExposureVisitModule { }
