import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { TrainingExposureService } from '../../../../services/activity/training-exposure/training-exposure.service';

@Component({
  selector: 'app-training-view',
  templateUrl: './training-view.component.html',
  styleUrls: ['./training-view.component.css']
})
export class TrainingViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private trainService:TrainingExposureService, private router: Router, private baseUrl: Apisendpoints) { }
  trainingData:any;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params.id){
        this.trainService.getTrainingExposure(this.baseUrl.getTrainingExposureById + params.id).subscribe((res)=>{
          this.trainingData = res['data'];
        });
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['activity-data-entry/training-and-exposure-visit/form'], {queryParams: {id : data._id}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'training-and-exposure-visit', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteTrainingExposure + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['activity-data-entry/training-and-exposure-visit/index']);
    }
  }
}
