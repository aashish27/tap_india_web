import { TrainingViewModule } from './training-view.module';

describe('TrainingViewModule', () => {
  let trainingViewModule: TrainingViewModule;

  beforeEach(() => {
    trainingViewModule = new TrainingViewModule();
  });

  it('should create an instance', () => {
    expect(trainingViewModule).toBeTruthy();
  });
});
