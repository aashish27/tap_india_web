import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrainingViewComponent } from './training-view.component';
import { Routes, RouterModule } from '@angular/router';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const ViewRoutes : Routes = [
  {
    path : '',
    component : TrainingViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(ViewRoutes)
  ],
  declarations: [TrainingViewComponent]
})
export class TrainingViewModule { }
