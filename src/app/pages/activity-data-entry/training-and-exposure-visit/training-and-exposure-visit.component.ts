import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../../animation';

@Component({
  selector: 'app-training-and-exposure-visit',
  templateUrl: './training-and-exposure-visit.component.html',
  animations: [
    topToBottom
  ]
})
export class TrainingAndExposureVisitComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
