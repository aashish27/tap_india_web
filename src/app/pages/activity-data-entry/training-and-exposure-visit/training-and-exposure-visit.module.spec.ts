import { TrainingAndExposureVisitModule } from './training-and-exposure-visit.module';

describe('TrainingAndExposureVisitModule', () => {
  let trainingAndExposureVisitModule: TrainingAndExposureVisitModule;

  beforeEach(() => {
    trainingAndExposureVisitModule = new TrainingAndExposureVisitModule();
  });

  it('should create an instance', () => {
    expect(trainingAndExposureVisitModule).toBeTruthy();
  });
});
