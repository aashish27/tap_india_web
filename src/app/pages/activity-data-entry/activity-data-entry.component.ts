import { Component, OnInit } from '@angular/core';
import { topToBottom } from '../../animation';

@Component({
  selector: 'app-activity-data-entry',
  templateUrl: './activity.component.html',
  animations: [
    topToBottom
  ]
})
export class ActivityDataEntryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
