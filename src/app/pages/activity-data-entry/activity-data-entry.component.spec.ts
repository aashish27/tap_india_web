import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityDataEntryComponent } from './activity-data-entry.component';

describe('ActivityDataEntryComponent', () => {
  let component: ActivityDataEntryComponent;
  let fixture: ComponentFixture<ActivityDataEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityDataEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDataEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
