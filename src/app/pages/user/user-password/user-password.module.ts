import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPasswordComponent } from './user-password.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
export const PassRoutes: Routes = [
  {
    path: '',
    component: UserPasswordComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PassRoutes),
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [UserPasswordComponent]
})
export class UserPasswordModule { }
