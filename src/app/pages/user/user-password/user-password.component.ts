import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../apisendpoints';
import { WhitespaceValidator } from '../../../validators/whitespace.validator';
import { UsersService } from '../../../services/user/users/users.service';
import { PasswordValidation } from '../../../validators/password.validator';

@Component({
  selector: 'app-user-password',
  templateUrl: './user-password.component.html',
  styleUrls: ['./user-password.component.css']
})
export class UserPasswordComponent implements OnInit {
  usersForm: FormGroup;
  submitted = false;
  userData:any;
  id: any;
  villages:any = [];
  userLevel:any = {};
  dropdownList = [];
  dropdownSettings = {};

  constructor(
    private usersService: UsersService,
    private router: Router , 
    private baseUrl: Apisendpoints
    ) {
      
    }

  ngOnInit() {
    this.usersForm = this.createFormGroup();
  }

  createFormGroup(){
    return new FormGroup({
      old_password: new FormControl('',[Validators.required, WhitespaceValidator]),
      password: new FormControl('',[Validators.required, WhitespaceValidator]),
      new_password: new FormControl('',[Validators.required, WhitespaceValidator])
    }, PasswordValidation.MatchPassword)
  }

  onSubmit(val) {
    this.usersForm.setErrors({invalid : true});

    this.usersService.putUsers(this.baseUrl.changePassword, this.usersForm.value ).subscribe( res=> { 
      if(res['success']){
        let data = JSON.parse(sessionStorage.getItem("userData"));
        data.isPasswordChangedOnce = true;
        sessionStorage.setItem("userData",JSON.stringify(data));
        this.router.navigate(['']);
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
          return false;
        };
      }
        this.usersForm.setErrors(null);
    },(error) => {
        this.usersForm.setErrors(null);
      console.log(error);
    });

  }

  revert() {
    this.usersForm.reset();
  }
}
