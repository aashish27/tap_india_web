import { UsersViewModule } from './users-view.module';

describe('UsersViewModule', () => {
  let usersViewModule: UsersViewModule;

  beforeEach(() => {
    usersViewModule = new UsersViewModule();
  });

  it('should create an instance', () => {
    expect(usersViewModule).toBeTruthy();
  });
});
