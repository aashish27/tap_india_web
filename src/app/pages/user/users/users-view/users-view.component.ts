import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
import { UsersService } from '../../../../services/user/users/users.service';

@Component({
  selector: 'app-users-view',
  templateUrl: './users-view.component.html',
  styleUrls: ['./users-view.component.css']
})
export class UsersViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private authGuard: AuthGuard, private userService: UsersService, private router: Router, private baseUrl: Apisendpoints) { }
  userData:any;;
  delete_id:any = "";
  villages:any = "";
  rowId:string = "";

  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
        this.rowId = params.id;
        this.userService.getUsers(this.baseUrl.getUsersById + params.id).subscribe((res)=>{
          this.userData = res['data'];
          this.villages = this.userData.villages.map(vil => vil.name).toString();
        });
    });
  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['user/users/form'], {queryParams: { id: this.rowId}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'users', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteUsers + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['user/users/index']);
    }
  }
}



