import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersViewComponent } from './users-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const userViewRoutes: Routes = [
  {
    path: '',
    component: UsersViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PopupModule,
    RouterModule.forChild(userViewRoutes),
  ],
  declarations: [UsersViewComponent]
})
export class UsersViewModule { }
