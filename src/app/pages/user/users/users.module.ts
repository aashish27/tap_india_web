import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { AuthGuard } from '../../guard/auth.guard';
export const UsersRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './users-index/users-index.module#UsersIndexModule',
        data: {component: 'users', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './users-view/users-view.module#UsersViewModule',
        data: {component: 'users', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './users-form/users-form.module#UsersFormModule',
        data: {component: 'users', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UsersRoutes)
  ],
  declarations: [UsersComponent]
})
export class UsersModule { }
