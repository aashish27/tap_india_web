import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersIndexComponent } from './users-index.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PopupModule } from '../../../../shared/popup/popup.module';

export const userIndexRoutes: Routes = [
  {
    path: '',
    component: UsersIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PopupModule,
    RouterModule.forChild(userIndexRoutes),
  ],
  declarations: [UsersIndexComponent]
})
export class UsersIndexModule { }
