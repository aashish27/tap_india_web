import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../../services/user/users/users.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-users-index',
  templateUrl: './users-index.component.html',
  styleUrls: ['./users-index.component.css']
})
export class UsersIndexComponent implements OnInit {
  users : any = {};
  tableHideShowStatus: boolean = false;
  dtOption: any = {};
  table:any;
  delete_id:any = "";

  constructor(private userService: UsersService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Activity Target data ..
    var self=this;
    this.userService.getUsers(this.baseUrl.getUsers).subscribe( res=> { 
      this.users  = res;
      if(this.users.success){
        setTimeout(function(){
          self.inittable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;  
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'users', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteUsers + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }
  
  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['user/users/form'], {queryParams: { id: data._id}});
    }else {
      this.router.navigate(['user/users/view'], {queryParams: { id: data._id}});
    }
  }

  inittable(){ // Data table Initialization after record fetched ..
    this.dtOption = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          {extend: 'csv', text: 'Download CSV', filename: "Users"}
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    }
    this.table = $('#usersTable').DataTable(this.dtOption);
  }
  
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'users', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}


