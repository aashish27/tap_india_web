import { UsersFormModule } from './users-form.module';

describe('UsersFormModule', () => {
  let usersFormModule: UsersFormModule;

  beforeEach(() => {
    usersFormModule = new UsersFormModule();
  });

  it('should create an instance', () => {
    expect(usersFormModule).toBeTruthy();
  });
});
