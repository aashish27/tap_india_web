import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersFormComponent } from './users-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

export const userFormRoutes: Routes = [
  {
    path: '',
    component: UsersFormComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,AngularMultiSelectModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(userFormRoutes),
  ],
  declarations: [UsersFormComponent]
})
export class UsersFormModule { }
