import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { VillageService } from '../../../../services/masters/village/village.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { WhitespaceValidator } from '../../../../validators/whitespace.validator';
import { UsersService } from '../../../../services/user/users/users.service';
import { mobileValidator } from '../../../../validators/mobile.validator';
import { EmailValidator } from '../../../../validators/email.validator';
import { UserLevelService } from '../../../../services/user/user-level/user-level.service';
import { PasswordValidation } from '../../../../validators/password.validator';

declare const $:any;

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {
  usersForm: FormGroup;
  submitted = false;
  userData:any;
  id: any;
  villages:any = [];
  userLevel:any = {};
  dropdownList = [];
  dropdownSettings = {};

  constructor(
    private villageService: VillageService, 
    private usersService: UsersService,
    private userLevelService: UserLevelService,
    private router: Router , 
    private baseUrl: Apisendpoints,
    private route: ActivatedRoute
    ) {
      
    }

  ngOnInit() {
    this.getVillages();
    this.getUserLevel();

    this.usersForm = this.createFormGroup();

    this.route.queryParams.subscribe((params)=>{
      this.usersService.getUsers(this.baseUrl.getUsersById + params.id).subscribe((res)=>{
        this.userData = res['data'];
        let village = [];
        if(this.userData['villages'].length){
          this.userData['villages'].filter((res)=>{ return res._id }).forEach(el => {
            village.push({ id : el._id, itemName: el.name});
          });
        } 
        this.id = this.userData['_id'];
        this.usersForm.setValue({ 
          first_name : this.userData['first_name'],
          last_name : this.userData['last_name'],
          mobile : this.userData['mobile'],
          password : this.userData['password'],
          confirm_password : this.userData['password'],
          email : this.userData['email'],
          userLevelId : this.userData['userLevelId']._id,
          villages : village
        });
      });
    });

    this.dropdownSettings = { 
      badgeShowLimit: 4,
      singleSelection: false, 
      maxHeight: 150,
      text:"Select Villages",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: true,
      classes:"myclass custom-class"
    };  

  }

  // onChanges(): void {
  //   this.cropDiversification.statusChanges.subscribe(val => {
  //         console.log(val);
          
  //   });
  // }

  createFormGroup(){
    return new FormGroup({
      first_name: new FormControl('',[Validators.required, WhitespaceValidator]),
      last_name: new FormControl('',[Validators.required, WhitespaceValidator]),
      mobile: new FormControl('',[mobileValidator]),
      password: new FormControl('',[Validators.required, WhitespaceValidator]),
      confirm_password: new FormControl('',[Validators.required, WhitespaceValidator]),
      email: new FormControl('',[Validators.required, EmailValidator, WhitespaceValidator]),
      userLevelId: new FormControl('',Validators.required),
      villages: new FormControl([])
    }, PasswordValidation.MatchPassword)
  }

  getVillages(){
    this.villageService.getVillages(this.baseUrl.getVillage).subscribe((res)=>{
      let data = res['data'];
      
      data.forEach(el => {
        this.villages.push({ id : el._id, itemName: el.name});
      });
      
    }, (error)=>{
      console.log(error);
    });
  }

  getUserLevel(){
    this.userLevelService.getUserLevel(this.baseUrl.getUserLevel).subscribe((res)=>{
      this.userLevel = res;
    },(error)=>{
      console.log(error);
    });
  }

  onSubmit(val) {
    this.usersForm.setErrors({invalid : true});
    let villData = this.usersForm.controls['villages'].value; 
    let data = [];
    villData.forEach(el => {
      data.push(el.id);
    });

    this.usersForm.patchValue({
      villages : data
    });

    if(this.id){   
      this.usersService.putUsers(this.baseUrl.putUsers + this.id, this.usersForm.value ).subscribe( res=> { 
        if(res['success']){
          this.router.navigate(['user/users/index']);
        }
         this.usersForm.setErrors(null);
      },(error) => {
         this.usersForm.setErrors(null);
        console.log(error);
      });
    }else {  
      this.usersService.postUsers(this.baseUrl.postUsers, this.usersForm.value ).subscribe( response=> { 
        if(response['success']){
          this.router.navigate(['user/users/index']);
        }
         this.usersForm.setErrors(null);
      },(error) => {
         this.usersForm.setErrors(null);
        console.log(error);
      });
    }

  }

  check(event){
    if (((event.which != 46 || (event.which == 46 && $(event.target).val() == '')) || $(event.target).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
       return event.preventDefault();
    }else if(event.which == 46 || event.which == 110 || event.which == 190){
      return event.preventDefault();
    }
    else if( $(event.target).val().split(".")[1] ? $(event.target).val().split(".")[1].length > 1 : false ){
      return event.preventDefault();
    }else if($(event.target).val().length > 9 && event.which !== 46  && event.which !== 8 && event.which !== 37  && event.which !== 39 ){
      return event.preventDefault();
    }else{
      return true;
    }
  }

  revert() {
    this.usersForm.reset();
  }
}

