import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-level',
  template: '<router-outlet></router-outlet>'
})
export class UsersLevelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
