import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersLevelComponent } from './users-level.component';
import { AuthGuard } from '../../guard/auth.guard';
export const UsersLevelRoutes: Routes = [
  {
    path: '',
    component: UsersLevelComponent,
    children : [
      {
        path: '',
        redirectTo: 'index',
        pathMatch: 'full',
      },{
        path: 'index',
        loadChildren: './users-level-index/users-level-index.module#UsersLevelIndexModule',
        data: {component: 'users-level', action: 'index'},
        canActivate: [AuthGuard]
      },{
        path: 'view',
        loadChildren: './users-level-view/users-level-view.module#UsersLevelViewModule',
        data: {component: 'users-level', action: 'view'},
        canActivate: [AuthGuard]
      },{
        path: 'form',
        loadChildren: './users-level-form/users-level-form.module#UsersLevelFormModule',
        data: {component: 'users-level', action: 'create'},
        canActivate: [AuthGuard],
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UsersLevelRoutes)
  ],
  declarations: [UsersLevelComponent]
})
export class UsersLevelModule { }

