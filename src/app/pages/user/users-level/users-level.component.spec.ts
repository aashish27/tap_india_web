import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersLevelComponent } from './users-level.component';

describe('UsersLevelComponent', () => {
  let component: UsersLevelComponent;
  let fixture: ComponentFixture<UsersLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
