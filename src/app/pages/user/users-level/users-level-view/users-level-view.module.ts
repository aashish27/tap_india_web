import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersLevelViewComponent } from './users-level-view.component';
import { PopupModule } from '../../../../shared/popup/popup.module';
export const usersLevelViewRoutes: Routes = [
  {
    path: '',
    component: UsersLevelViewComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    PopupModule,
    RouterModule.forChild(usersLevelViewRoutes),
  ],
  declarations: [UsersLevelViewComponent]
})
export class  UsersLevelViewModule  { }
