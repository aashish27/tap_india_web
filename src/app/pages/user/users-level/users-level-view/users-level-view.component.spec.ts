import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersLevelViewComponent } from './users-level-view.component';

describe('UsersLevelViewComponent', () => {
  let component: UsersLevelViewComponent;
  let fixture: ComponentFixture<UsersLevelViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersLevelViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersLevelViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
