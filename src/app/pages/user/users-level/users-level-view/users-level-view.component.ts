import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { AuthGuard } from '../../../../pages/guard/auth.guard';

@Component({
  selector: 'app-users-level-view',
  templateUrl: './users-level-view.component.html',
  styleUrls: ['./users-level-view.component.css']
})
export class UsersLevelViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private authGuard: AuthGuard, private baseUrl: Apisendpoints) { }
  userData:any;;
  delete_id:any = "";
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      if(params['data']){
        this.userData = JSON.parse(params['data']);
      }
    });

  }

  redirectTo(data, to){ // Rdirection .. 
    if(to == 'edit'){
      this.router.navigate(['user/users-level/form'], {queryParams: {name: data.name, id: data._id}, skipLocationChange: false});
    }else {
      this.router.navigate(['user/users-level/index'], {queryParams: {data: JSON.stringify(data)}});
    }
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'users-level', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteUserLevel + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.router.navigate(['user/users-level/index']);
    }
  }
}
