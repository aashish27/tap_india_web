import { UsersLevelViewModule } from './users-level-view.module';

describe('UsersLevelViewModule', () => {
  let usersLevelViewModule: UsersLevelViewModule;

  beforeEach(() => {
    usersLevelViewModule = new UsersLevelViewModule();
  });

  it('should create an instance', () => {
    expect(usersLevelViewModule).toBeTruthy();
  });
});
