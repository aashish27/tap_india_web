import { Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { Apisendpoints } from '../../../../apisendpoints';
import { LoaderService } from '../../../../services/loader.service';
import { UserLevelService } from '../../../../services/user/user-level/user-level.service';
import { AuthGuard } from '../../../../pages/guard/auth.guard';
declare const $: any;

@Component({
  selector: 'app-users-level-index',
  templateUrl: './users-level-index.component.html',
  styleUrls: ['./users-level-index.component.css']
})
export class UsersLevelIndexComponent implements OnInit {
  uLevel : any = {};
  tableHideShowStatus: boolean = false;
  dtoptions: any = {};
  table:any;
  delete_id:any = "";

  // tabletableHideShowStatusStatus: boolean = false;
  constructor(private userLevelService: UserLevelService, private authGuard: AuthGuard, private loaderService: LoaderService,  private baseUrl: Apisendpoints,   private renderer: Renderer, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.get();
  }

  get(){ // Get Districts data ..
    var self=this;
    this.userLevelService.getUserLevel(this.baseUrl.getUserLevel).subscribe( res=> { 
      this.uLevel  = res;
      if(this.uLevel.success){
        setTimeout(function(){
          self.initTable();
        }, 10);
        this.loaderService.hideLoader($);
        this.tableHideShowStatus = true;
      }
    });
  }

  delete(id){
    if(this.authGuard.checkActiveStatusCustom({component: 'users-level', action: 'delete'})){
      this.delete_id = this.baseUrl.deleteUserLevel + id;
    }
  }

  showDeleteStatus(event){
    if(event.success){
      this.table.destroy();
      this.get();
    }
  }

  redirectTo(data, to){ // Rdirection ..
    if(to == 'edit'){
      this.router.navigate(['user/users-level/form'], {queryParams: {name: data.name, id: data._id}, skipLocationChange: false});
    }else {
      this.router.navigate(['user/users-level/view'], {queryParams: {data: JSON.stringify(data)}, skipLocationChange: true});
    }
  }

  initTable(){ // Data Table Initialization after record fetched ..
    this.dtoptions = {
      "pagingType": "full_numbers",
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      responsive: true,
      scrollY: "60vh",
      language: {
          search: "_INPUT_",
          searchPlaceholder: "Search...",
      },
      dom: 'lBfrtip',
      buttons: [
          // {extend: 'csv', text: 'Download CSV', filename: "District"}
          //'copy', 'csv', 'excel', 'pdf', 'print'
      ]
  }
     this.table = $('#districtTable').DataTable(this.dtoptions);
  }
  
  exportCSV(){   // export csv button outside of table 
    if(this.authGuard.checkActiveStatusCustom({component: 'users-level', action: 'download'})){
      this.table.button( '.buttons-csv' ).trigger();
    }
  }
}

