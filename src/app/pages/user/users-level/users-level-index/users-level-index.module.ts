import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersLevelIndexComponent } from './users-level-index.component';
import { RouterModule, Routes } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { PopupModule } from '../../../../shared/popup/popup.module';
import { DistrictService } from '../../../../services/masters/district/district.service';
export const usersLevelIndexRoutes: Routes = [
  {
    path: '',
    component: UsersLevelIndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    PopupModule,
    RouterModule.forChild(usersLevelIndexRoutes),
    
  ],
  declarations: [UsersLevelIndexComponent],
  providers: [DistrictService]
})
export class UsersLevelIndexModule { }
