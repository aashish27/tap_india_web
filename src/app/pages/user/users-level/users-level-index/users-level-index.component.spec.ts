import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersLevelIndexComponent } from './users-level-index.component';

describe('UsersLevelIndexComponent', () => {
  let component: UsersLevelIndexComponent;
  let fixture: ComponentFixture<UsersLevelIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersLevelIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersLevelIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
