import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersLevelFormComponent } from './users-level-form.component';

describe('UsersLevelFormComponent', () => {
  let component: UsersLevelFormComponent;
  let fixture: ComponentFixture<UsersLevelFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersLevelFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersLevelFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
