import { UsersLevelFormModule } from './users-level-form.module';

describe('UsersLevelFormModule', () => {
  let usersLevelFormModule: UsersLevelFormModule;

  beforeEach(() => {
    usersLevelFormModule = new UsersLevelFormModule();
  });

  it('should create an instance', () => {
    expect(usersLevelFormModule).toBeTruthy();
  });
});
