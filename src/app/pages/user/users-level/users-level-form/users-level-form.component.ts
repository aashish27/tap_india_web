import { Component, OnInit, Renderer } from '@angular/core';
import { UserLevelService } from '../../../../services/user/user-level/user-level.service';
import { LoaderService } from '../../../../services/loader.service';
import { Apisendpoints } from '../../../../apisendpoints';
import { Router , ActivatedRoute } from '@angular/router';
import { resolve } from 'url';
import { ShowToastService } from '../../../../services/show-toast/show-toast.service';
@Component({
  selector: 'app-users-level-form',
  templateUrl: './users-level-form.component.html',
  styleUrls: ['./users-level-form.component.css']
})
export class UsersLevelFormComponent implements OnInit {
  viewStatus: boolean;
  modules: any;
  dummyModules: any = [];
  dummythRolesData: any;
  modifiedModulesDataBeforeSave: any;
  tableHideShowStatus: boolean = false;
  viewModuleSatus: boolean;
  rolesData: any = [];
  levelName: any;
  status:boolean = false;
  id: any;
  constructor(private toaster : ShowToastService, private route:ActivatedRoute, private userLevelService: UserLevelService,  private loaderService: LoaderService,  private baseUrl: Apisendpoints,   private renderer: Renderer, private router: Router) { }

  ngOnInit() {
    this.loaderService.showLoader($);
    this.levelName = "";
    this.viewStatus = true;
    this.viewModuleSatus= true;
    this.route.queryParams.subscribe(params => {
      this.levelName = params.name;
      this.id = params.id;
    });
    this.getUserRoleData();
  }

  getUserRoleData(){ // Get Component Module Strcture and modify it ...
    let this_parent = this;
    let endpoints;
    if(this.id){
      endpoints = "projectmodule/listwithaction?userLevelId="+this.id
    }else {
      endpoints = "projectmodule/listwithaction"          
    }
    new Promise((resolve, rejected) => {
        this.userLevelService.getUserLevel(endpoints).subscribe( res=> {
            this.rolesData = res;
            this.dummythRolesData = JSON.parse(JSON.stringify(this.rolesData.data));
            this.modifiedModulesDataBeforeSave = JSON.parse(JSON.stringify(this.rolesData.data));
            this.modules = this.rolesData.data;
            this.viewStatus = false;
            if(this.modules.length == 0){
              this.viewModuleSatus = true;
            }else {
              this.viewModuleSatus = false;
            }
            resolve("Resolved");
          }, (error) => {
            console.log('Error', error);
          });
        }).then((result)=>{
          this.loaderService.hideLoader($);
          this.tableHideShowStatus = true;
          setTimeout(function(){ 
            this_parent.checkStatus(); 
          }, 50);
        }).catch((err)=>{
          console.log(err);
        });
      
      }
      
  checkStatus(){ // Update Allocated status ..
    this.rolesData.data.forEach((parentElement, index)=>{ // Checked ..
      parentElement.action.forEach((parentElement)=>{
        if(parentElement.status){
          $("#pareentModule"+parentElement.actionId+index).prop('checked', true);
        }
      });

      if(parentElement.subModule.length > 0){ // For Sub module 
        if(!parentElement.isActive){
          parentElement.subModule.forEach((subAction)=>{
            subAction.action.forEach((actionElm)=>{ 
              $("#subModule"+subAction._id+actionElm.actionId+index).prop('disabled', true);
            });

            subAction.subModule.forEach((subChildAction1)=>{ // Sub of sub Child Checked 
              subChildAction1.action.forEach((childActionElm)=>{ 
                  $("#subModuleChildren"+subChildAction1._id+childActionElm.actionId+index).prop('disabled', true);
              });
            });

          }); 
        }else{
          parentElement.subModule.forEach((subAction)=>{
            
            subAction.action.forEach((actionElm)=>{ 
              if(actionElm.status){
                $("#subModule"+subAction._id+actionElm.actionId+index).prop('checked', true);
              }
            });

            subAction.subModule.forEach((subChildAction)=>{ // Sub of sub Child Checked 
              subChildAction.action.forEach((childActionElm)=>{ 
                if(childActionElm.status){
                  $("#subModuleChildren"+subChildAction._id+childActionElm.actionId+index).prop('checked', true);
                }
              });
            });
          });
        }
      }
    });
  }
  
  allocationDeallocationModuleAccess(parentModuleId = "",  actionId = "",  value, moduleLevel, subId = ""){ // Parent module access action ..
    this.dummyModules = this.dummythRolesData;
    this.dummyModules.forEach((parentElm, index)=>{
      if(parentElm._id == parentModuleId){
        if(moduleLevel == "parentModule"){
          parentElm.action.forEach((actionElm)=>{ // For parent module 
            if(actionElm.actionId == actionId){
              actionElm.status = value;
                if(!parentElm.action[0].status
                  && !parentElm.action[1].status
                  && !parentElm.action[2].status
                  && !parentElm.action[3].status
                  && !parentElm.action[4].status
                  && !parentElm.action[5].status){
                    parentElm.isActive = false;
                  }else {
                    parentElm.isActive = true;
                  }
                }
                if(parentElm.isActive){ // Disable Sub Module Checks ..
                  parentElm.subModule.forEach((subAction)=>{
                    subAction.action.forEach((actionElm)=>{ 
                      $("#subModule"+subAction._id+actionElm.actionId+index).prop('disabled', true);
                    });
                  }); 
                }else {
                  parentElm.subModule.forEach((subAction)=>{ // Enable Sub Module Checks ..
                    subAction.action.forEach((actionElm)=>{ 
                      $("#subModule"+subAction._id+actionElm.actionId+index).prop('disabled', false);
                    });
                  }); 
                }
              });
        }else {
          if(parentElm.subModule.length > 0){ // For Sub module 
            parentElm.subModule.forEach((subAction)=>{
              subAction.action.forEach((actionElm)=>{ // For parent module 
                if((actionElm.actionId == actionId) && (subAction._id == subId)){
                  actionElm.status = value; 
                  if(!subAction.action[0].status
                    && !subAction.action[1].status
                    && !subAction.action[2].status
                    && !subAction.action[3].status
                    && !subAction.action[4].status
                    && !subAction.action[5].status){
                      subAction.isActive = false;
                    }else {
                      subAction.isActive = true;
                    }
                  }
                });

                if(!subAction.isActive){ // Disable Sub Module Checks ..
                  subAction.subModule.forEach((subChildAction)=>{ // Sub of sub Child Checked 
                    subChildAction.action.forEach((childActionElm)=>{ 
                        $("#subModuleChildren"+subChildAction._id+childActionElm.actionId+index).prop('disabled', true);
                    });
                  }); 
                }else {
                  subAction.subModule.forEach((subChildAction)=>{ // Sub of sub Child Checked 
                    subChildAction.action.forEach((childActionElm)=>{ 
                        $("#subModuleChildren"+subChildAction._id+childActionElm.actionId+index).prop('disabled', false);
                    });
                  });
                }

                subAction.subModule.forEach(subModuleChildAction => { // Third level of Moule ..
                  subModuleChildAction.action.forEach((subModuleChildActionElm)=>{ // For parent module 
                    if((subModuleChildActionElm.actionId == actionId) && (subModuleChildAction._id == subId)){
                      subModuleChildActionElm.status = value; 
                      if(!subModuleChildAction.action[0].status
                        && !subModuleChildAction.action[1].status
                        && !subModuleChildAction.action[2].status
                        && !subModuleChildAction.action[3].status
                        && !subModuleChildAction.action[4].status
                        && !subModuleChildAction.action[5].status){
                          subModuleChildAction.isActive = false;
                        }else {
                          subModuleChildAction.isActive = true;
                        }
                      }
                    });
                });
              });
            }
          }
        }
      });
      this.modifiedModulesDataBeforeSave = this.dummyModules;
    }
    
    saveRoleModule(){// Save user role module access ...
      let moduleArray = [];
      this.modifiedModulesDataBeforeSave.forEach((parentElm)=>{
        let moudles = { // Parent Module breakups 
          'projectModuleId': parentElm._id,
          'action': parentElm.action,
          'isActive': parentElm.isActive
        }
        parentElm.subModule.forEach((subAction)=>{
          let moudles = {// Submodule breakups 
            'projectModuleId': subAction._id,
            'action': subAction.action,
            'isActive': subAction.isActive
          }
          subAction.subModule.forEach(subChildAction => {
            let moudles = {// Submodule breakups 
              'projectModuleId': subChildAction._id,
              'action': subChildAction.action,
              'isActive': subChildAction.isActive
            }
            moduleArray.push(moudles); 
          });
          moduleArray.push(moudles); 
        });
        moduleArray.push(moudles);      
      });
      let finalModule = {
        "name": this.levelName,
        "modules": moduleArray
      }

      if(typeof this.levelName === "undefined" || this.levelName == ""){
        this.toaster.filter("blankLevelName");
      }else {
        this.status = true;
        if(this.id){ // Update Existing Level ...
          this.userLevelService.putUserLevel(this.baseUrl.putUserLevel+ this.id,finalModule).subscribe((res)=>{ 
            if(res['success']){
              // this.getUserRoleData();
              this.status = false;
              this.router.navigate(['user/users-level/index']);
            }
          }, (error) => {
            this.status = false;
            this.toaster.filter("same");
              // console.log('Error', error);
          });
        }else { // Create new user level ...
          this.userLevelService.postUserLevel(this.baseUrl.postUserLevel,finalModule).subscribe((response)=>{ 
            if(response['success']){
              // this.getUserRoleData();
              this.status = false;
              this.router.navigate(['user/users-level/index']);
            }
          }, (error) => {
              this.status = false;
              this.toaster.filter("same");
              // console.log('Error', error);
          });
        }
      }
    // console.log("finalModule",finalModule);
    
  }
}
