import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersLevelFormComponent } from './users-level-form.component';
import {FormsModule} from '@angular/forms';
export const usersLevelViewRoutes: Routes = [
  {
    path: '',
    component: UsersLevelFormComponent
  }
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(usersLevelViewRoutes),
  ],
  declarations: [UsersLevelFormComponent]
})
export class UsersLevelFormModule { }
