import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';
export const UserRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule'
      },
      {
        path: 'users-level',
        loadChildren: './users-level/users-level.module#UsersLevelModule'
      },
      {
        path: 'user-password',
        loadChildren: './user-password/user-password.module#UserPasswordModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
  ],
  declarations: [UserComponent]
})
export class UserModule { }
