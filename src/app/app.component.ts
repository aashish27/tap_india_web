import {Component} from '@angular/core';
import {ToasterService} from './services/toaster/toaster.service';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';
import { HttpClient } from '@angular/common/http';
import { topToBottom } from './animation';
import { CommonFunctionService } from './services/common-service/common-service.service';
import { ShowToastService } from './services/show-toast/show-toast.service';

declare var $: any;

@Component({
    selector: 'app-root',
    // template: '<router-outlet><ng2-toasty [position]="position"></ng2-toasty></router-outlet>',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    animations: [
        topToBottom
    ]
})

export class AppComponent {
    executed: boolean = false;
    position  = "top-right";
    count: number = 0;
    public onlineFlag;

    constructor(private toastyService: ToastyService, private showToastService: ShowToastService,  private toaster: ToasterService,private _http: HttpClient, private commonService:CommonFunctionService) {
        this.checkstatus();
        setInterval(() => { //check after every 8 sec Online or not
            this.checkstatus();
        }, 8000);
        
        this.showToastService.listen().subscribe((m:any) => {
            if(m != ""){
                this.checkErrorStatus(m);
            }
        })
    }

    checkstatus() { //check online or not and show toaster
        this.onlineFlag = navigator.onLine;
        if (!this.onlineFlag) {
            this.executed = true;
            this.toastyService.clearAll();
            this.showToast({
                title: 'Connection Status',
                msg: 'No internet connection available !',
                timeout: 5000,
                theme: 'material',
                position: 'top-center',
                type: 'wait'
            });
        }
        else if (this.onlineFlag && this.executed) {
            this.toastyService.clearAll();
            this.showToast({
                title: 'Connection Status',
                msg: 'Connection established !',
                timeout: 5000,
                theme: 'material',
                position: 'top-center',
                type: 'success'
            });
            this.executed = false;
        }
    }
    
    checkErrorStatus(val){ // Check Error Status and Perform Respected Action ..
        if(typeof val == "string"){
            if(val == "Invalid"){
                this.toastyService.clearAll();
                this.showToast({title:'Invalid Credential', msg:'Check your Username or Password !', timeout: 5000, theme:'bootstrap', position:'top-right', type:'error'});  
            }else if(val == "blankLevelName"){
                this.toastyService.clearAll();
                this.showToast({title:'Warning!', msg:"Level Name can't be empty!", timeout: 5000, theme:'bootstrap', position:'top-right', type:'warning'});  
            }else if(val == "same"){
                this.showToast({title:'Error!', msg:"User-level already exists!", timeout: 5000, theme:'bootstrap', position:'top-right', type:'error'});  
            }
        }else{
            if(val.success){
                this.toastyService.clearAll();
                this.showToast({title:'Success!', msg: val.message, timeout: 5000, theme:'bootstrap', position:'top-right', type:'success'});
            }else{
                this.toastyService.clearAll();
                this.showToast({title:'Error!', msg: val.message, timeout: 5000, theme:'bootstrap', position:'top-right', type:'error'});
            }   
        }

        
    }

    showToast(options) { // Show Toaster ..
        let toastOptions = this.toaster.getToastData(options);
        this.position = options.position ? options.position : this.position;
        switch (options.type) {
            case 'default':
                this.toastyService.default(toastOptions);
                break;
            case 'info':
                this.toastyService.info(toastOptions);
                break;
            case 'success':
                this.toastyService.success(toastOptions);
                break;
            case 'wait':
                this.toastyService.wait(toastOptions);
                break;
            case 'error':
                this.toastyService.error(toastOptions);
                break;
            case 'warning':
                this.toastyService.warning(toastOptions);
                break;
        }
      }

}
