import { Injectable } from '@angular/core';

@Injectable()
export class Apisendpoints {

    //CHARTS

        //operational level
        getFarm = "farm_mechanization_overall?type=";
        getSoak = "soak_pit_demonstration_graph?";
        getKitchen = "kitchen_garden_ben_count";
        getCropDemo = "crop_demonstration_percent_wise";
        getThematic = "thematic_ben_graph";
        getCropDiv = "crop_diversification_area_count?";
        getHand = "hand_maize_sheller_count?"
        getHealth = "health_hygiene_women_count?"

        getCapacityExposure = "capacity_building_ben_count?";
        getCapacityDigital = "digital_literacy_center_gender_count?";
        getDigitalVillage = "digital_literacy_center_village_list";
        getYearlyVillage = "school_renovation_village_list";
        getYearlyAttendence = "school_renovation_growth_graph?";

        getVIllageList = "hand_maize_sheller_village_list";
        getCapacityVillage = "health_hygiene_village_list";
        getCropList = "crop_diversification_crop_list";
        getTopicList = "capacity_building_cbtopic_list";

        getAreaMech = "farm_mechanization_overall?type=coverage";
        getCostReduce = "farm_mechanization_overall?type=saving";
        getCropByAct = "farm_mechanization_crop_list?type=";


        getGoatData = "goat_monthly_data?";
        getGoatCount = "goat_monthly_count?";
        getAnimal = "animal_health_camp_count?";
        getLiveVillage = "goat_village_list";
        getLiveBeneficiary = "goat_beneficiary_list?villageId="

        //management level
        getTargetActivity = "target_activity_wise";
        getTargetBenficiary = "target_beneficiary_wise";
        getProgramGender = "program_wise_gender_bpl";
        getOverallGender = "beneficiary_gender_wise_distribution";
        getOverallBpl = "beneficiary_bpl_wise_distribution";

    //REPORTS

        //Agriculture
        getFmZero = "farm_mechanization_report?type=zero_tillage";
        getFmPotato = "farm_mechanization_report?type=potato_planter";
        getCropDemoReport = "crop_demonstration_report";
        getCropDivReport = "crop_diversification_report";

    //USER

        //Users APIs Endpoints ..
        getUsers = "user";
        getUsersById = "user/";
        postUsers =  "user";
        putUsers = "user/";
        deleteUsers = "user/";
        changePassword = "user.password"

        //UserLevel APIs Endpoints ..
        getUserLevel = "userlevel";
        postUserLevel =  "userlevel";
        putUserLevel = "userlevel/";
        deleteUserLevel = "userlevel/";
        getUserMoudleListWithAction = "projectmodule/listwithaction";

        getUserModuleList = "projectmodule";

    //MASTERS

        // Districts APIs Endpoints ..
        getDistrict = "district";
        postDistrict =  "district";
        putDistrict = "district/";
        deleteDistrict = "district/";
        // Block APIs Endpoints ..
        getBlock = "block";
        postBlock = "block";
        putBlock = "block/";
        deleteBlock = "block/";
        // Village APIs Endpoints ..
        getVillage = "village";
        postVillage = "village";
        putVillage = "village/";
        deleteVillage = "village/";
        // Activity APIs Endpoints ..
        getActivity = "activity";
        postActivity = "activity";
        putActivity = "activity/";
        deleteActivity = "activity/";
        // Activity Target APIs Endpoints ..
        getActivityTarget = "activity.target";
        postActivityTarget = "activity.target";
        putActivityTarget = "activity.target/";
        deleteActivityTarget = "activity.target/";
        // Crop APIs Endpoints ..
        getCrop = "crop";
        postCrop = "crop";
        putCrop = "crop/";
        deleteCrop = "crop/";
        // Variety APIs Endpoints ..
        getVariety = "variety";
        postVariety = "variety";
        putVariety = "variety/";
        deleteVariety = "variety/";
        // Machinery APIs Endpoints ..
        getMachinery = "machinery";
        postMachinery = "machinery";
        putMachinery = "machinery/";
        deleteMachinery = "machinery/";
        // Beneficiary APIs Endpoints ..
        getBeneficiary = "beneficiary";
        postBeneficiary = "beneficiary";
        putBeneficiary = "beneficiary/";
        deleteBeneficiary = "beneficiary/";
        downloadBeneficiary = "beneficiary_csv";
        // Cbtopic APIs Endpoints ..
        getCbtopic = "cbtopic";
        postCbtopic = "cbtopic";
        putCbtopic = "cbtopic/";
        deleteCbtopic = "cbtopic/";
        // Machinery Code APIs Endpoints ..
        getMachineryCode = "machinery.profile";
        postMachineryCode = "machinery.profile";
        putMachineryCode = "machinery.profile/";
        deleteMachineryCode = "machinery.profile/";
        // Machinery Owner APIs Endpoints ..
        getMachineryOwner = "machinery.owner";
        postMachineryOwner = "machinery.owner";
        putMachineryOwner = "machinery.owner/";
        deleteMachineryOwner = "machinery.owner/";
        //S3 url getter....
        getS3Url = "getS3Url";
    
    //ACTIVITY DATA ENTRY

        //Farm Bunding
        getFarmBunding = "crud/farm_bunding";
        getFarmBundingById = "crud/farm_bunding/";
        postFarmBunding = "crud/farm_bunding";
        putFarmBunding = "crud/farm_bunding/";
        deleteFarmBunding = "crud/farm_bunding/";

        //Soil Testing
        getSoilTesting = "crud/soil_testing";
        getSoilTestingById = "crud/soil_testing/";
        postSoilTesting = "crud/soil_testing";
        putSoilTesting = "crud/soil_testing/";
        deleteSoilTesting = "crud/soil_testing/";

        //Demonstration Of Crops Specific Pop
        getCropDemonstration = "crud/crop_specific_pop";
        getCropDemoById = "crud/crop_specific_pop/";
        postCropDemonstration = "crud/crop_specific_pop";
        putCropDemonstration = "crud/crop_specific_pop/";
        deleteCropDemonstration = "crud/crop_specific_pop/";

        //Horticulture
        getHorticulture = "crud/horticulture";
        getHorticultureById = "crud/horticulture/";
        postHorticulture = "crud/horticulture";
        putHorticulture = "crud/horticulture/";
        deleteHorticulture = "crud/horticulture/";
        
        //Compost Unit 
        getCompostUnit = "crud/composting";
        getCompostUnitById = "crud/composting/";
        postCompostUnit = "crud/composting";
        putCompostUnit = "crud/composting/";
        deleteCompostUnit = "crud/composting/";

        //Solar Spray Pumps 
        getSolarSpray = "crud/promotion_solar_spray_pump";
        getSolarSprayById = "crud/promotion_solar_spray_pump/";
        postSolarSpray = "crud/promotion_solar_spray_pump";
        putSolarSpray = "crud/promotion_solar_spray_pump/";
        deleteSolarSpray = "crud/promotion_solar_spray_pump/";

        //Training Exposure 
        getTrainingExposure = "crud/exposure_visit";
        getTrainingExposureById = "crud/exposure_visit/";
        postTrainingExposure = "crud/exposure_visit";
        putTrainingExposure = "crud/exposure_visit/";
        deleteTrainingExposure = "crud/exposure_visit/";

        //Capacity Building 
        getCapacityBuilding = "crud/capacity_building";
        getCapacityBuildingById = "crud/capacity_building/";
        postCapacityBuilding = "crud/capacity_building";
        putCapacityBuilding = "crud/capacity_building/";
        deleteCapacityBuilding = "crud/capacity_building/";

        //Pond Rejuvenation
        getPond = "crud/pond_rejuvination";
        getPondById = "crud/pond_rejuvination/";
        postPond = "crud/pond_rejuvination";
        putPond = "crud/pond_rejuvination/";
        deletePond = "crud/pond_rejuvination/";

        //farm pond
        getFarmPond = "crud/farm_pond";
        getFarmPondById = "crud/farm_pond/";
        postFarmPond = "crud/farm_pond";
        putFarmPond = "crud/farm_pond/";
        deleteFarmPond = "crud/farm_pond/";


    //DASHBOARD
        //card count
        getCardCount = "cardDetails";
        getWaterHarvestedGraph = "water_harvested_graph";
        getSoilTestingGraph = "soil_testing_graph";
        getReportData = "ben_activity_wise_count?"
}
