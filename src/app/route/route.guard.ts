import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuard implements CanActivate  {
  constructor(private auth: AuthService, private router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      // console.log("next",next);
      // console.log("state",state);
      let routesInfo = next.data;
      // console.log("roles",routesInfo);
      // if (!this.auth.isLoggedIn(routesInfo)) {
      //     this.router.navigate(['/unauthorized']);
      // }
      return true;
      // return this.auth.isLoggedIn();
      // return true;
  }
}
