export interface UserLevel{
    _id: string;
    modules:any;
    name: string; 
    createdAt: string;
    modifiedAt: string;
}