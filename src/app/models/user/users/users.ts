export interface Users{
    id: string;
    villages:any;
    first_name: string;
    last_name: string,
    password: string;
    email: string;
    userLevelId:any;
    user_name:string;
    createdAt: string;
    modifiedAt: string;
}