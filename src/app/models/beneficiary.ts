export interface Beneficiary{
    _id:string;
    name:string;
    images:any;
    villageId:any;
    location:any;
    photo:any;
    fatherName:string;
    mobile:string;
    dob:string;
    gender:string;
    socialCategory:string;
    createdAt:string;
    modifiedAt:string;
}