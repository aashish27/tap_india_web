export interface Machinery {
    _id: string;
    name: string;
    code: string;
    activityId:any;
    createdAt: string;
    modifiedAt: string;
} 
