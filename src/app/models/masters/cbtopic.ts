export interface CbTopic {
    _id: string;
    name: string;
    createdAt: string;
    modifiedAt: string;
}