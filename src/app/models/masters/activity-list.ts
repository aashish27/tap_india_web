export interface ActivityList {
    id: string;
    name: string;
    code: string,
    createdAt: string;
    modifiedAt: string;
}