export interface MachineryCode{
    _id:string;
    machineryId:any;
    createdAt:string;
    modifiedAt:string;
}