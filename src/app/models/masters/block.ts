export interface Block {
    id: string;
    name: string;
    districtId: any,
    createdAt: string;
    modifiedAt: string;
}