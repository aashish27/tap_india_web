export interface Village {
    _id: string;
    name: string;
    code: string;
    type: string;
    blockId: string;
    districtId: string;
    createdAt: string;
    modifiedAt: string;
} 
