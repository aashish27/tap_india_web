export interface District {
    id: string;
    name: string;
    createdAt: string;
    modifiedAt: string;
} 
