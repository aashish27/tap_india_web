export interface Variety {
    _id: string;
    name: string;
    cropId:any;
    createdAt: string;
    modifiedAt: string;
} 
