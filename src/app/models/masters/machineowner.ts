export interface MachineOwner{
    machineryId:any;
    villageId:any;
    beneficiaryId:any;
    createdAt:string;
    updatedAt:string;
}