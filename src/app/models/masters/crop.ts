export interface Crop {
    _id: string;
    name: string;
    tr_recovery:string;
    createdAt: string;
    modifiedAt: string;
} 
