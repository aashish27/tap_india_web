export interface ActivityTarget {
    _id: string;
    activityId: any;
    target_activity:string;
    target_beneficiary:string;
    createdAt: string;
    modifiedAt: string;
}