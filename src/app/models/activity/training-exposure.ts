export interface TrainingExposure{
    _id:string;
    modifiedAt:string;
    createdAt:string;
    topicDate:string;
    location:string;
    activityData:any;
    cbTopicData:any;
    totalFarmers:string;
}