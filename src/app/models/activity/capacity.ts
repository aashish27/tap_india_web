export interface Capacity{
    _id:string;
    trainingAndeXposureVisitData:any;
    activityData:any;
    cbTopicData:any;
    villageData:any;
    beneficiaryData:string;
    createdAt:string;
    modifiedAt:string;
}