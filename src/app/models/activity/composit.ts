export interface Composit{
    _id:string;
    quantity:string;
    farmerUsingCompostInField:string;
    activityData:any;
    villageData:any;
    beneficiaryData:string;
    modifiedAt:string;
    createdAt:string;
}