export interface Horticulture{
    _id:string,
    plantName:string,
    diversificationArea:string,
    productivity:string,
    activityData:any,
    villageData:any;
    beneficiaryData:any;
    cropData:any
    CreatedAt:string,
    UpdatedAt:string
}