export interface SolarSpray{
    _id:string;
    area:string;
    activityData:any;
    villageData:string;
    beneficiaryData:string;
    cropData:string;
    machineryOwnerData:string;
    machineryOwnerBeneficiaryData:string;
    createdAt:string;
    modifiedAt:string;
}