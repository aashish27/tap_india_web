export interface Pond{
    _id:string;
    activityData:any;
    noOfPondsRejuvenated:string;
    waterHarvestedByPonds:string;
    improvementInWaterAvailability:string;
    createdAt:string;
    modifiedAt:string;
}