export interface SoilTesting{
    _id:string;
    cowCount:string;
    buffaloCount:string;
    cowCalfCount:string;
    totalAnimalTreated:string;
    activityIdData:string;
    villageIdData:string;
    beneficiaryIdData:string;
    modifiedAt:string;
    createdAt:string;
}